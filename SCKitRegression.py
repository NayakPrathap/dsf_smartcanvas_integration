# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys

driver = None

def getOrCreateWebDriver():
	global driver
	driver = driver or webdriver.Firefox()
	return driver

def magicArgs(allArgsParent):
	global driver
	global TestOnSauceLabs
	global myVersion_Major
	global myVersion_Mainor
	global myVersion_Update
	global myVersion_Build
    # *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# Filename: sysArgvTest.py
	# Function: Test to retrieve system argument value
	# Behavior: Based on provided argument, it run test locally or over SauceLabs
	#           If some arguments are missing, then we stop to run test
	# Usage: Jenkins execution with parameter value
	# Author: Nobuaki Ogawa - nobuaki.ogawa@efi.com
	# Arguments:
	#   [1] Browser Name (SauceLabs - browserName) --- Input Example <firefox|chorme|firefox dev|firefox dev 64?>
	#   [2] Target Test URL
	#   [3] SmartCanvas - Version (7.2.x.x|7.3.x.x|Trunk(7.9.x.x))
	#   [4] SauceLabs URL
	#   [5] SauceLabs - platform
	#   [6] SauceLabs - version
	#   [7] SauceLabs - name
	# Tips: If you run test over SauceLabs, please ensure all above arguments were provided while test. 
	# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# **********************************************************
	# Create several variable based on system arguments as well as fallback value
	# **********************************************************
	allArgs = allArgsParent
	countArgs = len(allArgs)
	print("001. Provided Arguments Counts: " + str(countArgs))
	# Set FireFox exe path
	FIREFOX_STD_EXE_PATH = "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"
	FIREFOX_DEV_EXE_PATH  = "C:\\Program Files (x86)\\Mozilla Developer Preview\\firefox.exe"
	FIREFOX_STD_EXE_PATH_X64 = "C:\\Program Files\\Mozilla Firefox\\firefox.exe"
	FIREFOX_DEV_EXE_PATH_X64 = "C:\\Program Files\\Mozilla Developer Preview\\firefox.exe\\"
	default_URL = "https://devops-az06.directsmile.com/smartcanvas/index.html"
	default_SCVersion = "7.3.0.85"
	TestOnSauceLabs = False
	# **********************************************************
	# Check whether any parameter was provided or not
	# **********************************************************
	if countArgs > 7:
		print("002. TestURL & SauceLabsURL were provide within script call as system arguments")
		print("003. Run script via SauceLabs")
		TestOnSauceLabs = True
		Browser = allArgsParent[1:]
		TestURL = allArgsParent[2:]
		SmartCanvas_Version = allArgsParent[3:]
		SauceLabsURL = allArgsParent[4:]
		SauceLabs_platform = allArgsParent[5:]
		SauceLabs_version = allArgsParent[6:]
		SauceLabs_name = allArgsParent[7:]
		myUrl = TestURL[0]
		myBrowser = Browser[0]
		myVersion = SmartCanvas_Version[0]
		if myBrowser == "firefox dev":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 01")
		elif myBrowser == "firefox dev 64":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 02")
		elif myBrowser == "firefox 64":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 03")
		else:
			SauceLabs_browserName = myBrowser
			print ("Change browser to specified one -- 04")
		# **********************************************************************************************
		# This is the only code you need to edit in your existing scripts
		# The command_executor tells the test to run on Sauce, while the desired_capabilities
		# parameter tells us which browsers and OS to spin up.
		# **********************************************************************************************
		desired_cap = {
			'platform': SauceLabs_platform[0],
			'browserName': SauceLabs_browserName,
			'version': SauceLabs_version[0],
			'name': SauceLabs_name[0]+ " - " + myVersion,
		}
		driver = webdriver.Remote(
			command_executor = SauceLabsURL[0],
			desired_capabilities = desired_cap
		)
	elif countArgs > 4 and countArgs < 8:
		print("002. Browser & TestURL & SauceLabsURL were provide within script call as system arguments")
		print("003. Some SauceLabs relates arguments are missing, so we stop test now")
		quit()
	elif countArgs > 3:
		print("002. Browser & TestURL, and SmartCanvas Version were provided within script call as system arguments")
		print("003. Run script locally")
		Browser = allArgsParent[1:]
		myBrowser = Browser[0]
		TestURL = allArgsParent[2:]
		myUrl = TestURL[0]
		SmartCanvas_Version = allArgsParent[3:]
		myVersion = SmartCanvas_Version[0]
		if myBrowser == "chrome":        
			# **********************************************************
			#004. Set Chrome as default browser
			# **********************************************************
			print ("004. Set Chrome as default browser")
			driver = webdriver.Chrome()
			#006. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (Mozilla Developer Preview)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev 64":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox 64":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (FireFox - 64bit)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (FireFox - 64bit)")
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		else:
			# **********************************************************
			#004. Set Desired Capabilities for local execution (FireFox)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (FireFox)")
			myBrowser = "firefox"
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#005. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
	elif countArgs > 2:
		print("002. Browser was provided, but neither TestURL nor SmartCanvas Version couldn't be found within script call as system arguments")
		print("003. Run script locally")
		Browser = allArgsParent[1:]
		myBrowser = Browser[0]
		print("004. Set default target URL")
		# 004 Set default target URL as fallback
		myUrl = default_URL
		# 005. Set target SC Version: 
		myVersion = default_SCVersion
		print ("005. Set target SC Version: " + str(myVersion))
		if myBrowser == "chrome":
			# **********************************************************
			# 006. Set Chrome as default browser
			# **********************************************************
			print ("006. Set Chrome as default browser")
			driver = webdriver.Chrome()
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (Mozilla Developer Preview)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev 64":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox 64":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (FireFox - 64bit)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (FireFox - 64bit)")
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		else:
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (FireFox)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (FireFox)")
			myBrowser = "firefox"
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
	else:
		print("002. None of Browser, TestURL, SmartCanvas Version were provided within script call as system arguments")
		print("003. Run script locally")
		# 004 Set default as fallback
		print("004. Set default target URL")
		myUrl = default_URL
		# 005. Set target SC Version: 
		myVersion = default_SCVersion
		print ("005. Set target SC Version: " + str(myVersion))
		# **********************************************************
		# 006. Set Desired Capabilities for local execution
		# **********************************************************
		print ("006. Set Desired Capabilities for local execution")
		myBrowser = "firefox"
		binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
		caps = DesiredCapabilities.FIREFOX
		driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)

	# **********************************************************
	# Check actual Test URL
	# **********************************************************
	print("---- Test URL: " + myUrl) 

	# **********************************************************
	# Check Target SmartCanvas Version
	# **********************************************************
	print("---- SmartCanvas Version: " + myVersion) 
	myVersion_Major = myVersion.split(".")[0]
	myVersion_Mainor = myVersion.split(".")[1]
	myVersion_Update = myVersion.split(".")[2]
	myVersion_Build = myVersion.split(".")[3]
	# Set Ver7 >= condition
	if int(myVersion_Major) >= 7 and int(myVersion_Mainor) >= 3 and int(myVersion_Update) >= 0 and int(myVersion_Build) >= 85:
		print ("---- Alert may appear")
	else:
		print ("---- Alert may not appear while uploading")
	# **********************************************************
	
	# **********************************************************
	#1. Start WebDriver
	print ("#1. Start WebDriver")
	driver.implicitly_wait(30)

	#2. Browse specify url
	print ("#2. Browse specify url")
	driver.get(myUrl)
	wait = WebDriverWait(driver, 10)
	logo = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, "div.scMenuPanel")))
	print ("#2-1, I waited dynamically")
	time.sleep(3)
	# **********************************************************
	return (driver,TestOnSauceLabs,	myVersion_Major, myVersion_Mainor, myVersion_Update, myVersion_Build)

def saveCanvas():
	# **********************************************************************************************
	#4. Open File menu and Save it
	print ("#4. Open File menu and Save it")
	#4-1. Open File menu
	print ("#4-1. Open File menu")
	driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
	time.sleep(1)
	#4-2. Save
	print ("#4-2. Save")
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[2]").click()
	time.sleep(5)
	# **********************************************************************************************

def outputPDF_LOW():
    global DL_LINK_LOW
    global myDSMOURL_LOW
    global myPDFinfo_LOW
    # **********************************************************************************************
    #5. Output - Default
    print ("#5. Output")
    #5-1. Open File menu and click "Create preview PDF"
    print ("#5-1. Open File menu and click 'Create preview PDF'")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[6]").click()
    #5-2. Wait PDF rendering until 'Download PDF' link appear
    print ("#5-2. Wait PDF rendering until 'Download PDF' link appear")
    WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.LINK_TEXT, "Download PDF")))
    time.sleep(2)
    #5-3. Get part of link to be used in post action from "Download PDF" button
    print ("#5-3. Get part of link to be used in post action from 'Download PDF' button")
    DL_LINK_LOW = driver.find_element_by_link_text("Download PDF").get_attribute("href")
    p_LOW = re.compile("(?<=pdf=)(.+)")
    m_LOW = p_LOW.search(DL_LINK_LOW)
    myPDFinfo_LOW = m_LOW.group(0)
    #5-4. Get part of DSMO url
    print ("#5-4. Get part of DSMO url")
    p2_LOW = re.compile("^http(.+)/")
    m2_LOW = p2_LOW.search(DL_LINK_LOW)
    myDSMOURL_LOW = m2_LOW.group(0)
    #5-5. Click OK button to close dialog
    print ("#5-5. Click OK button to close dialog")
    driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
    return (DL_LINK_LOW, myDSMOURL_LOW, myPDFinfo_LOW)
    # **********************************************************************************************

def outputPDF_HIGH():
    global DL_LINK_HIGH
    global myDSMOURL_HIGH
    global myPDFinfo_HIGH
    # **********************************************************************************************
    #5. Output - Default
    print ("#5. Output")
    #5-6. Open File menu and click "Create #print PDF"
    print ("#5-6. Open File menu and click 'Create #print PDF'")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[7]").click()
    #5-7. Wait PDF rendering until 'Download PDF' link appear
    print ("#5-7. Wait PDF rendering until 'Download PDF' link appear")
    WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.LINK_TEXT, "Download PDF")))
    time.sleep(2)
    #5-8. Get part of link to be used in post action from "Download PDF" button
    print ("#5-8. Get part of link to be used in post action from 'Download PDF' button")
    DL_LINK_HIGH = driver.find_element_by_link_text("Download PDF").get_attribute("href")
    p_HIGH = re.compile("(?<=pdf=)(.+)")
    m_HIGH = p_HIGH.search(DL_LINK_HIGH)
    myPDFinfo_HIGH = m_HIGH.group(0)
    #5-9. Get part of DSMO url
    print ("#5-9. Get part of DSMO url")
    p2_HIGH = re.compile("^http(.+)/")
    m2_HIGH = p2_HIGH.search(DL_LINK_HIGH)
    myDSMOURL_HIGH = m2_HIGH.group(0)
    #5-10. Click OK button to close dialog
    print ("#5-10. Click OK button to close dialog")
    driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
    return (DL_LINK_HIGH, myDSMOURL_HIGH, myPDFinfo_HIGH)
    # **********************************************************************************************

def outputPDFCustom_LOW_CUSTOM():
    global DL_LINK_LOW_CUSTOM
    global myDSMOURL_LOW_CUSTOM
    global myPDFinfo_LOW_CUSTOM
    # **********************************************************************************************
    #8. Output - Custom Document Bleed for #print
    print ("#8. Output")
    #8-1. Open File menu and click "Create preview PDF"
    print ("#8-1. Open File menu and click 'Create preview PDF'")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[6]").click()
    #8-2. Wait PDF rendering until 'Download PDF' link appear
    print ("#8-2. Wait PDF rendering until 'Download PDF' link appear")
    WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.LINK_TEXT, "Download PDF")))
    time.sleep(2)
    #8-3. Get part of link to be used in post action from "Download PDF" button
    print ("#8-3. Get part of link to be used in post action from 'Download PDF' button")
    DL_LINK_LOW_CUSTOM = driver.find_element_by_link_text("Download PDF").get_attribute("href")
    p_LOW_CUSTOM = re.compile("(?<=pdf=)(.+)")
    m_LOW_CUSTOM = p_LOW_CUSTOM.search(DL_LINK_LOW_CUSTOM)
    myPDFinfo_LOW_CUSTOM = m_LOW_CUSTOM.group(0)
    #8-4. Get part of DSMO url
    print ("#8-4. Get part of DSMO url")
    p2_LOW_CUSTOM = re.compile("^http(.+)/")
    m2_LOW_CUSTOM = p2_LOW_CUSTOM.search(DL_LINK_LOW_CUSTOM)
    myDSMOURL_LOW_CUSTOM = m2_LOW_CUSTOM.group(0)
    #8-5. Click OK button to close dialog
    print ("#8-5. Click OK button to close dialog")
    driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
    return (DL_LINK_LOW_CUSTOM, myDSMOURL_LOW_CUSTOM, myPDFinfo_LOW_CUSTOM)
    # **********************************************************************************************

def outputPDFCustom_HIGH_CUSTOM():
    global DL_LINK_HIGH_CUSTOM
    global myDSMOURL_HIGH_CUSTOM
    global myPDFinfo_HIGH_CUSTOM
    # **********************************************************************************************
    #8. Output - Custom Document Bleed for #print
    print ("#8. Output")
    #8-6. Open File menu and click "Create #print PDF"
    print ("#8-6. Open File menu and click 'Create #print PDF'")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[7]").click()
    #5-7. Wait PDF rendering until 'Download PDF' link appear
    print ("#5-7. Wait PDF rendering until 'Download PDF' link appear")
    WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.LINK_TEXT, "Download PDF")))
    time.sleep(2)
    #8-8. Get part of link to be used in post action from "Download PDF" button
    print ("#8-8. Get part of link to be used in post action from 'Download PDF' button")
    DL_LINK_HIGH_CUSTOM = driver.find_element_by_link_text("Download PDF").get_attribute("href")
    p_HIGH_CUSTOM = re.compile("(?<=pdf=)(.+)")
    m_HIGH_CUSTOM = p_HIGH_CUSTOM.search(DL_LINK_HIGH_CUSTOM)
    myPDFinfo_HIGH_CUSTOM = m_HIGH_CUSTOM.group(0)
    #8-9. Get part of DSMO url
    print ("#8-9. Get part of DSMO url")
    p2_HIGH_CUSTOM = re.compile("^http(.+)/")
    m2_HIGH_CUSTOM = p2_HIGH_CUSTOM.search(DL_LINK_HIGH_CUSTOM)
    myDSMOURL_HIGH_CUSTOM = m2_HIGH_CUSTOM.group(0)
    #8-10. Click OK button to close dialog
    print ("#8-10. Click OK button to close dialog")
    driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
    time.sleep(3)
    return (DL_LINK_HIGH_CUSTOM, myDSMOURL_HIGH_CUSTOM, myPDFinfo_HIGH_CUSTOM)
    # **********************************************************************************************

def cleanupCanvas():
	# **********************************************************************************************
	#6-1. Clean Up - All Content on Canvas
	print ("#6-1. Clean Up - All Content on Canvas")
	driver.find_element_by_css_selector(".scComposition").click()
	driver.find_element_by_css_selector(".scComposition").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_css_selector(".scComposition").send_keys(Keys.DELETE)
	time.sleep(2)

def cleanupFonts(self):
    testFont001="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-Bd.otf" 
    testFont002="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-BdIt.otf" 
    testFont003="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-Lt.otf" 
    testFont004="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-LtIt.otf" 
    elem_path_001 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div/div/span"
    elem_path_002 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[2]/div/span"
    elem_path_003 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[3]/div/span"
    elem_path_004 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[4]/div/span"
    testFontName001="Helvetica Neue LT Std -Bold" 
    testFontName002="Helvetica Neue LT Std -Bold Italic" 
    testFontName003="Helvetica Neue LT Std -Light" 
    testFontName004="Helvetica Neue LT Std -Light Italic"
    #6-2. Clean Up - Manage fonts
    print ("#6-2. Manage fonts")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
    time.sleep(2)
    for i in range(self):
        print ("#6-3. Delete Font - " + "{0:03d}".format(i+1)+ "/" + format(self))
        driver.find_element_by_css_selector("span.title").click()
        driver.find_element_by_css_selector("div.scListBtn").click()
        print ("#6-4. Confirm deletion on alert window")
        alert = driver.switch_to_alert()
        alert.accept()
        time.sleep(2)
    print ("#6-5. Close dialog")
    driver.find_element_by_xpath("//div[10]/div[3]/div").click()
    time.sleep(2)
    # **********************************************************************************************

def cleanupDocumentSetting():
	# **********************************************************************************************
	#9. Clean Up - Document settings
	print ("#9. Clean Up - Document settings")
	driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
	time.sleep(1)
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
	print ("#9-1. Clean Up - Document settings")
	# ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
	elem_css = "div.scCol-8 > div.scFormCtrl > div.scCtrlDropdown > i.icon-DS_ArrowDown"
	driver.find_element_by_css_selector(elem_css).location_once_scrolled_into_view
	# Margin 
	print ("#9-2. Clean Up - Document settings -- Margin")
	for i in range(4):
		elem_xpath = "(//input[@type='text'])["+str(i+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).clear()
		driver.find_element_by_xpath(elem_xpath).send_keys("0")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	# Print bleed area
	print ("#9-3. Clean Up - Document settings -- Print bleed area")
	# ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
	elem_xpath = "(//input[@type='text'])["+str(4+int(CurrentInputNumber)) +"]"
	driver.find_element_by_xpath(elem_xpath).location_once_scrolled_into_view
	for i in range(4):
		# Print bleed area
		elem_xpath = "(//input[@type='text'])["+str(i+4+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).clear()
		driver.find_element_by_xpath(elem_xpath).send_keys("0")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	# Activate '#print bleed active' option
	print ("#9-3. Clean Up - Document settings -- Deactivate 'Print bleed active' option")
	driver.find_element_by_xpath("//div/main_view/div/div[34]/div[2]/div/div[13]/div/checkbox_ctrl/div/div/div").click()
	time.sleep(1)
	# Display bleed and offset
	for i in range(6):
		elem_xpath = "(//input[@type='text'])["+str(i+8+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).clear()
		driver.find_element_by_xpath(elem_xpath).send_keys("0")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	#8-5. DSMI document for preview rendering
	#driver.find_element_by_xpath("//div[23]/div/checkbox_ctrl/div/div/div").click()
	time.sleep(1)
	#8-6. Close dialog
	print ("#8-6. Close dialog")
	driver.find_element_by_xpath("//div[34]/div[3]/div").click()
	time.sleep(1)
	# **********************************************************************************************

def printPDF(PageNumber):
	# *********************************************************************************
	#18. Preview PDF: Open DSMO GetImage interface converted PDF page as png and jpg 
	print ("#18. Preview PDF: Open DSMO GetImage interface converted PDF page to PNG and JPG")
	# ************************************
	# Loop as page number amount - optional for multi page PDF support
	for i in range(PageNumber):
	# ************************************
		# Loop for Low & High Resolution PDF Output verification
		for j in range(2):
			# Link Generation - Low Resolution
			if str(j) == "0":
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#1. LowRes PDF as - PNG
						print ("#1. LowRes PDF as - PNG - 600px")
						url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#2. LowRes PDF as - JPG
						print ("#2. LowRes PDF as - JPG - 600px")
						url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
			# Link Generation - High Resolution
			else:
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#3. HighRes PDF as PNG
						print ("#3. HighRes PDF as PNG - 1500px")
						url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#4. HighRes PDF as JPG
						print ("#4. HighRes PDF as JPG - 1500px")
						url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)

	#5 Preview Download URL
	print ("#5 Preview Download URL")
	print ("#Preview Download Link: " + DL_LINK_LOW)
	#6 Print Download URL
	print ("#6 Print Download URL")
	print ("#Print Download Link: " + DL_LINK_HIGH)
	# *********************************************************************************

def printPDFCustom(PageNumber):
	# *********************************************************************************
	#18. Preview PDF: Open DSMO GetImage interface converted PDF page as png and jpg 
	print ("#18. Preview PDF: Open DSMO GetImage interface converted PDF page to PNG and JPG")
	print ("     Repeat 2 times for  - Print Bleed Active Option test")
	# ************************************
	# Loop as page number amount - optional for multi page PDF support
	for i in range(PageNumber):
	# ************************************
		# Loop for Low & High Resolution PDF Output verification
		for j in range(2):
			# Link Generation - Low Resolution
			if str(j) == "0":
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#1. LowRes PDF as - PNG
						print ("#1. LowRes PDF as - PNG - 600px")
						url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#2. LowRes PDF as - JPG
						print ("#2. LowRes PDF as - JPG - 600px")
						url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
			# Link Generation - High Resolution
			else:
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#3. HighRes PDF as PNG
						print ("#3. HighRes PDF as PNG - 1500px")
						url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#4. HighRes PDF as JPG
						print ("#4. HighRes PDF as JPG - 1500px")
						url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)


	# ************************************
	# Loop as page number amount - optional for multi page PDF support
	for i in range(PageNumber):
	# ************************************
		# Loop for Low & High Resolution PDF Output verification
		for j in range(2):
			# Link Generation - Low Resolution
			if str(j) == "0":
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#1. LowRes PDF as - PNG - PrintBleedActive
						print ("#1. LowRes PDF as - PNG - 600px - PrintBleedActive")
						url = myDSMOURL_LOW_CUSTOM + "GetImage.ashx?img=" + myPDFinfo_LOW_CUSTOM + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#2. LowRes PDF as - JPG - PrintBleedActive
						print ("#2. LowRes PDF as - JPG - 600px - PrintBleedActive")
						url = myDSMOURL_LOW_CUSTOM + "GetImage.ashx?img=" + myPDFinfo_LOW_CUSTOM + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
			# Link Generation - High Resolution
			else:
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#3. HighRes PDF as PNG - PrintBleedActive
						print ("#3. HighRes PDF as PNG - 1500px - PrintBleedActive")
						url = myDSMOURL_HIGH_CUSTOM + "GetImage.ashx?img=" + myPDFinfo_HIGH_CUSTOM + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#4. HighRes PDF as JPG - PrintBleedActive
						print ("#4. HighRes PDF as JPG - 1500px - PrintBleedActive")
						url = myDSMOURL_HIGH_CUSTOM + "GetImage.ashx?img=" + myPDFinfo_HIGH_CUSTOM + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)

	#5 Preview Download URL
	print ("#5 Preview Download URL")
	print ("#Preview Download Link: " + DL_LINK_LOW)
	#6 Print Download URL
	print ("#6 Print Download URL")
	print ("#Print Download Link: " + DL_LINK_HIGH)
	#5 Preview Download URL - PrintBleedActive
	print ("#5 Preview Download URL - PrintBleedActive")
	print ("#Preview Download Link: " + DL_LINK_LOW_CUSTOM)
	#6 Print Download URL - PrintBleedActive
	print ("#6 Print Download URL - PrintBleedActive")
	print ("#Print Download Link: " + DL_LINK_HIGH_CUSTOM)
	# *********************************************************************************

def addItems():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3. Add items
	print ("#3. Add items")
	#3-1. Add Text frame, and Edit
	print ("#3-1. Add Text frame, and Edit")
	driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
	driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
	if TestOnSauceLabs == True:
		print ("#3-2-1. Wait dynamically about appearance of next item due to execution on SauceLabs")
		elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
		WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
		driver.find_element_by_css_selector(elem_css).click()
	else:
		print ("#3-2-1. Skip dynamically waiting about appearance of next item due to local execution")
	time.sleep(3)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys("Hello [[FirstName]]!")
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(1)
	#3-2. Change font size
	print ("#3-2. Change font size")
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
	time.sleep(1)
	#3-3. Add Headline frame, and Edit
	print ("#3-3. Add Headline frame, and Edit")
	driver.find_element_by_css_selector("i.icon-DS_Headline").click()
	time.sleep(1)
	elem_path="//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]"
	driver.find_element_by_xpath(elem_path).click()
	time.sleep(3)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys("This is my Headline")
	time.sleep(1)
    #3-4. Add Image
	print ("#3-4. Add Image")
	driver.find_element_by_css_selector("i.icon-DS_Image").click()
	elem_css = "div.image > img"
	WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
	time.sleep(1)
	driver.find_element_by_css_selector(elem_css).click()
	#3-4-1. Move Imagge element
	print ("#3-4-1. Move Imagge element")
	#3-4-2-1. Open 'Layout' context tab menu
	print ("#3-4-2-1. Open 'Layout' context tab menu")
	elem_css = "ul.scContextTabs > li + li + li"
	WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
	driver.find_element_by_css_selector(elem_css).click()
	#3-4-2-2. Set X position of the element - 'X = 8 inch'
	print("#3-4-2-2. Set X position of the element - 'X = 8 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("8")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-4-2-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-4-2-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	#3-5. QRCode and click OK on modal dialog
	print ("#3-5. QRCode and click OK on modal dialog")
	driver.find_element_by_css_selector("i.icon-DS_QRcode").click()
	driver.find_element_by_css_selector("div.scCol-8 > div.scFormCtrl.sc100PercentWidth > div.scCtrlInput > input[type='text']").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_css_selector("div.scCol-8 > div.scFormCtrl.sc100PercentWidth > div.scCtrlInput > input[type='text']").send_keys("www.directsmile.com")
	driver.find_element_by_xpath("//div[38]/div[3]/div").click()
	#3-5-2. Move QRCode element
	print("#3-5-2. Move QRCode element")
	#3-5-2-2. Set X position of the element - 'X = 4 inch'
	print("#3-5-2-2. Set X position of the element - 'X = 4 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("4")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-5-2-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-5-2-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	#3-6-1. Add Rectangle Box element
	print("#3-6-1. Add Rectangle Box element")
	driver.find_element_by_css_selector("i.icon-DS_Checkbox").click()
	#3-6-2. Set X position of the element - 'X = 0.5 inch'
	print("#3-6-2. Set X position of the element - 'X = 0.5 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("0.5")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-6-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-6-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	#3-7-1. Add Ellipse element
	print("#3-7-1. Add Ellipse element")
	driver.find_element_by_css_selector("i.icon-DS_Ellipse").click()
	#3-7-2. Set X position of the element - 'X = 1 inch'
	print("#3-7-2. Set X position of the element - 'X = 1 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("1")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-7-3. Set Y position of the element - 'Y = 5 inch'
	print("#3-7-3. Set Y position of the element - 'Y = 5 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("5")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	#3-7-1. Add Line element
	print("#3-7-1. Add Line element")
	driver.find_element_by_css_selector("i.icon-DS_Line").click()
	#3-7-2. Set X position of the element - 'X = 8 inch'
	print("#3-7-2. Set X position of the element - 'X = 8 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("8")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-7-3. Set Y position of the element - 'Y = 4 inch'
	print("#3-7-3. Set Y position of the element - 'Y = 4 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("4")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	time.sleep(1)
	# **********************************************************************************************

def addItems_TEXT():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3. Add items
	print ("#3. Add items")
	#3-1. Add Text frame, and Edit
	print ("#3-1. Add Text frame, and Edit")
	driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
	driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()

def editItems_TEXT():
	global TestOnSauceLabs
    # **********************************************************************************************
	if TestOnSauceLabs == True:
		print ("#3-2-1. Wait dynamically about appearance of next item due to execution on SauceLabs")
		elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
		WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
		driver.find_element_by_css_selector(elem_css).click()
	else:
		print ("#3-2-1. Skip dynamically waiting about appearance of next item due to local execution")
	time.sleep(3)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(2)
	driver.find_element_by_xpath("//div").send_keys("Hello [[FirstName]]!")
	time.sleep(2)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(2)
	#3-2. Change font size
	print ("#3-2. Change font size")
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
	time.sleep(2)
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
	time.sleep(2)
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
	time.sleep(2)
	# **********************************************************************************************

def addItems_Headline():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-3. Add Headline frame, and Edit
	print ("#3-3. Add Headline frame, and Edit")
	driver.find_element_by_css_selector("i.icon-DS_Headline").click()
	time.sleep(1)
	elem_path="//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]"
	driver.find_element_by_xpath(elem_path).click()
	time.sleep(3)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys("This is my Headline")
	time.sleep(1)
	driver.find_element_by_xpath("//div").click()
	# **********************************************************************************************

def addItems_Image():
	global TestOnSauceLabs
    # **********************************************************************************************
    #3-4. Add Image
	print ("#3-4. Add Image")
	driver.find_element_by_css_selector("i.icon-DS_Image").click()
	elem_css = "div.image > img"
	WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
	time.sleep(1)
	driver.find_element_by_css_selector(elem_css).click()
	time.sleep(1)
	# **********************************************************************************************

def moveItems_Image():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-4-1. Move Imagge element
	print ("#3-4-1. Move Imagge element")
	#3-4-2-1. Open 'Layout' context tab menu
	print ("#3-4-2-1. Open 'Layout' context tab menu")
	elem_css = "ul.scContextTabs > li + li + li"
	WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
	driver.find_element_by_css_selector(elem_css).click()
	#3-4-2-2. Set X position of the element - 'X = 8 inch'
	print("#3-4-2-2. Set X position of the element - 'X = 8 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("8")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-4-2-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-4-2-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	# **********************************************************************************************

def addItems_QRCode():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-5. QRCode and click OK on modal dialog
	print ("#3-5. QRCode and click OK on modal dialog")
	driver.find_element_by_css_selector("i.icon-DS_QRcode").click()
	driver.find_element_by_css_selector("div.scCol-8 > div.scFormCtrl.sc100PercentWidth > div.scCtrlInput > input[type='text']").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_css_selector("div.scCol-8 > div.scFormCtrl.sc100PercentWidth > div.scCtrlInput > input[type='text']").send_keys("www.directsmile.com")
	driver.find_element_by_xpath("//div[38]/div[3]/div").click()
	# **********************************************************************************************

def moveItems_QRCode():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-5-2. Move QRCode element
	print("#3-5-2. Move QRCode element")
	#3-5-2-2. Set X position of the element - 'X = 4 inch'
	print("#3-5-2-2. Set X position of the element - 'X = 4 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("4")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-5-2-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-5-2-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	# **********************************************************************************************

def addItems_Rectangle():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-6-1. Add Rectangle Box element
	print("#3-6-1. Add Rectangle Box element")
	driver.find_element_by_css_selector("i.icon-DS_Checkbox").click()
	# **********************************************************************************************

def moveItems_Rectangle():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-6-2. Set X position of the element - 'X = 0.5 inch'
	print("#3-6-2. Set X position of the element - 'X = 0.5 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("0.5")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-6-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-6-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	# **********************************************************************************************

def addItems_Ellipse():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-7-1. Add Ellipse element
	print("#3-7-1. Add Ellipse element")
	driver.find_element_by_css_selector("i.icon-DS_Ellipse").click()
	# **********************************************************************************************

def moveItems_Ellipse():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-7-2. Set X position of the element - 'X = 1 inch'
	print("#3-7-2. Set X position of the element - 'X = 1 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("1")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-7-3. Set Y position of the element - 'Y = 5 inch'
	print("#3-7-3. Set Y position of the element - 'Y = 5 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("5")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	# **********************************************************************************************

def addItems_Line():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-7-1. Add Line element
	print("#3-7-1. Add Line element")
	driver.find_element_by_css_selector("i.icon-DS_Line").click()
	# **********************************************************************************************

def moveItems_Line():
	global TestOnSauceLabs
    # **********************************************************************************************
	#3-7-2. Set X position of the element - 'X = 8 inch'
	print("#3-7-2. Set X position of the element - 'X = 8 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("8")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-7-3. Set Y position of the element - 'Y = 4 inch'
	print("#3-7-3. Set Y position of the element - 'Y = 4 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("4")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	time.sleep(1)
	# **********************************************************************************************

def setDocument():
	global CurrentInputNumber
    # **********************************************************************************************
	#4. Document settings
	print ("#4. Document settings")
	driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
	time.sleep(1)
	#4-1. Configure "Document Setting"
	#Ver7.3 - [150]
	CurrentInputNumber = "150"
	# Margin 
	for i in range(4):
		elem_xpath = "(//input[@type='text'])["+str(i+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.CONTROL, "a")
		driver.find_element_by_xpath(elem_xpath).send_keys("1")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	#4-2. Activate '#print bleed active' option
	print ("#4-2. Activate '#print bleed active' option")
	elem_path = "//div/main_view/div/div[34]/div[2]/div/div[13]/div/checkbox_ctrl/div/div/div"
	driver.find_element_by_xpath(elem_path).click()
	time.sleep(1)
	# Print bleed area
	# ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
	elem_xpath = "(//input[@type='text'])["+str(i+4+int(CurrentInputNumber)) +"]"
	driver.find_element_by_xpath(elem_xpath).location_once_scrolled_into_view
	for i in range(4):
		# Print bleed area
		elem_xpath = "(//input[@type='text'])["+str(i+4+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.CONTROL, "a")
		driver.find_element_by_xpath(elem_xpath).send_keys("1")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	# Display bleed and offset
	for i in range(6):
		elem_xpath = "(//input[@type='text'])["+str(i+8+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.CONTROL, "a")
		driver.find_element_by_xpath(elem_xpath).send_keys("1")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	#4-5. DSMI document for preview rendering
	#driver.find_element_by_xpath("//div[23]/div/checkbox_ctrl/div/div/div").click()
	time.sleep(1)
	#4-6. Close dialog
	print ("#4-6. Close dialog")
	driver.find_element_by_xpath("//div[34]/div[3]/div").click()
	time.sleep(1)
	return CurrentInputNumber

def exitCanvas():
	# **********************************************************************************************
	#11. Exit
	print ("#11. Exit")
	driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[12]/span").click()
	time.sleep(1)
	#11-1. Click OK to save unsaved change alert dialog
	print ("#11-1. Click OK to save unsaved change alert dialog")
	elem_path = "//div[8]/div[3]/div/div"
	driver.find_element_by_xpath(elem_path).click()
	# **********************************************************************************************

def quitDriver():
    # **********************************************************************************************
    #15. Close
    print ("#15. Close")
    driver.quit()
    # **********************************************************************************************

def DIRSMILE_173():
    # **********************************************************
    #3. Document settings
    print ("#15. Document settings")
    time.sleep(1)
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
    #3-1. Assert Print Bleed area text
    element = driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[34]/div[2]/div/div[12]/div")
    assert element.text == "Print bleed area"
    time.sleep(1)

def DIRSMILE_181_CreateImp():
    # **********************************************************************************************
    #3. Manage impositions
    print ("#3. Manage impositions")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[10]").click()
    time.sleep(2)
    print ("#3-1. Create new Imposition")
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[12]/div[2]/div/div/div/div[2]/div").click()
    # **********************************************************************************************
    #3-2. Set Imposition's Display Name
    print ("#3-2. Set Imposition's Display Name")
    driver.find_element_by_xpath("(//input[@type='text'])[67]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[67]").send_keys("DevOps Imposition")
    driver.find_element_by_xpath("(//input[@type='text'])[67]").send_keys(Keys.RETURN)
    time.sleep(1)
    # **********************************************************************************************
    #3-3. Set Imposition's Description
    print ("#3-3. Set Imposition's Description")
    driver.find_element_by_xpath("(//input[@type='text'])[68]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[68]").send_keys("DevOps Imposition description is here")
    driver.find_element_by_xpath("(//input[@type='text'])[68]").send_keys(Keys.RETURN)
    time.sleep(1)
    # **********************************************************************************************
    #3-4. Click Create button
    print ("#3-4. Click Create button")
    driver.find_element_by_css_selector("div.scSimpleListButtonBar > div.scBtn").click()
    time.sleep(3)
    # **********************************************************************************************
    #3-5. Set sheet Width = 15 inch
    print ("#3-5. Set sheet Width = 15 inch")
    driver.find_element_by_xpath("(//input[@type='text'])[74]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[74]").send_keys("15")
    driver.find_element_by_xpath("(//input[@type='text'])[74]").send_keys(Keys.RETURN)
    # **********************************************************************************************
    #3-6. Set sheet Height = 20 inch
    print ("#3-6. Set sheet Height = 20 inch")
    driver.find_element_by_xpath("(//input[@type='text'])[75]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[75]").send_keys("20")
    driver.find_element_by_xpath("(//input[@type='text'])[75]").send_keys(Keys.RETURN)
    # **********************************************************************************************
    #3-7. Close Imposition Editor dialog
    print ("#3-7. Close Imposition Editor dialog")
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[14]/div[3]/div").click()
    # **********************************************************************************************

def DIRSMILE_181_Assert():
    # **********************************************************************************************
    #4-1. Assert Created Imposition
    print ("#4-1. Assert Created Imposition")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[10]").click()
    time.sleep(2)
    #4-2. Select created imposition
    print ("#4-2. Select created imposition")
    elem_path = "//div[@id='scContainerCtrl']/main_view/div/div[12]/div[2]/div/div/div/div/div/div/span"
    driver.find_element_by_xpath(elem_path).click()
    #driver.find_element_by_css_selector("div.content").click()
    time.sleep(1)
    #4-3. Enable 'Default print imposition'
    print ("#4-2. Select created imposition")
    driver.find_element_by_xpath("//div[2]/div/div/div[11]/checkbox_ctrl/div/div/div/span").click()
    #4-4. Enable 'Default preview imposition'
    print ("#4-2. Select created imposition")
    driver.find_element_by_xpath("//div[2]/div/div/div[12]/checkbox_ctrl/div/div/div/span").click()
    time.sleep(1)
    #4-5. Close Imposition Manager dialog
    print ("#4-5. Close Imposition Manager dialog")
    driver.find_element_by_xpath("//div[12]/div[3]/div").click()

def DIRSMILE_181_CleanupImposition():
    # **********************************************************************************************
    #5-1. Clean Up Imposition
    print ("#5-1. Assert Created Imposition")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[10]").click()
    time.sleep(2)
    #5-2. Select created imposition
    print ("#5-2. Select created imposition")
    elem_path = "//div[@id='scContainerCtrl']/main_view/div/div[12]/div[2]/div/div/div/div/div/div/span"
    driver.find_element_by_xpath(elem_path).click()
    #driver.find_element_by_css_selector("div.content").click()
    time.sleep(1)
    #5-3. Disable 'Default print imposition'
    print ("#5-3. Select created imposition")
    driver.find_element_by_xpath("//div[2]/div/div/div[11]/checkbox_ctrl/div/div/div/span").click()
    #5-4. Disable 'Default preview imposition'
    print ("#5-4. Select created imposition")
    driver.find_element_by_xpath("//div[2]/div/div/div[12]/checkbox_ctrl/div/div/div/span").click()
    time.sleep(1)
    #5-5. Delete Imposition file
    print ("#5-5. Close Imposition Manager dialog")
    driver.find_element_by_xpath("//div[12]/div[2]/div/div/div/div[2]/div[2]").click()
    #5-6. Confirm deletion on alert window
    print ("#5-6. Confirm deletion on alert window")
    alert = driver.switch_to_alert()
    alert.accept()
    #5-7. Close Imposition Manager dialog
    print ("#5-7. Close Imposition Manager dialog")
    driver.find_element_by_xpath("//div[12]/div[3]/div").click()
    # **********************************************************************************************

def DIRSMILE_361_AddEdit(): 
    # **********************************************************************************************
    #3-1. Add Text frame, and Edit
    print ("#3-1. Add Text frame, and Edit")
    driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
    time.sleep(1)
    driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys("Hello [[FirstName]]!")
    time.sleep(1)
    driver.find_element_by_xpath("//div").click()
    time.sleep(1)
    # **********************************************************************************************

def DIRSMILE_361_OpenBuyerMode(): 
    # **********************************************************************************************
    # Check Issue fix thru Buyer mode
    print ("# Check Issue fix thru Buyer mode")
    # Open View menu
    driver.find_element_by_xpath("//menu_panel/div/ul/li[4]/span").click()
    time.sleep(1)
    # Click 'Buyer mode view'
    driver.find_element_by_xpath("//li[4]/ul/li[6]").click()
    time.sleep(1)
    # **********************************************************************************************

def DIRSMILE_361_EditTextOnBuyerView(): 
    # **********************************************************************************************
    #3-2. Select Text frame, and Edit again
    # Click existing text frame
    driver.find_element_by_xpath("//div/div/div/div/div/div/div/div[2]/div/div").click()
    time.sleep(1)
    # Click Edit button
    driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]").click()
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").send_keys(Keys.DELETE)
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys("DevOps team was here")
    time.sleep(1)
    driver.find_element_by_xpath("//div").click()
    # **********************************************************************************************

def DIRSMILE_361_EditTextOnBuyerViewSecondTime(): 
    # **********************************************************************************************
    #3-2. Select Text frame, and Edit again
    # Click existing text frame
    driver.find_element_by_xpath("//div/div/div/div/div/div/div/div[2]/div/div").click()
    time.sleep(1)
    # Click Edit button
    driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]").click()
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").send_keys(Keys.DELETE)
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys("DevOps team was here, and edited twice!")
    time.sleep(1)
    driver.find_element_by_xpath("//div").click()
    time.sleep(1)
    # **********************************************************************************************

def DIRSMILE_361_CloseBuyerMode(): 
    # **********************************************************************************************
    # Close 'Buyer mode view'
    print ("close Buyer mode")
    driver.find_element_by_css_selector("div.button > i.icon-DS_Close").click()
    time.sleep(1)
    # **********************************************************************************************

def DIRSMILE_385_AddItem(myBrowser):
    # **********************************************************************************************
    #3. Add items
    print ("#3. Add items")
    # Loop Item addition and property customization action
    # Loop 15 times
    for h in range(2):
        # Repeat line addition twice
        # Change color and point size by boolean for output comparision
        for i in range(15):
            #3-1. Add Line element
            LineNumber = ((h*2)+i+1)
            print("#3-1-" + str(LineNumber) + ". Add Line element")
            driver.find_element_by_css_selector("i.icon-DS_Line").click()
            #3-2. Set X position of the element - 'X = 0.5 inch'
            print("#3-2. Set X position of the element - 'X = 0.5 inch'")
            #Set xPath condition
            if myBrowser == "chrome":
                # Chrome xpath
                elem_path="//div[4]/div/div/div[2]/div/div/text_ctrl/div/div[2]/div/div[2]/input"
            else:
                elem_path="(//input[@type='text'])[23]"
            driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
            time.sleep(1)
            driver.find_element_by_xpath(elem_path).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(elem_path).send_keys("0.5")
            driver.find_element_by_xpath(elem_path).send_keys(Keys.RETURN)
            #3-3. Set Y position of the element - 'Y = 0.5 inch'
            YPos = 0.5 + 0.5*i
            print("#3-3. Set Y position of the element - 'Y = " + str(YPos) + " inch'")
            #Set xPath condition
            if myBrowser == "chrome":
                # Chrome xpath
                elem_path="//div[2]/div/div[2]/text_ctrl/div/div[2]/div/div[2]/input"
            else:
                # FireFox xpath
                elem_path="(//input[@type='text'])[24]"
            driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
            time.sleep(1)
            driver.find_element_by_xpath(elem_path).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(elem_path).send_keys(str(YPos))
            driver.find_element_by_xpath(elem_path).send_keys(Keys.RETURN)
            #3-4. Set Width of the element - 'W = 10 inch'
            print("#3-4. Set Width of the element - 'W = 10 inch'")
            #Set xPath condition
            if myBrowser == "chrome":
                # Chrome xpath
                elem_path="//div[4]/div/div/div[2]/div[2]/div/text_ctrl/div/div[2]/div/div[2]/input"
            else:
                # FireFox xpath
                elem_path="(//input[@type='text'])[25]"
            driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
            time.sleep(1)
            driver.find_element_by_xpath(elem_path).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(elem_path).send_keys("10")
            driver.find_element_by_xpath(elem_path).send_keys(Keys.RETURN)
            #3-5. Set Height of the element - H = 0.5 inch'
            print("#3-5. Set Height of the element - H = 0.5 inch'")
            #Set xPath condition
            if myBrowser == "chrome":
                # Chrome xpath
                elem_path="//div[2]/div[2]/text_ctrl/div/div[2]/div/div[2]/input"
            else:
                # FireFox xpath
                elem_path="(//input[@type='text'])[26]"
            driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
            time.sleep(1)
            driver.find_element_by_xpath(elem_path).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(elem_path).send_keys("0.5")
            driver.find_element_by_xpath(elem_path).send_keys(Keys.RETURN)
            #3-6. Set Point to line
            if str(h) == "0":
                #Set xPath condition
                if myBrowser == "chrome":
                    # Chrome xpath
                    elem_path="//div[3]/div[2]/div/div[2]/text_ctrl/div/div/div/div[3]/input"
                else:
                    # FireFox xpath
                    elem_path="(//input[@type='text'])[32]"
                LinePoint = 1 + 0.25*i
                print("#3-6. Set Point to line - 'Point = "+str(LinePoint)+" point'")
                driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
                time.sleep(1)
                driver.find_element_by_xpath(elem_path).send_keys(Keys.CONTROL, "a")
                driver.find_element_by_xpath(elem_path).send_keys(str(LinePoint))
                driver.find_element_by_xpath(elem_path).send_keys(Keys.RETURN)
            #3-7. Set Position of line
            print("#3-7. Set Position of line")
            # Open Dropdown menu
            elem_css = "div.scCol-9 > div.scFormCtrl > div.scCtrlDropdown > i.icon-DS_ArrowDown"
            #Xpath = "//div[3]/text_ctrl/div/div[2]/div/div[3]/i"
            driver.find_element_by_css_selector(elem_css).click()
            # Select position option from selection
            # - Top
            #css="div.scCol-9 > div.scFormCtrl > div.scDropdownList > div.scDropDownListScroller > ul > li"
            elem_path="//div[3]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
            driver.find_element_by_xpath(elem_path).click()
            # - Bottom
            #Xpath="//div[3]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
            # - Left
            #Xpath="//div[3]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[3]"
            # - Right
            #Xpath="//div[3]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[4]"
            # - Diagonal1
            #Xpath="//div[3]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[5]"
            # - Diagonal2
            #Xpath="//div[3]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[6]"
            #3-8, Condition for point and line setting to compare with red 1pt line as default
            if str(h) == "1":
                print("#3-8, Condition for point and line setting to compare with red 1pt line as default")
                #3-5. Set Color to line
                #Open Color option menu
                elem_path = "//div[3]/div[2]/div/div/colorpicker_ctrl/div/div/div/div[2]/i"
                driver.find_element_by_xpath(elem_path).click()
                #Select RED
                elem_path = "//div[@id='scColorSwatchesListDiv']/div[17]/div[2]/div"
                driver.find_element_by_xpath(elem_path).click()
                #Close Color selection dialog
                elem_css ="div.scListBtn.scBtn"
                driver.find_element_by_css_selector(elem_css).click()
            else:
                #Stay Default
                print("#Stay Default")
    # **********************************************************************************************

def DIRSMILE_431_AddFont(tempdir,partNr):
    # **********************************************************************************************
    #9. Manage fonts
    print ("#9. Manage fonts")
    tempdir = "C:\\temp\\SmartCanvas_FontUpload\\"
    testFont001 = tempdir + "Arial\\arial.ttf" 
    testFont002 = tempdir + "Arial\\arialbd.ttf" 
    testFont003 = tempdir + "Arial\\arialbi.ttf"
    testFont004 = tempdir + "Arial\\ariali.ttf"
    testFont005 = tempdir + "Arial\\ARIALN.TTF" 
    testFont006 = tempdir + "Arial\\ARIALNB.TTF" 
    testFont007 = tempdir + "Arial\\ARIALNBI.TTF"
    testFont008 = tempdir + "Arial\\ARIALNI.TTF"
    testFont009 = tempdir + "Arial\\ARIALUNI.TTF" 
    testFont010 = tempdir + "Arial\\ariblk.ttf" 
    testFont011 = tempdir + "Arial\\ARLRDBD.TTF"
    elem_path_001 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div/div/span"
    elem_path_002 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[2]/div/span"
    elem_path_003 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[3]/div/span"
    elem_path_004 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[4]/div/span"
    elem_path_005 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[5]/div/span"
    elem_path_006 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[6]/div/span"
    elem_path_007 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[7]/div/span"
    elem_path_008 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[8]/div/span"
    elem_path_009 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[9]/div/span"
    elem_path_010 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[10]/div/span"
    elem_path_011 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[11]/div/span"
    testFontName001="Arial" 
    testFontName002="Arial Bold"
    testFontName003="Arial Bold Italic" 
    testFontName004="Arial Italic" 
    testFontName005="Arial Narrow"
    testFontName006="Arial Narrow Bold"
    testFontName007="Arial Narrow Bold Italic"
    testFontName008="Arial Narrow Italic"
    testFontName009="Arial Unicode MS"
    testFontName010="Arial Black"
    testFontName011="Arial Rounded MT Bold"
    testFontNameInDisplayOrder001="Arial" 
    testFontNameInDisplayOrder002="Arial Black"
    testFontNameInDisplayOrder003="Arial Bold Italic" 
    testFontNameInDisplayOrder004="Arial Bold" 
    testFontNameInDisplayOrder005="Arial Italic"
    testFontNameInDisplayOrder006="Arial" 
    testFontNameInDisplayOrder007="Arial Narrow"
    testFontNameInDisplayOrder008="Arial Narrow Bold"
    testFontNameInDisplayOrder009="Arial Narrow Bold Italic"
    testFontNameInDisplayOrder010="Arial Narrow Italic"
    testFontNameInDisplayOrder011="Arial Rounded MT Bold"
    testFontNameInDisplayOrder012="Arial Unicode MS"
    fixSCtemp_elem_path_001 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div"
    fixSCtemp_elem_path_002 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[2]/div"
    fixSCtemp_elem_path_003 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[3]/div"
    fixSCtemp_elem_path_004 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[4]/div"
    fixSCtemp_elem_path_005 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[5]/div"
    fixSCtemp_elem_path_006 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[6]/div"
    fixSCtemp_elem_path_007 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[7]/div"
    fixSCtemp_elem_path_008 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[8]/div"
    fixSCtemp_elem_path_009 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[9]/div"
    fixSCtemp_elem_path_010 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[10]/div"
    fixSCtemp_elem_path_011 = "//div[@id='eins']/main_view/div/div[10]/div[2]/div/div/div/div/div[11]/div"
    #9-1. Upload font
    if partNr == "1":
        # 1st group of the fonts
        print ("# 1st group of the fonts")
        for i in range(3):
            #9-1. Open Menu dropdown
            print ("#9-1. Open Menu dropdown")
            driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
            time.sleep(1)
            #9-2. Open Font Manager Dialog
            print ("#9-2. Open Font Manager Dialog")
            time.sleep(1)
            driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
            print ("#9-3. Set Upload Font path")
            #9-3. Set Upload Font path
            testFont = eval("testFont"+"{0:03d}".format(i+1))
            testFontName = eval("testFontName"+"{0:03d}".format(i+1))
            #9-4. Set Expected Font xpath in the list menu
            print ("#9-4. Set Expected Font xpath in the list menu")
            testFontPath = eval("fixSCtemp_elem_path_"+"{0:03d}".format(i+1))
            print ("#9-5. Uploading font : ---- ---- ---- ---- " + str(testFontName))
            driver.file_detector.is_local_file()
            driver.find_element_by_css_selector('input[name="selectedFonts"]').send_keys(testFont)
            wait = WebDriverWait(driver, 60)
            wait.until(EC.presence_of_element_located((By.XPATH,testFontPath)))
            time.sleep(10)
            print ("#9-6. Close dialog")
            driver.find_element_by_xpath("//div[10]/div[3]/div").click()
            time.sleep(1)
    elif partNr == "2":
        # 3rd group of the fonts
        print ("# 2nd group of the fonts")
        # **********************************************************************************************
        #9-1. Upload font
        for i in range(3):
            #9-1. Open Menu dropdown
            print ("#9-1. Open Menu dropdown")
            driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
            time.sleep(1)
            #9-2. Open Font Manager Dialog
            print ("#9-2. Open Font Manager Dialog")
            time.sleep(1)
            driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
            print ("#9-3. Set Upload Font path")
            #9-3. Set Upload Font path
            testFont = eval("testFont"+"{0:03d}".format(i+4))
            testFontName = eval("testFontName"+"{0:03d}".format(i+4))
            #9-4. Set Expected Font xpath in the list menu
            print ("#9-4. Set Expected Font xpath in the list menu")
            testFontPath = eval("fixSCtemp_elem_path_"+"{0:03d}".format(i+4))
            print ("#9-5. Uploading font : ---- ---- ---- ---- " + str(testFontName))
            driver.file_detector.is_local_file()
            driver.find_element_by_css_selector('input[name="selectedFonts"]').send_keys(testFont)
            wait = WebDriverWait(driver, 60)
            wait.until(EC.presence_of_element_located((By.XPATH,testFontPath)))
            time.sleep(10)
            print ("#9-6. Close dialog")
            driver.find_element_by_xpath("//div[10]/div[3]/div").click()
            time.sleep(1)
    elif partNr == "3":
        # 3rd group of the fonts
        print ("# 3rd group of the fonts")
        # **********************************************************************************************
        #9-1. Upload font
        for i in range(3):
            #9-1. Open Menu dropdown
            print ("#9-1. Open Menu dropdown")
            driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
            time.sleep(1)
            #9-2. Open Font Manager Dialog
            print ("#9-2. Open Font Manager Dialog")
            time.sleep(1)
            driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
            print ("#9-3. Set Upload Font path")
            #9-3. Set Upload Font path
            testFont = eval("testFont"+"{0:03d}".format(i+7))
            testFontName = eval("testFontName"+"{0:03d}".format(i+7))
            #9-4. Set Expected Font xpath in the list menu
            print ("#9-4. Set Expected Font xpath in the list menu")
            testFontPath = eval("fixSCtemp_elem_path_"+"{0:03d}".format(i+7))
            print ("#9-5. Uploading font : ---- ---- ---- ---- " + str(testFontName))
            driver.file_detector.is_local_file()
            driver.find_element_by_css_selector('input[name="selectedFonts"]').send_keys(testFont)
            wait = WebDriverWait(driver, 60)
            wait.until(EC.presence_of_element_located((By.XPATH,testFontPath)))
            time.sleep(10)
            print ("#9-6. Close dialog")
            driver.find_element_by_xpath("//div[10]/div[3]/div").click()
            time.sleep(1)
    elif partNr == "4":
        print ("# 4th group of the fonts")
        # **********************************************************************************************
        #9-1. Upload font
        for i in range(2):
            #9-1. Open Menu dropdown
            print ("#9-1. Open Menu dropdown")
            driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
            time.sleep(1)
            #9-2. Open Font Manager Dialog
            print ("#9-2. Open Font Manager Dialog")
            time.sleep(1)
            driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
            print ("#9-3. Set Upload Font path")
            #9-3. Set Upload Font path
            testFont = eval("testFont"+"{0:03d}".format(i+10))
            testFontName = eval("testFontName"+"{0:03d}".format(i+10))
            #9-4. Set Expected Font xpath in the list menu
            print ("#9-4. Set Expected Font xpath in the list menu")
            testFontPath = eval("fixSCtemp_elem_path_"+"{0:03d}".format(i+10))
            print ("#9-5. Uploading font : ---- ---- ---- ---- " + str(testFontName))
            driver.file_detector.is_local_file()
            driver.find_element_by_css_selector('input[name="selectedFonts"]').send_keys(testFont)
            wait = WebDriverWait(driver, 60)
            wait.until(EC.presence_of_element_located((By.XPATH,testFontPath)))
            time.sleep(10)
            print ("#9-6. Close dialog")
            driver.find_element_by_xpath("//div[10]/div[3]/div").click()
            time.sleep(1)
    else:
        # Fall back selection
        print ("# Upload all fonts")
        # **********************************************************************************************
        #9-1. Upload font
        for i in range(11):
            #9-1. Open Menu dropdown
            print ("#9-1. Open Menu dropdown")
            driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
            time.sleep(1)
            #9-2. Open Font Manager Dialog
            print ("#9-2. Open Font Manager Dialog")
            time.sleep(1)
            driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
            print ("#9-3. Set Upload Font path")
            #9-3. Set Upload Font path
            testFont = eval("testFont"+"{0:03d}".format(i+1))
            testFontName = eval("testFontName"+"{0:03d}".format(i+1))
            #9-4. Set Expected Font xpath in the list menu
            print ("#9-4. Set Expected Font xpath in the list menu")
            testFontPath = eval("fixSCtemp_elem_path_"+"{0:03d}".format(i+1))
            print ("#9-5. Uploading font : ---- ---- ---- ---- " + str(testFontName))
            driver.file_detector.is_local_file()
            driver.find_element_by_css_selector('input[name="selectedFonts"]').send_keys(testFont)
            wait = WebDriverWait(driver, 60)
            wait.until(EC.presence_of_element_located((By.XPATH,testFontPath)))
            time.sleep(10)
            print ("#9-6. Close dialog")
            driver.find_element_by_xpath("//div[10]/div[3]/div").click()
            time.sleep(1)
        # **********************************************************************************************

def DIRSMILE_431_AddItem():
    # **********************************************************************************************
    #3. Add items
    print ("#3. Add items")
    elem_path_001 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div/div/span"
    elem_path_002 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[2]/div/span"
    elem_path_003 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[3]/div/span"
    elem_path_004 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[4]/div/span"
    elem_path_005 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[5]/div/span"
    elem_path_006 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[6]/div/span"
    elem_path_007 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[7]/div/span"
    elem_path_008 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[8]/div/span"
    elem_path_009 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[9]/div/span"
    elem_path_010 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[10]/div/span"
    elem_path_011 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[11]/div/span"
    testFontName001="Arial" 
    testFontName002="Arial Bold"
    testFontName003="Arial Bold Italic" 
    testFontName004="Arial Italic" 
    testFontName005="Arial Narrow"
    testFontName006="Arial Narrow Bold"
    testFontName007="Arial Narrow Bold Italic"
    testFontName008="Arial Narrow Italic"
    testFontName009="Arial Unicode MS"
    testFontName010="Arial Black"
    testFontName011="Arial Rounded MT Bold"
    testFontNameInDisplayOrder001="Arial" 
    testFontNameInDisplayOrder002="Arial Black"
    testFontNameInDisplayOrder003="Arial Bold Italic" 
    testFontNameInDisplayOrder004="Arial Bold" 
    testFontNameInDisplayOrder005="Arial Italic"
    testFontNameInDisplayOrder006="Arial" 
    testFontNameInDisplayOrder007="Arial Narrow"
    testFontNameInDisplayOrder008="Arial Narrow Bold"
    testFontNameInDisplayOrder009="Arial Narrow Bold Italic"
    testFontNameInDisplayOrder010="Arial Narrow Italic"
    testFontNameInDisplayOrder011="Arial Rounded MT Bold"
    testFontNameInDisplayOrder012="Arial Unicode MS"
    for i in range(12):
        #3-1. Set Font Name
        print ("#3-1. Set Font Name")
        testFontName = eval("testFontNameInDisplayOrder"+"{0:03d}".format(i+1))
        print ("#3-1-1. Set Font Name : " + str(testFontName))
        #3-2. Add Text frame, and Edit
        print ("#3-2. Add Text frame, and Edit")
        driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
        driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
        if TestOnSauceLabs == True:
            print ("#3-2-1. Wait dynamically about appearance of next item due to execution on SauceLabs")
            elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
            WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.CSS_SELECTOR, elem_css)))
            driver.find_element_by_css_selector(elem_css).click()
        else:
            print ("#3-2-1. Skip dynamically waiting about appearance of next item due to local execution")
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(testFontName)
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        time.sleep(1)
        #3-3. Change font size
        print ("#3-3. Change font size")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
        time.sleep(2)
        #3-4. Set font style
        print ("#3-4. Set font style")
        #3-5. Open Font selection dropdown
        print ("#3-5. Open Font selection dropdown")
        driver.find_element_by_xpath("//div[2]/div/div[2]/div[2]/div/div/text_ctrl/div/div[2]/div/div[4]/i").click()
        #3-6. Select Font from selection
        print ("#3-6. Select Font from selection")
        targetFontPath_dropdown = "//div[2]/div/div[2]/div[2]/div/div/text_ctrl/div/div[2]/div/div[5]/div/ul/li[" + str(i+1) + "]"
        # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
        driver.find_element_by_xpath(targetFontPath_dropdown).location_once_scrolled_into_view
        driver.find_element_by_xpath(targetFontPath_dropdown).click()
        time.sleep(1)
        #3-7. Position frame
        #print ("#3-7. Position frame")
        driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li").click()
        #3-8. Select Top
        print("#3-8. Select Top")
        driver.find_element_by_css_selector("span.scLayerItemName").click()
        #3-9. Open 'Layout' context tab menu
        print ("#3-9. Open 'Layout' context tab menu")
        driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
        #3-10. Set X position of the element - 'X =< 7 inch'
        #print("#3-10. Set X position of the element - 'X =< 7 inch'")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
        #3-11. Set Y position of the element - 'Y =< 7 inch'
        print("#3-11. Set Y position of the element - 'Y =< 7 inch'")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(0.5+(0.5)*i))
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
        print ("#3-12. Add Text frame, and Edit - " + "{0:03d}".format(i+1)+ "/012")
        time.sleep(1)
    # **********************************************************************************************

def DIRSMILE_431_CleanupFont():
    # **********************************************************************************************
    #6-2. Clean Up - Manage fonts
    print ("#6-2. Manage fonts")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
    time.sleep(2)
    for i in range(11):
        print ("#6-3. Delete Font - " + "{0:03d}".format(i+1)+ "/011")
        driver.find_element_by_css_selector("span.title").click()
        driver.find_element_by_css_selector("div.scListBtn").click()
        print ("#6-4. Confirm deletion on alert window")
        alert = driver.switch_to_alert()
        alert.accept()
        time.sleep(2)
    print ("#6-5. Close dialog")
    driver.find_element_by_xpath("//div[10]/div[3]/div").click()
    time.sleep(2)
    # **********************************************************************************************

def DSF_30343():
    # *********************************************************************************
    print ("#3-1. Store Special Characters Strings")
    # Test 1: ASCII Special Characters in UI
    str001 = u'"This sentence is wrapped in double quotes."'
    str002 = u"'single quotes'"
    str003 = u"[square brackets]"
    str004 = u"{curly braces}"
    str005 = u"<angle brackets>"
    str006 = u"<! -- XML comment -->"
    str007 = u'"quoted" segment & ampersand'
    str008 = u'"A "quoted" segment; & (entity); wrapped in quotes"'
    str009 = u"backslashes and colon: \c:\mydocs"
    str010 = u"back-quote: Hawai`i"
    str011 = u"hash: #20"
    str012 = u"escaped slash: \/"
    # Test 2: Strings of over 500 characters
    str013 = u"START 1) I am typing in more than 500 characters of text to test whether the more than 500 characters are cut off or if they display correctly. 500 characters is a lot of text. 2) I am typing in more than 500 characters of text to test whether the more than 500 characters are cut off or if they display correctly. 500 characters is a lot of text. 3) I am typing in more than 500 characters of text to test whether the more than 500 characters are cut off or if they display correctly. 500 characters is a lot of text. END"
    # Test 3: Non-ASCII Characters in UI
    str014 = u"ñóǹ äŝçíì 汉语/漢語  华语/華語 Huáyǔ; 中文 Zhōngwén 漢字仮名交じり文 Lech Wałęsa æøå"
    # Test 4: ASCII Special Characters and Non-ASCII Characters in fields with autocomplete behavior
    str015 = u"Sanford & Sons"
    str016 = u"O'Reilly"
    str017 = u"Hawai`i"
    str018 = u'"Bull" Connor'
    str019 = u"Lech Wałęsa"
    str020 = u"Herr Müller"
    # *********************************************************************************
    print ("#3-2. Store Special Characters Strings - Expected post-auto corrected FieldForm value")
    # Test 1: ASCII Special Characters in UI
    NoSpecialStr001 = "[[Thissentenceiswrappedindoublequotes.]]"
    NoSpecialStr002 = "[[singlequotes]]"
    NoSpecialStr003 = "[[squarebrackets]]"
    NoSpecialStr004 = "[[curlybraces]]"
    NoSpecialStr005 = "[[anglebrackets]]"
    NoSpecialStr006 = "[[--XMLcomment--]]"
    NoSpecialStr007 = "[[quotedsegmentampersand]]"
    NoSpecialStr008 = "[[Aquotedsegmententitywrappedinquotes]]"
    NoSpecialStr009 = "[[backslashesandcoloncmydocs]]"
    NoSpecialStr010 = "[[back-quoteHawaii]]"
    NoSpecialStr011 = "[[hash20]]"
    NoSpecialStr012 = "[[escapedslash]]"
    # Test 2: Strings of over 500 characters
    NoSpecialStr013 = "[[START1Iamtypinginmorethan500charactersoftexttotestwhetherthemorethan500charactersarecutofforiftheydisplaycorrectly.500charactersisalotoftext.2Iamtypinginmorethan500charactersoftexttotestwhetherthemorethan500charactersarecutofforiftheydisplaycorrectly.500charactersisalotoftext.3Iamtypinginmorethan500charactersoftexttotestwhetherthemorethan500charactersarecutofforiftheydisplaycorrectly.500charactersisalotoftext.END"
    # Test 3: Non-ASCII Characters in UI
    NoSpecialStr014 = "[[noaeciiHuayZhngwenLechWaesaaeoa]]"
    # Test 4: ASCII Special Characters and Non-ASCII Characters in fields with autocomplete behavior
    NoSpecialStr015 = "[[SanfordSons]]"
    NoSpecialStr016 = "[[OReilly]]"
    NoSpecialStr017 = "[[Hawaii]]"
    NoSpecialStr018 = "[[BullConnor]]"
    NoSpecialStr019 = "[[LechWaesa]]"
    NoSpecialStr020 = "[[HerrMueller]]"
    # *********************************************************************************

    # *********************************************************************************
    # THIS IS REGRESSION TEST ABOUT DSF-30343
    # Force to name FormFields name with special character, then check how it was handled 
    # *********************************************************************************
    #5. Add Items for test
    print("#5-001 - Add New FormField")
    for i in range(20):
        driver.find_element_by_css_selector("div.scContextBlockContent > div.scBtn").click()
        assTxt = "[[FormField0]]"
        #print (assTxt)
        InputNr = "(//input[@type='text'])[" + str(i*2+38) + "]"
        InputNr2 = "(//input[@type='text'])[" + str(i*2+39) + "]"
        strNr = eval("str"+"{0:03d}".format(i+1))
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        #print (InputNr)
        #print (strNr)
        if str(i) == "0":
            Xpath = "//label_ctrl/div/div"
            #print (Xpath)
            assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(Xpath).click()
            print ("---- Scroll down to show target content in window (Necessary to handle invisible element in window)")
            driver.find_element_by_xpath(InputNr).location_once_scrolled_into_view
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            assert strNr2 in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-001-" + "{0:03d}".format(i+1) + " - Add New FormField")
        else:
            Xpath = "//div[" + str(i*2+1) + "]/div/label_ctrl/div/div"
            #print (Xpath)
            assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(Xpath).click()
            print ("---- Scroll down to show target content in window (Necessary to handle invisible element in window)")
            driver.find_element_by_xpath(InputNr).location_once_scrolled_into_view
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            assert strNr2 in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-001-" + "{0:03d}".format(i+1) + " - Add New FormField")
    time.sleep(1)
    # *********************************************************************************

    # *********************************************************************************
    #3. Add items
    print ("#3. Add items")
    #3-1. Add Text frame, and Edit
    print ("#3-1. Add Text frame, and Edit - 001/004")
    for i in range(7):
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
        driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
        # *****************************************************************************
        # Condition for SauceLabs execution optional selection operation
        # *****************************************************************************
        if TestOnSauceLabs == True:
            print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
            elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
            WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
            driver.find_element_by_css_selector(elem_css).click()
        else:
            print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
        # *****************************************************************************
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("//div").send_keys(strNr2)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        #3-3. Change font size
        print ("#3-3. Change font size")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
        #3-4. Change position
        print ("#3-4. Change position - Reset current target")
        driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li").click()
        #3-5. Select Top
        print("#3-5. Select Top")
        driver.find_element_by_css_selector("span.scLayerItemName").click()
        #3-6. Open 'Layout' context tab menu
        print ("#3-6. Open 'Layout' context tab menu")
        driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
        #3-7. Set X position of the element - 'X =< 7 inch'
        #print("#3-7. Set X position of the element - 'X =< 7 inch'")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
        #3-8. Set Y position of the element - 'Y =< 7 inch'
        print("#3-8. Set Y position of the element - 'Y =< 7 inch'")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1))
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
        print ("#3-9. Add Text frame, and Edit - 001/004 - " + "{0:03d}".format(i+1)+ "/020")
    # **********************************************************************************************

    # *********************************************************************************
    #4. Add Page
    print ("#4. Add Page - 02")
    driver.find_element_by_css_selector("i.icon-DS_Add").click()
    print ("#4-1. Change target page to new one - 02")
    targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[2]").click()
    targetCanvas = driver.find_element_by_xpath("//div").click()
    # *********************************************************************************

    # *********************************************************************************
    #3. Add items
    print ("#3. Add items")
    #3-1. Add Text frame, and Edit
    print ("#3-1. Add Text frame, and Edit - 002/004")
    for i in range(7,12):
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
        driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
        # *****************************************************************************
        # Condition for SauceLabs execution optional selection operation
        # *****************************************************************************
        if TestOnSauceLabs == True:
            print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
            elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
            WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
            driver.find_element_by_css_selector(elem_css).click()
        else:
            print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
        # *****************************************************************************
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("//div").send_keys(strNr2)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        #3-3. Change font size
        print ("#3-3. Change font size")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
        #3-4. Change position
        print ("#3-4. Change position - Reset current target")
        targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[2]").click()
        targetCanvas = driver.find_element_by_xpath("//div").click()
        #3-5. Select Top
        print("#3-5. Select Top")
        driver.find_element_by_css_selector("span.scLayerItemName").click()
        #3-6. Open 'Layout' context tab menu
        print ("#3-6. Open 'Layout' context tab menu")
        driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
        #3-7. Set X position of the element - 'X =< 7 inch'
        #print("#3-7. Set X position of the element - 'X =< 7 inch'")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
        #3-8. Set Y position of the element - 'Y =< 7 inch'
        print("#3-8. Set Y position of the element - 'Y =< 7 inch'")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1-7))
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
        print ("#3-9. Add Text frame, and Edit - 002/004 - " + "{0:03d}".format(i+1)+ "/020")
    # **********************************************************************************************

    # *********************************************************************************
    #4. Add Page
    print ("#4. Add Page - 03")
    driver.find_element_by_css_selector("i.icon-DS_Add").click()
    print ("#4-1. Change target page to new one - 03")
    targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[3]").click()
    targetCanvas = driver.find_element_by_xpath("//div").click()
    # *********************************************************************************

    # *********************************************************************************
    #3. Add items
    print ("#3. Add items")
    #3-1. Add Text frame, and Edit
    print ("#3-1. Add Text frame, and Edit - 003/004")
    for i in range(13,14):
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
        driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
        # *****************************************************************************
        # Condition for SauceLabs execution optional selection operation
        # *****************************************************************************
        if TestOnSauceLabs == True:
            print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
            elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
            WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
            driver.find_element_by_css_selector(elem_css).click()
        else:
            print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
        # *****************************************************************************
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("//div").send_keys(strNr2)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        #3-3. Change font size
        print ("#3-3. Change font size")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
        #3-4. Change position
        print ("#3-4. Change position - Reset current target")
        targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[3]").click()
        targetCanvas = driver.find_element_by_xpath("//div").click()
        #3-5. Select Top
        print("#3-5. Select Top")
        driver.find_element_by_css_selector("span.scLayerItemName").click()
        #3-6. Open 'Layout' context tab menu
        print ("#3-6. Open 'Layout' context tab menu")
        driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
        #3-7. Set X position of the element - 'X =< 7 inch'
        #print("#3-7. Set X position of the element - 'X =< 7 inch'")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
        #3-8. Set Y position of the element - 'Y =< 7 inch'
        print("#3-8. Set Y position of the element - 'Y =< 7 inch'")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1-13))
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
        print ("#3-9. Add Text frame, and Edit - 003/004 - " + "{0:03d}".format(i+1)+ "/020")
    # **********************************************************************************************

    # *********************************************************************************
    #4. Add Page
    print ("#4. Add Page - 04")
    driver.find_element_by_css_selector("i.icon-DS_Add").click()
    print ("#4-1. Change target page to new one - 04")
    targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[4]").click()
    targetCanvas = driver.find_element_by_xpath("//div").click()
    # *********************************************************************************

    # *********************************************************************************
    #3. Add items
    print ("#3. Add items")
    #3-1. Add Text frame, and Edit
    print ("#3-1. Add Text frame, and Edit - 004/004")
    for i in range(14,20):
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
        driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
        # *****************************************************************************
        # Condition for SauceLabs execution optional selection operation
        # *****************************************************************************
        if TestOnSauceLabs == True:
            print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
            elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
            WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
            driver.find_element_by_css_selector(elem_css).click()
        else:
            print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
        # *****************************************************************************
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("//div").send_keys(strNr2)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        #3-3. Change font size
        print ("#3-3. Change font size")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
        #3-4. Change position
        print ("#3-4. Change position - Reset current target")
        targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[4]").click()
        targetCanvas = driver.find_element_by_xpath("//div").click()
        #3-5. Select Top
        print("#3-5. Select Top")
        driver.find_element_by_css_selector("span.scLayerItemName").click()
        #3-6. Open 'Layout' context tab menu
        print ("#3-6. Open 'Layout' context tab menu")
        driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
        #3-7. Set X position of the element - 'X =< 7 inch'
        #print("#3-7. Set X position of the element - 'X =< 7 inch'")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
        #3-8. Set Y position of the element - 'Y =< 7 inch'
        print("#3-8. Set Y position of the element - 'Y =< 7 inch'")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1-14))
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
        print ("#3-9. Add Text frame, and Edit - 004/004 - " + "{0:03d}".format(i+1)+ "/020")
    # **********************************************************************************************

def TickFacingPage():
    # **********************************************************************************************
    #15. Document settings
    print ("#15. Document settings")
    time.sleep(1)
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    time.sleep(1)
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
    #15-1. Enable Facing Pages option
    print ("#15-1. Enable Facing Pages option")
    driver.find_element_by_xpath("//div[6]/div[2]/checkbox_ctrl/div/div/div").click()
    #15-2. Close dialog
    print ("#15-2. Close dialog")
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[34]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************

def cleanupPages(PageNumber):
    # **********************************************************************************************
    #17-2-1. Clean Up - Page
    print ("#17-2-1. Clean Up - Pages")
    for i in range(PageNumber):
        elem_path = "//div[2]/div/ul/li/ul/li"
        driver.find_element_by_xpath(elem_path).click()
        driver.find_element_by_css_selector("i.icon-DS_TrashCan").click()
        print ("#17-2-2. Clean Up - Pages: " + "{0:03d}".format(i+1))
        time.sleep(1)

def cleanupFormFields(FormFieldsNumber):
    # **********************************************************************************************
    #17-4. Clean Up - FormField
    print ("#17-4. Clean Up - FormField")
    for i in range(FormFieldsNumber):
        driver.find_element_by_css_selector("div.scCol-1 > div.scIconBtn > i.icon-DS_TrashCan").click()
        time.sleep(1)
    # **********************************************************************************************

def DSMI_Document_for_Preview_Rendering__SettingDocument(Source):
    # **********************************************************************************************
    #4. Document settings
    print ("#4. Document settings")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
    time.sleep(1)
    #4-5. DSMI document for preview rendering
    print("#4-5. DSMI document for preview rendering")
    elem_path = "//div[23]/div/checkbox_ctrl/div/div/div"
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    time.sleep(1)
    # DSMI document for preview rendering Disabled
    #elem_css = "scCheckbox icon-DS_Checkbox"
    # DSMI document for preview rendering Enabled 
    #elem_css = "scCheckbox icon-DS_Checkbox icon-DS_SelectedCheckbox"
    driver.find_element_by_xpath(elem_path).click()
    time.sleep(1)
    # ****************************************
    # Set Mode type
    # ---- Open Mode selection dropdown menu
    elem_path="//div[25]/div/text_ctrl/div/div[2]/div/div[3]/i"
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    time.sleep(1)
    driver.find_element_by_xpath(elem_path).click()
    # ---- Select Print
    elem_path="//div[25]/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    # ---- Select Preview
    #elem_path="//div[25]/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    driver.find_element_by_xpath(elem_path).click()
    # ****************************************
    # Set Alias Name
    # This value could be changed due to another setting in SC(make sure to run on fresh template)
    # It cause one higher number incase you have already activate "DSMI document for preview rendering" and open dialog.
    # Deactivated -- >elem_xpath="(//input[@type='text'])[163]"
    # Activated -- >elem_xpath="(//input[@type='text'])[164]"
    # ****************************************
    elem_xpath="(//input[@type='text'])[163]" 
    # *******************
    # *******************
    # ---- Set Alias Name for Preview
    vdpAliasName = "TumblerDevOps"
    print ("# ---- Set Alias Name for Preview : " + str(vdpAliasName))
    # *******************
    # *******************
    # ---- Perform type operation
    driver.find_element_by_xpath(elem_xpath).location_once_scrolled_into_view
    driver.find_element_by_xpath(elem_xpath).clear()
    driver.find_element_by_xpath(elem_xpath).send_keys(str(vdpAliasName))
    driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
    time.sleep(1)
    # ****************************************
    # Set Source type
    # ---- Open Source selection dropdown menu
    elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[3]/i"
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    driver.find_element_by_xpath(elem_path).click()
    if Source == "OwnAccount":
        # ---- Select Own account
        elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    else:
        # ---- Select Global account
        elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    driver.find_element_by_xpath(elem_path).click()
    # ****************************************
    #4-6. Close dialog
    print ("#4-6. Close dialog")
    driver.find_element_by_xpath("//div[34]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************

    # *********************************************************************************
    #3-1. Store Form Field name(s)
    # *********************************************************************************
    print ("#3-1. Set name for FormField")
    # Test 1: Set name for FormField
    str001 = 'color'
    str002 = 'png'

    print ("#3-2. Set expected registered name for FormField")
    # Test 1: Set expected registered name for FormField
    NoSpecialStr001 = "[[color]]"
    NoSpecialStr002 = "[[png]]"

    print ("#3-3. Set default value for FormField")
    # Test 1: Set default value for FormField
    defVal001 = "darkblue"
    #selection = [blue, charcoal, clear, darkblue, green, orange, purple, red]
    defVal002 = u"https://nogtips.files.wordpress.com/2012/10/hourglass1.png"
    # *********************************************************************************

    # *********************************************************************************
    #5. Add FormField
    print("#5. Add New FormField")
    for i in range(2):
        driver.find_element_by_css_selector("div.scContextBlockContent > div.scBtn").click()
        time.sleep(1)
        assTxt = "[[FormField0]]"
        #print (assTxt)
        InputNr = "(//input[@type='text'])[" + str(i*2+38) + "]"
        InputNr2 = "(//input[@type='text'])[" + str(i*2+39) + "]"
        strNr = eval("str"+"{0:03d}".format(i+1))
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        strNr3 = eval("defVal"+"{0:03d}".format(i+1))
        #print (InputNr)
        #print (strNr)
        if str(i) == "0":
            #Xpath = "//label_ctrl/div/div"
            css_path="div.scTitle"
            assert assTxt in driver.find_element_by_css_selector(css_path).text
            #print (Xpath)
            #assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_css_selector(css_path).click()
            time.sleep(1)
            #driver.find_element_by_xpath(Xpath).click()
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            #assert strNr2 in driver.find_element_by_xpath(Xpath).text
            assert strNr2 in driver.find_element_by_css_selector(css_path).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr3)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-" + "{0:03d}".format(i+1) + " - Add New FormField")
        else:
            Xpath = "//div[" + str(i*2+1) + "]/div/label_ctrl/div/div"
            #print (Xpath)
            assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(Xpath).click()
            time.sleep(1)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            assert strNr2 in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr3)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-001-" + "{0:03d}".format(i+1) + " - Add New FormField")
    time.sleep(1)
    # *********************************************************************************

    # *********************************************************************************
    # Change FormField Type setting
    # ---- Open Edit dialog
    elem_path = "//div[@id='scContainerCtrl']/main_view/div/context_panel/div/div[4]/div/div/div[2]/div/div[4]/div[2]/div"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # ---- ---- FormField Editor = Image type configured state
    #elem_path = "//div[@id='scContainerCtrl']/main_view/div/context_panel/div/div[4]/div/div/div[2]/div/div[3]/div[3]/div/i"
    # ---- Add Display name
    elem_path = "(//input[@type='text'])[98]"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    target.send_keys(Keys.CONTROL, "a")
    target.send_keys(vdpAliasName + " for SC preview")
    target.send_keys(Keys.RETURN)
    time.sleep(1)
    # ---- Default Value
    #elem_path = "(//input[@type='text'])[98]"
    # ---- Enable/Disable Mandatory option
    #elem_path = "//div[4]/div/div[2]/checkbox_ctrl/div/div/div"
    # ---- Change Type - Default(Text) to Image
    elem_path = "//div[16]/div[2]/div/div[2]/div/div/div[5]/text_ctrl/div/div[2]/div/div[3]/i"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    ## ---- ---- Text
    #elem_path = "//div[5]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    ## ---- ---- ---- Category
    #elem_path = "//div[16]/div[2]/div/div[2]/div/div/div[6]/text_ctrl/div/div[2]/div/div[3]/i"
    ## 	[None]
    #elem_path = "//div[6]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    ## 	Firstname
    #elem_path = "//div[6]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    ## 	Lastname
    #elem_path = "//div[6]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[3]"
    ## 	n
    #elem_path = "//div[6]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[n]"
    ## ---- ---- ---- Text maximum length
    ## ---- ---- List
    #elem_path = "//div[5]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    ## ---- ---- ---- Category
    ## ---- ---- ---- ---- List menu (Left side)
    ## ---- ---- ---- ---- Add List entry
    ## ---- ---- ---- ---- Set number for entry
    ## ---- ---- ---- ---- Set value for entry
    ## ---- ---- ---- ---- Remove List entry
    ## ---- ---- ---- ---- Change Order in list (up)
    ## ---- ---- ---- ---- Change Order in list (down)
    # ---- ---- Image
    elem_path = "//div[5]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[3]"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # ---- ---- ---- Category
    # ---- Close Edit dialog
    # 	ok button - xPath:idRelative
    #elem_path = "//div[@id='scContainerCtrl']/main_view/div/div[16]/div[3]/div"
    # 	ok button - xPath:position
    elem_path = "//div[16]/div[3]/div"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # 	cancel button - xPath:idRelative
    #elem_path = "//div[@id='scContainerCtrl']/main_view/div/div[16]/div[3]/div[2]"
    # 	cancel button - xPath:position
    #elem_path = "//div[16]/div[3]/div[2]"
    #time.sleep(1)
    # Set Default Image for Image type FormField
    # ---- Open Image dialog
    if TestOnSauceLabs == True:
        # SauceLabs specification (Due to missing image content, maybe browser issue?)
        print ("# SauceLabs specification")
        elem_path="//div[1]/main_view/div/context_panel/div/div[4]/div/div[1]/div[2]/div[1]/div/div[1]/img"
        target = driver.find_element_by_xpath(elem_path)
    else:
        # Local specification
        print ("# Local specification")
        elem_css = "i.icon-DS_ImageUploadFail"
        target = driver.find_element_by_css_selector(elem_css)
    target.click()
    time.sleep(1)
    # ---- ---- Create Category
    # ---- ---- Upload Image file
    elem_path = "//input[@name='myImageUploadButton']"
    print ("# ---- ---- Upload Image file")
    driver.file_detector.is_local_file()
    target = driver.find_element_by_xpath(elem_path)
    target.send_keys("C:\\temp\\SmartCanvas_ImageUpload\\man whispering4.png")
    # ---- Set Version base Operation condition
    if int(myVersion_Major) >= 7 and int(myVersion_Mainor) >= 3 and int(myVersion_Update) >= 0 and int(myVersion_Build) >= 85:
        # ---- ---- Click ok on Alert pop-up (@New Feature at 7.3.0.85)
        try:
            alert = driver.switch_to_alert()
            alert.accept()
        except:
            print ("no alert to accept")
            time.sleep(1)
    else:
        print ("Alert may not appear while uploading")
        time.sleep(1)
    # ---- ---- Wait Uploading - up to 20 sec
    print("# ---- ---- Wait Uploading - up to 20 sec")
    wait = WebDriverWait(driver,20)
    elem_path = "//div[2]/div/div/img"
    wait.until(EC.visibility_of_element_located((By.XPATH, elem_path)))
    # ---- ---- ---- Select image file
    elem_path = "//div[2]/div/div/img"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # *********************************************************************************

    # *********************************************************************************
    #3-4. Add Image
    print ("#3-4. Add Image")
    driver.find_element_by_css_selector("i.icon-DS_Image").click()
    time.sleep(1)
    # ---- Select Uploaded Image file - 2nd in list
    elem_path = "//div[@id='scContainerCtrl']/main_view/div/div[26]/div[2]/div/div[2]/div[2]/div/div/img"
    wait.until(EC.visibility_of_element_located((By.XPATH, elem_path)))
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    # *********************************************************************************

    # *********************************************************************************
    # Link Image Frame to FormField
    # ---- Enable/Disable Link to FormField option - xpath:position
    elem_path = "//div[4]/div[2]/div/div/checkbox_ctrl/div/div/div"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # ---- Open FormField  dropdown menu - xpath:position
    elem_path = "//div[2]/div/text_ctrl/div/div[2]/div/div[3]/i"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # ---- Select 1st FormField in menu - xpath:position
    elem_path = "//div[2]/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # *********************************************************************************

    # *********************************************************************************
    #3-4-1. Move Imagge element
    print ("#3-4-1. Move Imagge element")
    #3-4-2-1. Open 'Layout' context tab menu
    print ("#3-4-2-1. Open 'Layout' context tab menu")
    driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
    #3-4-2-2. Set X position of the element - 'X = 2 inch'
    print("#3-4-2-2. Set X position of the element - 'X = 2 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("2")
    driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
    #3-4-2-3. Set Y position of the element - 'Y = 0.5 inch'
    print("#3-4-2-3. Set Y position of the element - 'Y = 0.5 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("0.5")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
    #3-4-2-2. Set X position of the element - 'W = 7 inch'
    print("#3-4-2-2. Set X position of the element - 'W = 7 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[25]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[25]").send_keys("7")
    driver.find_element_by_xpath("(//input[@type='text'])[25]").send_keys(Keys.RETURN)
    #3-4-2-3. Set Y position of the element - 'H = 7 inch'
    print("#3-4-2-3. Set Y position of the element - 'H = 7 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[26]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[26]").send_keys("7")
    driver.find_element_by_xpath("(//input[@type='text'])[26]").send_keys(Keys.RETURN)
    # *********************************************************************************

def openDocumentSetting():
    # **********************************************************************************************
    #1-1. Document settings
    # **********************************************************************************************
    print ("#1-1. Document settings")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
    time.sleep(1)
    # **********************************************************************************************

def doubleClick_scModalHead():
    # **********************************************************************************************
    #1-2. Double Click Document Setting window's Head part to maximize size
    # **********************************************************************************************
    print ("#1-2. Double Click Document Setting window's Head part to maximize size")
    driver.execute_script("$('.scModalHead').dblclick();")
    time.sleep(1)
    # **********************************************************************************************

def closeDocumentSetting():
    # ****************************************
    #1-3. Close dialog
    print ("#1-3. Close dialog")
    # ****************************************
    driver.find_element_by_xpath("//div[34]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************

def scroll_to_DSMI_Document_for_Preview_Rendering():
    # **********************************************************************************************
    #4-6. Scroll down to DSMI document for preview rendering section
    print("#4-6. Scroll down to DSMI document for preview rendering section")
    # **********************************************************************************************
    elem_path = "//div[23]/div/checkbox_ctrl/div/div/div"
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    ##4-6. Scroll down to bottom
    ## **********************************************************************************************
    #elem_xpath="//div/main_view/div/div[34]/div[2]/div/div[25]/div[2]/text_ctrl/div/div[2]/div/div[3]/i"
    #print ("#4-6. Scroll down to bottom")
    #driver.find_element_by_xpath(elem_xpath).location_once_scrolled_into_view
    #time.sleep(3)
    # **********************************************************************************************	

def configure_DSMI_Document_for_Preview_Rendering():
    # **********************************************************************************************
    #4-5. DSMI document for preview rendering
    print("#4-5. DSMI document for preview rendering")
    # **********************************************************************************************
    elem_path = "//div[23]/div/checkbox_ctrl/div/div/div"
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    time.sleep(1)
    # DSMI document for preview rendering Disabled
    #elem_css = "scCheckbox icon-DS_Checkbox"
    # DSMI document for preview rendering Enabled 
    #elem_css = "scCheckbox icon-DS_Checkbox icon-DS_SelectedCheckbox"
    driver.find_element_by_xpath(elem_path).click()
    time.sleep(1)
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    time.sleep(1)
    # **********************************************************************************************	

def configure_DSMI_Document_for_Preview_Rendering____SetMode(ModeType):
    # ****************************************
    # Set Mode type
    # ---- Open Mode selection dropdown menu
    # ****************************************
    elem_path_MODE ="//div/main_view/div/div[34]/div[2]/div/div[25]/div/text_ctrl/div/div[2]/div/div[3]/i"
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    driver.find_element_by_xpath(elem_path_MODE).location_once_scrolled_into_view
    driver.find_element_by_xpath(elem_path_MODE).click()
    # ****************************************
    # Set Source type
    # ---- Open Source selection dropdown menu
    # ****************************************
    #elem_path_SOURCE ="//div/main_view/div/div[34]/div[2]/div/div[25]/div[2]/text_ctrl/div/div[2]/div/div[3]/i"
    ## ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    #driver.find_element_by_xpath(elem_path_SOURCE).location_once_scrolled_into_view
    #driver.find_element_by_xpath(elem_path_SOURCE).click()
    # ****************************************
    time.sleep(1)
    # ****************************************
    if ModeType == "Print":
		# ---- Select Print
        elem_path="//div/main_view/div/div[34]/div[2]/div/div[25]/div[1]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    else:
		# ---- Select Preview
        elem_path="//div/main_view/div/div[34]/div[2]/div/div[25]/div[1]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    driver.find_element_by_xpath(elem_path).click()
    time.sleep(1)
    # **********************************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____SetAliasName(vdpAliasName):
    # ****************************************
    # Set Alias Name
    # This value could be changed due to another setting in SC(make sure to run on fresh template)
    # It cause one higher number incase you have already activate "DSMI document for preview rendering" and open dialog.
    # Deactivated -- >elem_xpath="(//input[@type='text'])[163]"
    # Activated -- >elem_xpath="(//input[@type='text'])[164]"
    # ****************************************
    elem_xpath="(//input[@type='text'])[163]" 
    # *******************
    # *******************
    if vdpAliasName == "":
	    # ---- Set Alias Name for Preview
        vdpAliasName = "TumblerDevOps"
    else:
        vdpAliasName = vdpAliasName
    print ("# ---- Set Alias Name for Preview : " + str(vdpAliasName))
    # *******************
    # *******************
    # ---- Perform type operation
    driver.find_element_by_xpath(elem_xpath).clear()
    driver.find_element_by_xpath(elem_xpath).send_keys(str(vdpAliasName))
    driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
    time.sleep(1)
    # **********************************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____SetSourceType(Source):
    # ****************************************
    # Set Source type
    # ---- Open Source selection dropdown menu
    # ****************************************
    elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[3]/i"
    driver.find_element_by_xpath(elem_path).click()
    if Source == "OwnAccount":
        # ---- Select Own account
        elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    else:
        # ---- Select Global account
        elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    driver.find_element_by_xpath(elem_path).click()
    # **********************************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____CloseDialog():
    # ****************************************
    #4-6. Close dialog
    print ("#4-6. Close dialog")
    # ****************************************
    driver.find_element_by_xpath("//div[34]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************


def configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields_Add():
    global TestOnSauceLabs
    # *********************************************************************************
    #3-1. Store Form Field name(s)
    # *********************************************************************************
    print ("#3-1. Set name for FormField")
    # Test 1: Set name for FormField
    str001 = 'color'
    str002 = 'png'

    print ("#3-2. Set expected registered name for FormField")
    # Test 1: Set expected registered name for FormField
    NoSpecialStr001 = "[[color]]"
    NoSpecialStr002 = "[[png]]"

    print ("#3-3. Set default value for FormField")
    # Test 1: Set default value for FormField
    defVal001 = "darkblue"
    #selection = [blue, charcoal, clear, darkblue, green, orange, purple, red]
    defVal002 = u"https://nogtips.files.wordpress.com/2012/10/hourglass1.png"
    # *********************************************************************************

    # *********************************************************************************
    #5. Add FormField
    print("#5. Add New FormField")
    for i in range(2):
        driver.find_element_by_css_selector("div.scContextBlockContent > div.scBtn").click()
        time.sleep(1)
        assTxt = "[[FormField0]]"
        #print (assTxt)
        InputNr = "(//input[@type='text'])[" + str(i*2+38) + "]"
        InputNr2 = "(//input[@type='text'])[" + str(i*2+39) + "]"
        strNr = eval("str"+"{0:03d}".format(i+1))
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        strNr3 = eval("defVal"+"{0:03d}".format(i+1))
        #print (InputNr)
        #print (strNr)
        if str(i) == "0":
            #Xpath = "//label_ctrl/div/div"
            css_path="div.scTitle"
            assert assTxt in driver.find_element_by_css_selector(css_path).text
            #print (Xpath)
            #assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_css_selector(css_path).click()
            time.sleep(1)
            #driver.find_element_by_xpath(Xpath).click()
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            #assert strNr2 in driver.find_element_by_xpath(Xpath).text
            assert strNr2 in driver.find_element_by_css_selector(css_path).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr3)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-" + "{0:03d}".format(i+1) + " - Add New FormField")
        else:
            Xpath = "//div[" + str(i*2+1) + "]/div/label_ctrl/div/div"
            #print (Xpath)
            assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(Xpath).click()
            time.sleep(1)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            assert strNr2 in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr3)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-001-" + "{0:03d}".format(i+1) + " - Add New FormField")
    time.sleep(1)
    # *********************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields_Open_FormFieldEditor():
    # *********************************************************************************
    # Change FormField Type setting
    # ---- Open Edit dialog
    elem_path = "//div[@id='scContainerCtrl']/main_view/div/context_panel/div/div[4]/div/div/div[2]/div/div[4]/div[2]/div/i"
    #elem_path = "//div[@id='eins']/main_view/div/context_panel/div/div[4]/div/div/div[2]/div/div[4]/div[2]/div"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # *********************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields_Edit_FormFieldEditor(vdpAliasName):
    # *********************************************************************************
    # ---- ---- FormField Editor = Image type configured state
    #elem_path = "//div[@id='scContainerCtrl']/main_view/div/context_panel/div/div[4]/div/div/div[2]/div/div[3]/div[3]/div/i"
    # ---- Add Display name
    elem_path = "(//input[@type='text'])[98]"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    target.send_keys(Keys.CONTROL, "a")
    target.send_keys(vdpAliasName + " for SC preview")
    target.send_keys(Keys.RETURN)
    time.sleep(1)
    # ---- Default Value
    #elem_path = "(//input[@type='text'])[98]"
    # ---- Enable/Disable Mandatory option
    #elem_path = "//div[4]/div/div[2]/checkbox_ctrl/div/div/div"
    # ---- Change Type - Default(Text) to Image
    elem_path = "//div[16]/div[2]/div/div[2]/div/div/div[5]/text_ctrl/div/div[2]/div/div[3]/i"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    ## ---- ---- Text
    #elem_path = "//div[5]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    ## ---- ---- ---- Category
    #elem_path = "//div[16]/div[2]/div/div[2]/div/div/div[6]/text_ctrl/div/div[2]/div/div[3]/i"
    ## 	[None]
    #elem_path = "//div[6]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    ## 	Firstname
    #elem_path = "//div[6]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    ## 	Lastname
    #elem_path = "//div[6]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[3]"
    ## 	n
    #elem_path = "//div[6]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[n]"
    ## ---- ---- ---- Text maximum length
    ## ---- ---- List
    #elem_path = "//div[5]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    ## ---- ---- ---- Category
    ## ---- ---- ---- ---- List menu (Left side)
    ## ---- ---- ---- ---- Add List entry
    ## ---- ---- ---- ---- Set number for entry
    ## ---- ---- ---- ---- Set value for entry
    ## ---- ---- ---- ---- Remove List entry
    ## ---- ---- ---- ---- Change Order in list (up)
    ## ---- ---- ---- ---- Change Order in list (down)
    # ---- ---- Image
    elem_path = "//div[5]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[3]"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # ---- ---- ---- Category
    # *********************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields_Close_FormFieldEditor():
    # *********************************************************************************
    # ---- Close Edit dialog
    # 	ok button - xPath:idRelative
    #elem_path = "//div[@id='scContainerCtrl']/main_view/div/div[16]/div[3]/div"
    # 	ok button - xPath:position
    elem_path = "//div[16]/div[3]/div"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # 	cancel button - xPath:idRelative
    #elem_path = "//div[@id='scContainerCtrl']/main_view/div/div[16]/div[3]/div[2]"
    # 	cancel button - xPath:position
    #elem_path = "//div[16]/div[3]/div[2]"
    #time.sleep(1)
    # *********************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____UploadImage():
    global TestOnSauceLabs
    # *********************************************************************************
    # ---- Open Image dialog
    if TestOnSauceLabs == True:
        # SauceLabs specification (Due to missing image content, maybe browser issue?)
        print ("# SauceLabs specification")
        elem_css = "i.icon-DS_ImageUploadFail"
        target = driver.find_element_by_css_selector(elem_css)
        #elem_path="//div[1]/main_view/div/context_panel/div/div[4]/div/div[1]/div[2]/div[1]/div/div[1]/img"
        #target = driver.find_element_by_xpath(elem_path)
    else:
        # Local specification
        print ("# Local specification")
        elem_css = "i.icon-DS_ImageUploadFail"
        target = driver.find_element_by_css_selector(elem_css)
    target.click()
    time.sleep(1)
    # ---- ---- Create Category
    # ---- ---- Upload Image file
    elem_path = "//input[@name='myImageUploadButton']"
    print ("# ---- ---- Upload Image file")
    driver.file_detector.is_local_file()
    target = driver.find_element_by_xpath(elem_path)
    target.send_keys("C:\\temp\\SmartCanvas_ImageUpload\\man whispering4.png")
    # ---- Set Version base Operation condition
    if int(myVersion_Major) >= 7 and int(myVersion_Mainor) >= 3 and int(myVersion_Update) >= 0 and int(myVersion_Build) >= 85:
        # ---- ---- Click ok on Alert pop-up (@New Feature at 7.3.0.85)
        try:
            alert = driver.switch_to_alert()
            alert.accept()
        except:
            print ("no alert to accept")
            time.sleep(1)
    else:
        print ("Alert may not appear while uploading")
        time.sleep(1)
    # ---- ---- Wait Uploading - up to 60 sec
    print("# ---- ---- Wait Uploading - up to 60 sec")
    wait = WebDriverWait(driver,60)
    elem_path = "//div[2]/div/div/img"
    wait.until(EC.visibility_of_element_located((By.XPATH, elem_path)))
    # *********************************************************************************
    
def configure_DSMI_Document_for_Preview_Rendering____SelectImage():
    # ---- ---- ---- Select image file
    elem_path = "//div[2]/div/div/img"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # *********************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____ConfigureImage_Add():
    # *********************************************************************************
    #3-4. Add Image
    print ("#3-4. Add Image")
    driver.find_element_by_css_selector("i.icon-DS_Image").click()
    time.sleep(1)
    # ---- Select Uploaded Image file - 2nd in list
    elem_path = "//div[2]/div/div/img"
    print("# ---- ---- Wait Uploading - up to 60 sec")
    wait = WebDriverWait(driver,60)
    wait.until(EC.visibility_of_any_elements_located((By.XPATH, elem_path)))
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # *********************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____ConfigureImage_Link():
    # *********************************************************************************
    # Link Image Frame to FormField
    # ---- Enable/Disable Link to FormField option - xpath:position
    elem_path = "//div[4]/div[2]/div/div/checkbox_ctrl/div/div/div"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # ---- Open FormField  dropdown menu - xpath:position
    elem_path = "//div[2]/div/text_ctrl/div/div[2]/div/div[3]/i"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # ---- Select 1st FormField in menu - xpath:position
    elem_path = "//div[2]/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # *********************************************************************************

def configure_DSMI_Document_for_Preview_Rendering____ConfigureImage_Move():
    # *********************************************************************************
    #3-4-1. Move Imagge element
    print ("#3-4-1. Move Imagge element")
    #3-4-2-1. Open 'Layout' context tab menu
    print ("#3-4-2-1. Open 'Layout' context tab menu")
    driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
    #3-4-2-2. Set X position of the element - 'X = 2 inch'
    print("#3-4-2-2. Set X position of the element - 'X = 2 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("2")
    driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
    #3-4-2-3. Set Y position of the element - 'Y = 0.5 inch'
    print("#3-4-2-3. Set Y position of the element - 'Y = 0.5 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("0.5")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
    #3-4-2-2. Set X position of the element - 'W = 7 inch'
    print("#3-4-2-2. Set X position of the element - 'W = 7 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[25]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[25]").send_keys("7")
    driver.find_element_by_xpath("(//input[@type='text'])[25]").send_keys(Keys.RETURN)
    #3-4-2-3. Set Y position of the element - 'H = 7 inch'
    print("#3-4-2-3. Set Y position of the element - 'H = 7 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[26]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[26]").send_keys("7")
    driver.find_element_by_xpath("(//input[@type='text'])[26]").send_keys(Keys.RETURN)
    # *********************************************************************************

def deleteUploadedImageFile():
    # *********************************************************************************
    #14. Clean Up -- Delete Uploaded Image file
    # *********************************************************************************
    #14-1. Clean Up -- Image
    print ("#14-1. Clean Up -- Image")
    driver.find_element_by_css_selector("i.icon-DS_Image").click()
    time.sleep(1)
    # ---- ---- Clean-Up ---- Upload Image file
    print("# ---- ---- Clean-Up ---- Upload Image file")
    elem_path = "//div[1]/main_view/div/div[26]/div[2]/div[1]/div[2]/div[2]/div[2]/div[3]"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # ---- ---- Click ok on Alert pop-up
    print("# ---- ---- Click ok on Alert pop-up")
    alert = driver.switch_to_alert()
    alert.accept()
    time.sleep(1)
    # ---- ---- Close Image library dialog
    print("# ---- ---- Close Image library dialog")
    elem_path = "//div[26]/div[3]/div/div"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # *********************************************************************************

def Cleanup__DSMI_Document_for_Preview_Rendering__SettingDocument():
    # **********************************************************************************************
    #15.---- Clean-Up - Clean-Up Document settings
    print ("#15.---- Clean-Up - Clean-Up Document settings")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
    time.sleep(1)
    # ****************************************
    #15-1.---- Clean-Up - Set Alias Name
    print ("#15-1.---- Clean-Up - Set Alias Name")
    # This value could be changed due to another setting in SC(make sure to run on fresh template)
    # It cause one higher number incase you have already activate "DSMI document for preview rendering" and open dialog.
    # Deactivated -- >elem_xpath="(//input[@type='text'])[163]"
    # Activated -- >elem_xpath="(//input[@type='text'])[164]"
    # ****************************************
    elem_xpath="(//input[@type='text'])[164]"
    #15-1-1.---- Clean-Up ---- Perform type operation
    print ("#15-1-1.---- Clean-Up ---- Perform type operation")
    driver.find_element_by_xpath(elem_xpath).clear()
    driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
    time.sleep(1)
    # ****************************************
    #15-2.---- Clean-Up - Set Mode type
    print ("#15-2.---- Clean-Up - Set Mode type")
    # ****************************************
    #15-2-1.---- Clean-Up - ---- Open Mode selection dropdown menu
    print ("#15-2-1.---- Clean-Up - ---- Open Mode selection dropdown menu")
    elem_path="//div[25]/div/text_ctrl/div/div[2]/div/div[3]/i"
    #15-2-2.---- Clean-Up - ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    print ("#15-2-2.---- Clean-Up - ---- Scroll down to show target content in window (Necessary to handle invisible element in window)")
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    time.sleep(1)
    driver.find_element_by_xpath(elem_path).click()
    #15-2-3.---- Clean-Up - ---- Select Print
    print ("#15-2-3.---- Clean-Up - ---- Select Print")
    elem_path="//div[25]/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    #15-2-3.---- Clean-Up - ---- Select Preview
    #print ("#15-2-3.---- Clean-Up - ---- Select Preview")
    #elem_path="//div[25]/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    driver.find_element_by_xpath(elem_path).click()
    # ****************************************
    #15-3.---- Clean-Up - Set Source type
    print ("#15-3.---- Clean-Up - Set Source type")
    # ****************************************
    #15-3-1.---- Clean-Up - ---- Open Source selection dropdown menu
    print ("#15-3-1.---- Clean-Up - ---- Open Source selection dropdown menu")
    elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[3]/i"
    driver.find_element_by_xpath(elem_path).click()
    #15-3-2.---- Clean-Up - ---- Select Own account
    print ("#15-3-2.---- Clean-Up - ---- Select Own account")
    elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    #15-3-2.---- Clean-Up - ---- Select Global account
    print ("#15-3-2.---- Clean-Up - ---- Select Global account")
    #elem_path="//div[25]/div[2]/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    driver.find_element_by_xpath(elem_path).click()
    # ****************************************
    #15-4.---- Clean-Up DSMI document for preview rendering
    print("#15-4.---- Clean-Up DSMI document for preview rendering")
    # ****************************************
    elem_path="//div[1]/main_view/div/div[34]/div[2]/div/div[23]/div/checkbox_ctrl/div/div/div"
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    driver.find_element_by_xpath(elem_path).click()
    time.sleep(1)
    # ****************************************
    #15-5.---- Clean-Up Close dialog
    print ("#15-5.---- Clean-Up Close dialog")
    # ****************************************
    driver.find_element_by_xpath("//div[34]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************

def DSF_30349_AddFormField():
    # *********************************************************************************
    # THIS IS REGRESSION TEST ABOUT DSF-30349
    # Force to name FormFields name with special character, then check how it was handled 
    # *********************************************************************************
    print("#001 - Add New FormField")
    driver.find_element_by_css_selector("div.scContextBlockContent > div.scBtn").click()
    assert "[[FormField0]]" in driver.find_element_by_css_selector("div.scTitle").text
    driver.find_element_by_css_selector("div.scTitle").click()
    time.sleep(1)

    print("#002 - Set special character as FormField name")
    driver.find_element_by_css_selector("input.scEditMode").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_css_selector("input.scEditMode").send_keys(u"Name's")
    driver.find_element_by_css_selector("input.scEditMode").send_keys(Keys.RETURN)
    assert "[[Names]]" in driver.find_element_by_css_selector("div.scTitle").text
    time.sleep(1)

    print("#003 - Set default value for created FormField")
    driver.find_element_by_xpath("(//input[@type='text'])[39]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[39]").send_keys(u"Name's")
    driver.find_element_by_xpath("(//input[@type='text'])[39]").send_keys(Keys.RETURN)  
    time.sleep(1)

def DSF_30349_AddItem():
    # *********************************************************************************
    #3. Add items
    print ("#3. Add items")
    #3-1. Add Text frame, and Edit
    print ("#3-1. Add Text frame, and Edit")
    for i in range(1):
        driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
        driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
        # *****************************************************************************
        # Condition for SauceLabs execution optional selection operation
        # *****************************************************************************
        if TestOnSauceLabs == True:
            print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
            elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
            wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
            driver.find_element_by_css_selector(elem_css).click()
        else:
            print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
        # *****************************************************************************
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        time.sleep(2)
        driver.find_element_by_xpath("//div").send_keys("[[Names]]")
        time.sleep(2)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        time.sleep(1)
        #3-3. Change font size
        print ("#3-3. Change font size")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
        time.sleep(1)
        driver.find_element_by_xpath("//div").click()
        time.sleep(1)
    # *********************************************************************************

def RichTextFormatOptionCheckboxIssue_AddItems():
    # **********************************************************************************************
    #3. Add items
    print ("#3. Add items")
    #3-1. Add Text frame, and Edit
    print ("#3-1. Add Text frame, and Edit")
    #3-1-1. Add Text Item
    elem_css="span.scToolbarPanelIcon > i.icon-DS_Text"
    css = driver.find_element_by_css_selector(elem_css).click()
    #3-1-2. Edit text (Click inside frame, and Edit text)
    elem_path="//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]"
    xpath = driver.find_element_by_xpath(elem_path).click()
    elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
    WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
    driver.find_element_by_css_selector(elem_css).click()
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").send_keys("Hello [[FirstName]]!")
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").click()
    time.sleep(1)
    # **********************************************************************************************
    for i in range(3):
        #3-2-1. Select top item on the layer
        driver.find_element_by_css_selector("span.scLayerItemName").click()
        time.sleep(1)
        #3-2-2. Disable Rich-Text format option
        driver.find_element_by_xpath("//checkbox_ctrl/div/div/div").click()
        time.sleep(1)
        #3-2-3. Open View menu
        driver.find_element_by_xpath("//menu_panel/div/ul/li[4]/span").click()
        time.sleep(1)
        #3-2-4. Click 'Buyer mode view'
        driver.find_element_by_xpath("//li[4]/ul/li[6]").click()
        time.sleep(1)
        #3-2-5. Select text frame
        driver.find_element_by_xpath("//div/div/div/div/div/div/div/div[2]/div/div/p").click()
        time.sleep(1)
        #3-2-6. Select Edit mode option
        driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]").click()
        time.sleep(2)
        if i == 0 or i == 2:
            # **********************************************************************************************
            print ("#3-2-7. Assert display value of scTextToolbar - Assert display: none;")
            element = driver.find_element_by_id("scTextToolbar")
            element.get_attribute("style")
            assert 'display: none;' in element.get_attribute("style")
            # **********************************************************************************************
        else:
            # **********************************************************************************************
            print ("#3-2-7. Assert display value of scTextToolbar - Assert display: block;")
            element = driver.find_element_by_id("scTextToolbar")
            element.get_attribute("style")
            assert 'display: block;' in element.get_attribute("style")
            time.sleep(2)
            # **********************************************************************************************
        print ("#3-2-8. close Buyer mode")
        driver.find_element_by_css_selector("div.button > i.icon-DS_Close").click()
        time.sleep(2)
    # **********************************************************************************************

def getWindowSize():
    # **********************************************************
    #4. Current Window Size
    # **********************************************************
    winSize = driver.get_window_size()
    print ("#4. Current Window Size: " + str(winSize))

def changeWindowSize(sizeW, sizeH):
    # **********************************************************
    #4-1. Expand Windows Size to be able to recieve all command to add FormFields
    # **********************************************************
    print("#4-1. Expand Windows Size to be able to recieve all command to add FormFields")
    driver.set_window_size(sizeW, sizeH)

def toolbarShownOnlyStyles_AddFont():
    # **********************************************************************************************
    #9. Manage fonts
    print ("#9. Manage fonts")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
    print ("#9-1. Set Font file path on Canvas list")
    testFontPath = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div"
    time.sleep(2)
    print ("#9-2. Specify Font file path")
    driver.file_detector.is_local_file()
    driver.find_element_by_css_selector('input[name="selectedFonts"]').send_keys("C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-BdIt.otf")
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,testFontPath)))
    time.sleep(3)
    print ("#9-3. Close dialog")
    driver.find_element_by_xpath("//div[10]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************

def toolbarShownOnlyStyles_AddItems():
    # **********************************************************************************************
    #4-1. Add new Style
    print ("#4-1. Add new Style")
    # Select top item on the layer
    driver.find_element_by_css_selector("span.scLayerItemName").click()
    #4-1-1. Open Style dropdown menu
    driver.find_element_by_xpath("//div[2]/div/div/text_style_ctrl/div/div/div[2]/div/div").click()
    #4-1-2. Click add new button
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[42]/div[2]/div[2]/div[3]/i").click()
    #4-1-3. Click edit button on new style item
    driver.find_element_by_xpath("//div[42]/div/div/div[2]/div[2]/i").click()
    time.sleep(1)
    #4-2-1. Name Style
    driver.find_element_by_xpath("(//input[@type='text'])[117]").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath("(//input[@type='text'])[117]").send_keys("DevOps Style")
    #4-2-2. Set text color
    driver.find_element_by_xpath("//div[2]/div[2]/div[2]/div/div[2]/colorpicker_ctrl/div/div/div/div[2]/i").click()
    time.sleep(1)
    driver.find_element_by_xpath("//div[@id='scColorSwatchesListDiv']/div[5]/div[2]/div").click()
    time.sleep(1)
    driver.find_element_by_css_selector("div.scListBtn.scBtn").click()
    time.sleep(1)
    #4-3-1. Open font dropdown menu
    driver.find_element_by_xpath("//div[2]/div[2]/div[2]/div[2]/div[2]/text_ctrl/div/div[2]/div/div[4]/i").click()
    #4-3-2. Select font from 9th from top of dropdown
    elem_path = "//div[2]/text_ctrl/div/div[2]/div/div[5]/div/ul/li[9]"
    driver.find_element_by_xpath(elem_path).location_once_scrolled_into_view
    driver.find_element_by_xpath(elem_path).click()
    #4-4. Set font size
    driver.find_element_by_xpath("(//input[@type='text'])[119]").clear()
    driver.find_element_by_xpath("(//input[@type='text'])[119]").send_keys("40")
    #4-5. Set line height
    driver.find_element_by_xpath("(//input[@type='text'])[120]").clear()
    driver.find_element_by_xpath("(//input[@type='text'])[120]").send_keys("30")
    #4-6. Set line alignment
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[20]/div[2]/div/div[2]/div[2]/div[2]/div[5]/div[2]/div[3]/i").click()
    #4-7. Set Character spacing
    driver.find_element_by_xpath("//div[6]/div[2]/text_ctrl/div/div[2]/div/div[4]/i").click()
    time.sleep(1)
    driver.find_element_by_xpath("(//input[@type='text'])[121]").clear()
    driver.find_element_by_xpath("(//input[@type='text'])[121]").send_keys("100")
    #4-8. Click OK to close text style manager dialog
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[20]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************

def toolbarShownOnlyStyles_AssertTest():
    global elem_01,elem_02,elem_03,elem_04,elem_05,elem_06,elem_07,elem_08
    # **********************************************************************************************
    # Configure BuyerSide configuration for item
    # **********************************************************
    print ("# Configure BuyerSide configuration for item")
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/context_panel/div//ul/li[6]").click()
    time.sleep(2)
    # **********************************************************
    # Deactivate all option except "Style"
    # **********************************************************
    # Style
    elem_01 = driver.find_element_by_xpath("//div[9]/div[2]/div/div/checkbox_ctrl/div/div/div")
    # CharAlign 
    elem_02 = driver.find_element_by_xpath("//div[9]/div[2]/div[2]/div/checkbox_ctrl/div/div/div")
    # LineHeight 
    elem_03 = driver.find_element_by_xpath("//div[9]/div[2]/div[3]/div/checkbox_ctrl/div/div/div")
    # ResetTextFormat 
    elem_04 = driver.find_element_by_xpath("//div[9]/div[2]/div[4]/div/checkbox_ctrl/div/div/div")
    # Font 
    elem_05 = driver.find_element_by_xpath("//div[9]/div[2]/div/div[2]/checkbox_ctrl/div/div/div")
    # FontSize 
    elem_06 = driver.find_element_by_xpath("//div[9]/div[2]/div[2]/div[2]/checkbox_ctrl/div/div/div")
    # FontColor 
    elem_07 = driver.find_element_by_xpath("//div[9]/div[2]/div[3]/div[2]/checkbox_ctrl/div/div/div")
    # TextFlow 
    elem_08 = driver.find_element_by_xpath("//div[9]/div[2]/div[4]/div[2]/checkbox_ctrl/div/div/div")
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    elem_01.location_once_scrolled_into_view
    # ***********************************************************
    # "Assert Text option's status - Expect Activated as default"
    print ("Assert Text option's status - Expect Activated as default")
    # ***********************************************************
    for i in range(8):
        i = i+1
        element = "elem_" + str("{0:02d}".format(i))
        assert 'scCheckbox icon-DS_Checkbox icon-DS_SelectedCheckbox' in globals()[element].get_attribute("class")
    # ***********************************************************
    # "Change and Assert Text option's status - Expect DeActivated by this"
    print ("Change and Assert Text option's status - Expect DeActivated by this")
    # ***********************************************************
    for i in range(7):
        i = i+2
        element = "elem_" + str("{0:02d}".format(i))
        globals()[element].click()
        assert 'scCheckbox icon-DS_Checkbox' in globals()[element].get_attribute("class")
    # **********************************************************
    # Assert Style option status - Expect Deactivated as default
    # **********************************************************
    print ("Assert Style option status - Expect Deactivated as default")
    element = driver.find_element_by_xpath("//div[6]/div/textstyles_panel/div/div[2]/div/div/checkbox_ctrl/div/div/div")
    element.get_attribute("class")
    assert 'scCheckbox icon-DS_Checkbox' in element.get_attribute("class")
    # Enable [Default] "Style" option
    print ("# Scroll where 'Style' option appears position")
    element.location_once_scrolled_into_view
    print ("# Enable [Default] 'Style' option")
    element.click()
    assert 'scCheckbox icon-DS_Checkbox icon-DS_SelectedCheckbox' in element.get_attribute("class")
    # **********************************************************
    # Assert Style option status - Expect Deactivated as default
    # **********************************************************
    print ("Assert Style option status - Expect Deactivated as default")
    element = driver.find_element_by_xpath("//div[6]/div/textstyles_panel/div/div[2]/div[2]/div/checkbox_ctrl/div/div/div")
    element.get_attribute("class")
    assert 'scCheckbox icon-DS_Checkbox' in element.get_attribute("class")
    # Enable new created "Style" option
    element.location_once_scrolled_into_view
    element.click()
    assert 'scCheckbox icon-DS_Checkbox icon-DS_SelectedCheckbox' in element.get_attribute("class")
    time.sleep(1)
    # **********************************************************************************************
    # Check Issue fix thru Buyer mode
    print ("# Check Issue fix thru Buyer mode")
    # Open View menu
    driver.find_element_by_xpath("//menu_panel/div/ul/li[4]/span").click()
    time.sleep(1)
    # Click 'Buyer mode view'
    driver.find_element_by_xpath("//li[4]/ul/li[6]").click()
    time.sleep(1)
    #7-1. Clean Up - All Content on Canvas
    print ("#7-1. Clean Up - All Content on Canvas")
    driver.find_element_by_css_selector(".scComposition").click()
    time.sleep(1)
    driver.find_element_by_css_selector(".scComposition").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_css_selector(".scComposition").send_keys(Keys.DELETE)
    time.sleep(2)
    # Add new text item
    driver.find_element_by_css_selector("div.button > i.icon-DS_Text").click()
    time.sleep(1)
    # Select Edit mode option
    driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]").click()
    # **********************************************************************************************
    print ("Assert display value of scTextToolbar")
    element = driver.find_element_by_id("scTextToolbar")
    element.get_attribute("style")
    assert 'display: block;' in element.get_attribute("style")
    # **********************************************************************************************
    # Check issue fix by adding new text item and applying new style for frame
    print ("# Check issue fix by adding new text item and applying new style for frame")
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").send_keys("Hello [[FirstName]]!")
    # Check whether we can apply new created "Style"
    driver.find_element_by_xpath("//div[2]/div/div[2]/i").click()
    time.sleep(1)
    # Set default "Style" at first
    driver.find_element_by_xpath("//div[42]/div/div/div").click()
    time.sleep(1)
    # Set new "Style"
    driver.find_element_by_xpath("//div[42]/div/div/div[2]").click()
    time.sleep(1)
    # Click "OK" button on Style selection dropdown menu
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[42]/div[2]/div").click()
    print ("close Buyer mode")
    driver.find_element_by_css_selector("div.button > i.icon-DS_Close").click()
    time.sleep(2)
    # **********************************************************************************************

def toolbarShownOnlyStyles_CleanupConfiguration():
    # **********************************************************************************************
    # Clean-Up Configuration
    print ("# Clean-Up Configuration")
    # Configure BuyerSide configuration for item
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/context_panel/div//ul/li[6]").click()
    # **********************************************************
    # Clean-Up - Activate all options
    # **********************************************************
    # Style
    elem_01 = driver.find_element_by_xpath("//div[9]/div[2]/div/div/checkbox_ctrl/div/div/div")
    # CharAlign 
    elem_02 = driver.find_element_by_xpath("//div[9]/div[2]/div[2]/div/checkbox_ctrl/div/div/div")
    # LineHeight 
    elem_03 = driver.find_element_by_xpath("//div[9]/div[2]/div[3]/div/checkbox_ctrl/div/div/div")
    # ResetTextFormat 
    elem_04 = driver.find_element_by_xpath("//div[9]/div[2]/div[4]/div/checkbox_ctrl/div/div/div")
    # Font 
    elem_05 = driver.find_element_by_xpath("//div[9]/div[2]/div/div[2]/checkbox_ctrl/div/div/div")
    # FontSize 
    elem_06 = driver.find_element_by_xpath("//div[9]/div[2]/div[2]/div[2]/checkbox_ctrl/div/div/div")
    # FontColor 
    elem_07 = driver.find_element_by_xpath("//div[9]/div[2]/div[3]/div[2]/checkbox_ctrl/div/div/div")
    # TextFlow 
    elem_08 = driver.find_element_by_xpath("//div[9]/div[2]/div[4]/div[2]/checkbox_ctrl/div/div/div")
    # ***********************************************************
    # "Clean-Up Assert Text option's status - Expect Activated as default"
    print ("Clean-Up Assert Text option's status - Expect Activated as default")
    # ***********************************************************
    for i in range(1):
        i = i+1
        element = "elem_" + str("{0:02d}".format(i))
        assert 'scCheckbox icon-DS_Checkbox icon-DS_SelectedCheckbox' in globals()[element].get_attribute("class")
    # ***********************************************************
    # "Clean-Up Change and Assert Text option's status - Expect DeActivated by this"
    print ("Clean-Up Change and Assert Text option's status - Expect DeActivated by this")
    # ***********************************************************
    for i in range(7):
        i = i+2
        element = "elem_" + str("{0:02d}".format(i))
        globals()[element].click()
        assert 'scCheckbox icon-DS_Checkbox' in globals()[element].get_attribute("class")
    # ************************************************************************
    # Clean-Up Assert Style option status - Expect Deactivated as default --- [Default]
    # ************************************************************************
    print ("Clean-Up Assert Style option status - Expect Deactivated as default --- [Default]")
    element = driver.find_element_by_xpath("//div[6]/div/textstyles_panel/div/div[2]/div/div/checkbox_ctrl/div/div/div")
    element.get_attribute("class")
    assert 'scCheckbox icon-DS_Checkbox icon-DS_SelectedCheckbox' in element.get_attribute("class")
    # Clean-Up Enable [Default] "Style" option
    element.click()
    assert 'scCheckbox icon-DS_Checkbox' in element.get_attribute("class")
    # ************************************************************************
    # Clean-Up Assert Style option status - Expect Deactivated as default --- new created "Style"
    # ************************************************************************
    print ("Clean-Up Assert Style option status - Expect Deactivated as default --- new created 'Style'")
    element = driver.find_element_by_xpath("//div[6]/div/textstyles_panel/div/div[2]/div[2]/div/checkbox_ctrl/div/div/div")
    element.get_attribute("class")
    assert 'scCheckbox icon-DS_Checkbox icon-DS_SelectedCheckbox' in element.get_attribute("class")
    # Clean-Up Enable new created "Style" option
    element.click()
    assert 'scCheckbox icon-DS_Checkbox' in element.get_attribute("class")
    time.sleep(1)
    # **********************************************************************************************

def toolbarShownOnlyStyles_CleanupStyle():
    # **********************************************************************************************
    # Clean-Up Style
    print ("# Clean-Up Style")
    # Select top item on the layer
    driver.find_element_by_css_selector("span.scLayerItemName").click()
    #4-1-1. Open Style dropdown menu
    driver.find_element_by_xpath("//div[2]/div/div/text_style_ctrl/div/div/div[2]/div/div").click()
    # Select target style for deletion
    driver.find_element_by_xpath("//div[42]/div/div/div[2]/div/div/div").click()
    # Click Delete button
    driver.find_element_by_xpath("//div[@id='scContainerCtrl']/main_view/div/div[42]/div[2]/div[2]/div[4]/i").click()
    # **********************************************************************************************

def toolbarShownOnlyStyles_CleanupFont():
    # **********************************************************************************************
    #9. Clean Up - Manage fonts
    print ("#9. Manage fonts")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
    time.sleep(2)
    print ("#9-1. Delete Font")
    elem_path = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[8]/div/span"
    driver.find_element_by_xpath(elem_path).click()
    #driver.find_element_by_css_selector("span.title").click()
    driver.find_element_by_css_selector("div.scListBtn").click()
    print ("#9-1. Confirm deletion on alert window")
    alert = driver.switch_to_alert()
    alert.accept()
    print ("#9-3. Close dialog")
    driver.find_element_by_xpath("//div[10]/div[3]/div").click()
    time.sleep(2)
    # **********************************************************************************************

def DSF_31075_AddFonts():
    # **********************************************************************************************
    #9. Manage fonts
    print ("#9. Manage fonts")
    testFont001="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-Bd.otf" 
    testFont002="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-BdIt.otf" 
    testFont003="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-Lt.otf" 
    testFont004="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-LtIt.otf" 
    elem_path_001 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div/div/span"
    elem_path_002 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[2]/div/span"
    elem_path_003 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[3]/div/span"
    elem_path_004 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[4]/div/span"
    testFontName001="Helvetica Neue LT Std -Bold" 
    testFontName002="Helvetica Neue LT Std -Bold Italic" 
    testFontName003="Helvetica Neue LT Std -Light" 
    testFontName004="Helvetica Neue LT Std -Light Italic"
    #9-1-1. Open Menu dropdown - First 2 records loop
    print("#9-1-1. Open Menu dropdown - First 2 records loop")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    #9-2. Open Font Manager Dialog
    print ("#9-2. Open Font Manager Dialog")
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
    time.sleep(2)
    for i in range(2):
        print ("#9-3. Set Upload Font path")
        #9-3. Set Upload Font path
        testFont = eval("testFont"+"{0:03d}".format(i+1))
        testFontName = eval("testFontName"+"{0:03d}".format(i+1))
        #9-4. Set Expected Font xpath in the list menu
        print ("#9-4. Set Expected Font xpath in the list menu")
        testFontPath = eval("elem_path_"+"{0:03d}".format(i+1))
        print ("#9-5. Uploading font :" + str(testFontName))
        driver.file_detector.is_local_file()
        driver.find_element_by_css_selector('input[name="selectedFonts"]').send_keys(testFont)
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.XPATH,testFontPath)))
        time.sleep(3)
    print ("#9-6. Close dialog")
    driver.find_element_by_xpath("//div[10]/div[3]/div").click()
    time.sleep(5)
    #9-1-2. Open Menu dropdown - Last 2 records loop
    print("#9-1-2. Open Menu dropdown - Last 2 records loop")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    #9-2. Open Font Manager Dialog
    print ("#9-2. Open Font Manager Dialog")
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
    time.sleep(2)
    for i in range(2):
        print ("#9-3. Set Upload Font path")
        #9-3. Set Upload Font path
        testFont = eval("testFont"+"{0:03d}".format(i+3))
        testFontName = eval("testFontName"+"{0:03d}".format(i+3))
        #9-4. Set Expected Font xpath in the list menu
        print ("#9-4. Set Expected Font xpath in the list menu")
        testFontPath = eval("elem_path_"+"{0:03d}".format(i+3))
        print ("#9-5. Uploading font :" + str(testFontName))
        driver.file_detector.is_local_file()
        driver.find_element_by_css_selector('input[name="selectedFonts"]').send_keys(testFont)
        wait = WebDriverWait(driver, 10)
        wait.until(EC.visibility_of_element_located((By.XPATH,testFontPath)))
        time.sleep(3)
    print ("#9-6. Close dialog")
    driver.find_element_by_xpath("//div[10]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************

def DSF_31075_AddItems():
    # **********************************************************************************************
    #3. Add items
    print ("#3. Add items")
    testFont001="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-Bd.otf" 
    testFont002="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-BdIt.otf" 
    testFont003="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-Lt.otf" 
    testFont004="C:\\temp\\SmartCanvas_FontUpload\\CRM-Attachment_HelveticaNeueLTStd-LtIt.otf" 
    elem_path_001 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div/div/span"
    elem_path_002 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[2]/div/span"
    elem_path_003 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[3]/div/span"
    elem_path_004 = "//div[@id='scContainerCtrl']/main_view/div/div[10]/div[2]/div/div/div/div/div[4]/div/span"
    testFontName001="Helvetica Neue LT Std -Bold" 
    testFontName002="Helvetica Neue LT Std -Bold Italic" 
    testFontName003="Helvetica Neue LT Std -Light" 
    testFontName004="Helvetica Neue LT Std -Light Italic"
    for i in range(4):
        #3-1. Set Font Name
        print ("#3-1. Set Font Name")
        testFontName = eval("testFontName"+"{0:03d}".format(i+1))
        print ("#3-1-1. Set Font Name : " + str(testFontName))
        #3-2. Add Text frame, and Edit
        print ("#3-2. Add Text frame, and Edit")
        driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
        driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
        if TestOnSauceLabs == True:
            print ("#3-2-1. Wait dynamically about appearance of next item due to execution on SauceLabs")
            elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
            driver.find_element_by_css_selector(elem_css).click()
        else:
            print ("#3-2-1. Skip dynamically waiting about appearance of next item due to local execution")
        time.sleep(1)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        time.sleep(2)
        driver.find_element_by_xpath("//div").send_keys(testFontName)
        time.sleep(2)
        driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
        time.sleep(2)
        #3-3. Change font size
        print ("#3-3. Change font size")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
        driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
        time.sleep(2)
        #3-4. Set font style
        print ("#3-4. Set font style")
        #3-5. Open Font selection dropdown
        print ("#3-5. Open Font selection dropdown")
        driver.find_element_by_xpath("//div[2]/div/div[2]/div[2]/div/div/text_ctrl/div/div[2]/div/div[4]/i").click()
        #3-6. Select Font from selection
        print ("#3-6. Select Font from selection")
        targetFontPath_dropdown = "//div[2]/div/div[2]/div[2]/div/div/text_ctrl/div/div[2]/div/div[5]/div/ul/li[" + str(i+2) + "]"
        driver.find_element_by_xpath(targetFontPath_dropdown).click()
        time.sleep(1)
        #3-7. Position frame
        #print ("#3-7. Position frame")
        driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li").click()
        #3-8. Select Top
        print("#3-8. Select Top")
        driver.find_element_by_css_selector("span.scLayerItemName").click()
        #3-9. Open 'Layout' context tab menu
        print ("#3-9. Open 'Layout' context tab menu")
        driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
        #3-10. Set X position of the element - 'X =< 7 inch'
        #print("#3-10. Set X position of the element - 'X =< 7 inch'")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
        #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
        #3-11. Set Y position of the element - 'Y =< 7 inch'
        print("#3-11. Set Y position of the element - 'Y =< 7 inch'")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1))
        driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
        print ("#3-12. Add Text frame, and Edit - " + "{0:03d}".format(i+1)+ "/004")
        time.sleep(1)
    # **********************************************************************************************

def DSF_31523_AddItems():
    # *********************************************************************************
    #1. Set switch for input element counter 
    print ("#1. Set switch for input element counter ")
    Xposition_path = "(//input[@type='text'])[23]"
    Yposition_path = "(//input[@type='text'])[24]"
    Width_path = "(//input[@type='text'])[25]"
    Height_path = "(//input[@type='text'])[26]"
    # *********************************************************************************
    #3. Add items
    print ("#3. Add items")
    #3-1-1. Add Rectangle Box element and Position, Resize, Change Color, Set to Background
    print("#3-1-1. Add Rectangle Box element and Position, Resize, Change Color, Set to Background")
    driver.find_element_by_css_selector("i.icon-DS_Checkbox").click()
    #3-1-2. Get ID information of this item
    print("#3-1-2. Get ID information of this item")
    myItemID=driver.find_element_by_xpath("//div[1]/main_view/div/context_panel/div/div[3]/div/div[4]/div[2]/div/div/colorpicker_ctrl/div/div[1]").get_attribute("id")
    print(myItemID)
    #3-1-3. Set X position of the element - 'X = 0 inch'
    print("#3-1-3. Set X position of the element - 'X = 0 inch'")
    X = driver.find_element_by_xpath(Xposition_path)
    X.click()
    time.sleep(0.3)
    X.send_keys(Keys.CONTROL, "a")
    X.send_keys("0")
    X.send_keys(Keys.RETURN)
    #3-1-4. Set Y position of the element - 'Y = 0 inch'
    print("#3-1-4. Set Y position of the element - 'Y = 0 inch'")
    Y = driver.find_element_by_xpath(Yposition_path)
    Y.click()
    time.sleep(0.3)
    Y.send_keys(Keys.CONTROL, "a")
    Y.send_keys("0")
    Y.send_keys(Keys.RETURN)
    #3-1-5. Set Width of the element - 'W = 11.69291 inch'
    print("#3-1-5. Set Width of the element - 'W = 11.69291 inch'")
    W = driver.find_element_by_xpath(Width_path)
    W.click()
    time.sleep(0.3)
    W.send_keys(Keys.CONTROL, "a")
    W.send_keys("11.69291")
    W.send_keys(Keys.RETURN)
    #3-1-6. Set Height of the element - 'H = 8.26772 inch'
    print("#3-1-6. Set Height of the element - 'H = 8.26772 inch'")
    H = driver.find_element_by_xpath(Height_path)
    H.click()
    time.sleep(0.3)
    H.send_keys(Keys.CONTROL, "a")
    H.send_keys("8.26772")
    H.send_keys(Keys.RETURN)
    #3-1-7. Change color'
    print("#3-1-7. Change color'")
    #cssID="#"+myItemID+ " > div.scFormCtrl > div.scCtrlColorPicker"
    #print(cssID)
    driver.find_element_by_xpath("//div[3]/div/div[4]/div[2]/div/div/colorpicker_ctrl/div/div/div/div").click()
    driver.find_element_by_xpath("//div[@id='scColorSwatchesListDiv']/div[16]/div[2]/div").click()
    time.sleep(1)
    #3-2-1. Add Rectangle Box element and Position, Resize
    print("#3-2-1. Add Rectangle Box element and Position, Resize")
    driver.find_element_by_css_selector("i.icon-DS_Checkbox").click()
    #3-2-2. Get ID information of this item
    print("#3-2-2. Get ID information of this item")
    myItemID=driver.find_element_by_xpath("//div[1]/main_view/div/context_panel/div/div[3]/div/div[4]/div[2]/div/div/colorpicker_ctrl/div/div[1]").get_attribute("id")
    print(myItemID)
    #3-2-3. Set X position of the element - 'X = 0.3 inch'
    print("#3-2-3. Set X position of the element - 'X = 0.3 inch'")
    X.click()
    time.sleep(0.3)
    X.send_keys(Keys.CONTROL, "a")
    X.send_keys("0.3")
    X.send_keys(Keys.RETURN)
    #3-2-4. Set Y position of the element - 'Y = 0 inch'
    print("#3-2-4. Set Y position of the element - 'Y = 0 inch'")
    Y.click()
    time.sleep(0.3)
    Y.send_keys(Keys.CONTROL, "a")
    Y.send_keys("0.3")
    Y.send_keys(Keys.RETURN)
    #3-2-5. Set Width of the element - 'W = 11.69291 inch'
    print("#3-2-5. Set Width of the element - 'W = 11.69291 inch'")
    W.click()
    time.sleep(0.3)
    W.send_keys(Keys.CONTROL, "a")
    W.send_keys("11.09291")
    W.send_keys(Keys.RETURN)
    #3-2-6. Set Height of the element - 'H = 8.26772 inch'
    print("#3-2-6. Set Height of the element - 'H = 8.26772 inch'")
    H.click()
    time.sleep(0.3)
    H.send_keys(Keys.CONTROL, "a")
    H.send_keys("7.66772")
    H.send_keys(Keys.RETURN)
    # *********************************************************************************

def DSF_31523_ConfigureDocumentSetting():
    global CurrentInputNumber
    # *****************************************************************************************
    #4. Document settings
    print ("#4. Document settings")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
    time.sleep(0.3)
    #4-1. Configure "Document Setting"
    #Ver7.3.0.85 - [149]
    #Ver7.3.0.85 - DSMI document preview rendering : True - [150]
    #Ver7.3.2.125 - [149] all time
    CurrentInputNumber = "149"
    # Margin 
    for i in range(4):
        elem_xpath = "(//input[@type='text'])["+str(i+int(CurrentInputNumber)) +"]"
        E = driver.find_element_by_xpath(elem_xpath)
        E.click()
        time.sleep(0.3)
        E.send_keys(Keys.CONTROL, "a")
        E.send_keys("0.3")
        E.send_keys(Keys.RETURN)
    #4-2. Activate '#print bleed active' option
    print ("#4-2. Activate '#print bleed active' option")
    elem_path = "//div/main_view/div/div[34]/div[2]/div/div[13]/div/checkbox_ctrl/div/div/div"
    driver.find_element_by_xpath(elem_path).click()
    time.sleep(1)
    # Print bleed area
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    elem_xpath = "(//input[@type='text'])["+str(4+int(CurrentInputNumber)) +"]"
    E.location_once_scrolled_into_view
    for i in range(4):
        # Print bleed area
        elem_xpath = "(//input[@type='text'])["+str(i+4+int(CurrentInputNumber)) +"]"
        E = driver.find_element_by_xpath(elem_xpath)
        E.click()
        time.sleep(0.3)
        E.send_keys(Keys.CONTROL, "a")
        E.send_keys("0.125")
        E.send_keys(Keys.RETURN)
    # Display bleed and offset
    for i in range(6):
        elem_xpath = "(//input[@type='text'])["+str(i+8+int(CurrentInputNumber)) +"]"
        E = driver.find_element_by_xpath(elem_xpath)
        E.click()
        time.sleep(0.3)
        E.send_keys(Keys.CONTROL, "a")
        E.send_keys("0.2")
        E.send_keys(Keys.RETURN)
    #4-5. DSMI document for preview rendering
    #driver.find_element_by_xpath("//div/main_view/div/div[34]/div[2]/div/div[23]/div/checkbox_ctrl/div/div/div").click()
    time.sleep(0.3)
    #4-6. Close dialog
    print ("#4-6. Close dialog")
    driver.find_element_by_xpath("//div[34]/div[3]/div").click()
    time.sleep(0.3)
    return CurrentInputNumber
    # *****************************************************************************************


def DSF_31523_CleanupDocumentSetting():
    # **********************************************************************************************
    #17. Clean Up
    print ("#17. Clean Up")
    #17-. Clean Up - Document settings
    print ("#17-. Clean Up - Document settings")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    time.sleep(1)
    driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
    print ("#17--1. Clean Up - Document settings")
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    elem_css = "div.scCol-8 > div.scFormCtrl > div.scCtrlDropdown > i.icon-DS_ArrowDown"
    driver.find_element_by_css_selector(elem_css).location_once_scrolled_into_view
    # Margin 
    print ("#17--2. Clean Up - Document settings -- Margin")
    for i in range(4):
        elem_xpath = "(//input[@type='text'])["+str(i+int(CurrentInputNumber)) +"]"
        E = driver.find_element_by_xpath(elem_xpath)
        E.clear()
        E.send_keys("0")
        E.send_keys(Keys.RETURN)
    # Print bleed area
    print ("#17--3. Clean Up - Document settings -- Print bleed area")
    # ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
    elem_xpath = "(//input[@type='text'])["+str(4+int(CurrentInputNumber)) +"]"
    E.location_once_scrolled_into_view
    for i in range(4):
        # Print bleed area
        elem_xpath = "(//input[@type='text'])["+str(i+4+int(CurrentInputNumber)) +"]"
        E = driver.find_element_by_xpath(elem_xpath)
        E.clear()
        E.send_keys("0")
        E.send_keys(Keys.RETURN)
    # Activate '#print bleed active' option
    print ("#17--3. Clean Up - Document settings -- Deactivate 'Print bleed active' option")
    driver.find_element_by_xpath("//div/main_view/div/div[34]/div[2]/div/div[13]/div/checkbox_ctrl/div/div/div").click()
    time.sleep(1)
    # Display bleed and offset
    for i in range(6):
        elem_xpath = "(//input[@type='text'])["+str(i+8+int(CurrentInputNumber)) +"]"
        E = driver.find_element_by_xpath(elem_xpath)
        E.clear()
        E.send_keys("0")
        E.send_keys(Keys.RETURN)
    #8-5. DSMI document for preview rendering
    #driver.find_element_by_xpath("//div[23]/div/checkbox_ctrl/div/div/div").click()
    time.sleep(1)
    #8-6. Close dialog
    print ("#8-6. Close dialog")
    driver.find_element_by_xpath("//div[34]/div[3]/div").click()
    time.sleep(1)
    # **********************************************************************************************

def DSF_32455_GT():
    # *********************************************************************************
    # Testcase - Create new FormField
    # *********************************************************************************
    print ("#3-1. Set name for FormField")
    # Test 1: Set name for FormField
    str001 = 'myForm_01'
    str002 = 'myForm_02'

    # *********************************************************************************
    print ("#3-2. Set expected registered name for FormField")
    # Test 1: Set expected registered name for FormField
    NoSpecialStr001 = "[[myForm_01]]"
    NoSpecialStr002 = "[[myForm_02]]"
    # *********************************************************************************

    # *********************************************************************************
    print ("#3-3. Set default value for FormField")
    # Test 1: Set default value for FormField
    defVal001 = "0"
    defVal002 = "DSF-32455"
    # *********************************************************************************

    # *********************************************************************************
    #4. Current Window Size
    winSize = driver.get_window_size()
    print ("#4. Current Window Size: " + str(winSize))
    #4-1. Expand Windows Size to be able to recieve all command to add FormFields
    print("#4-1. Expand Windows Size to be able to recieve all command to add FormFields")
    driver.set_window_size(1280,2000)
    # *********************************************************************************

    # *********************************************************************************
    #5. Add FormField
    print("#5. Add New FormField")
    for i in range(2):
        driver.find_element_by_css_selector("div.scContextBlockContent > div.scBtn").click()
        time.sleep(1)
        assTxt = "[[FormField0]]"
        #print (assTxt)
        InputNr = "(//input[@type='text'])[" + str(i*2+38) + "]"
        InputNr2 = "(//input[@type='text'])[" + str(i*2+39) + "]"
        strNr = eval("str"+"{0:03d}".format(i+1))
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        strNr3 = eval("defVal"+"{0:03d}".format(i+1))
        #print (InputNr)
        #print (strNr)
        if str(i) == "0":
            #Xpath = "//label_ctrl/div/div"
            css_path="div.scTitle"
            assert assTxt in driver.find_element_by_css_selector(css_path).text
            #print (Xpath)
            #assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_css_selector(css_path).click()
            time.sleep(1)
            #driver.find_element_by_xpath(Xpath).click()
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            #assert strNr2 in driver.find_element_by_xpath(Xpath).text
            assert strNr2 in driver.find_element_by_css_selector(css_path).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr3)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-" + "{0:03d}".format(i+1) + " - Add New FormField")
        else:
            Xpath = "//div[" + str(i*2+1) + "]/div/label_ctrl/div/div"
            #print (Xpath)
            assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(Xpath).click()
            time.sleep(1)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            assert strNr2 in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr3)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-001-" + "{0:03d}".format(i+1) + " - Add New FormField")
    time.sleep(1)
    # *********************************************************************************

    # *********************************************************************************
    #6. Testcase - Create new Switch
    print ("#6. Testcase - Create new Switch")
    # *********************************************************************************
    #6-1. Click "Add Switch" button
    print ("#6-1. Click 'Add Switch' button")
    # ********************************
    driver.find_element_by_xpath("//div[3]/div[2]/div[2]").click()
    # ********************************
    #6-2. Click "Edit Switch" button of the first one
    # ********************************
    print ("#6-2. Click 'Edit Switch' button of the first one")
    driver.find_element_by_xpath("//div[3]/div[2]/div/div/div[2]/div/i").click()
    # ********************************
    #6-3. Create Switch Condition
    print ("#6-3. Create Switch Condition")
    # ********************************
    elem = "//div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).click()
    time.sleep(1)
    # ********************************
    #6-4. Set Target field
    print ("#6-4. Set Target field")
    # ********************************
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath(elem).send_keys("[[myForm_01]]")
    driver.find_element_by_xpath(elem).send_keys(Keys.RETURN)
    time.sleep(1)
    # ********************************
    #6-5. Change opertor of the condition
    print ("#6-5. Change opertor of the condition")
    # ********************************
    elem = "//div[1]/main_view/div/div[32]/div[2]/div/div/div/div/div/div/div/div/div[1]/div/div/text_ctrl/div/div[2]/div/div[3]/i"
    driver.find_element_by_xpath(elem).click()
    elem_eq = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    elem_neq = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    elem_contain = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[3]"
    elem_gt = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[4]"
    elem_lt = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[5]"
    elem_strw = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[6]"
    elem_endw = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[7]"
    driver.find_element_by_xpath(elem_gt).click()
    # ********************************
    #6-7. Set Value field
    print ("#6-7. Set Value field")
    # ********************************
    elem = "//div/div/div/div/div/div/div/div/div[2]/div/div/div"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).click()
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath(elem).send_keys("10")
    driver.find_element_by_xpath(elem).send_keys(Keys.RETURN)
    time.sleep(1)
    # ********************************
    #6-8. Close Switch Designer dialog
    print ("#6-8. Close Switch Designer dialog")
    # ********************************
    elem = "//div[32]/div[3]/div"
    driver.find_element_by_xpath(elem).click()
    # ********************************
    #6-9. Set name for Switch
    print ("#6-9. Set name for Switch")
    # ********************************
    elem = "//div[3]/div[2]/div/div/div/label_ctrl/div/div"
    driver.find_element_by_xpath(elem).click()
    elem = "(//input[@type='text'])[42]"
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath(elem).send_keys("GT")
    driver.find_element_by_xpath(elem).send_keys(Keys.RETURN)
    time.sleep(1)
    # *********************************************************************************

    # *********************************************************************************
    #7. Reset Windows Size to default
    print ("#7. Reset Windows Size to default")
    driver.set_window_size(1040,784)
    # *********************************************************************************

    # *********************************************************************************
    # Testcase - Create new Variable
    # *********************************************************************************

    # *********************************************************************************
    #8. Testcase - Create new Layer
    print ("#8. Testcase - Create new Layer")
    # *********************************************************************************
    #8-1. Create New Layer - GT
    print ("#8-1. Create New Layer - GT")
    # ********************************
    elem_css = "div.scLayersPanelToolbar > div.scIconBtn > i.icon-DS_Add"
    driver.find_element_by_css_selector(elem_css).click()
    # ********************************
    #8-2. Select created Layer
    print ("#8-2. Select created Layer")
    # ********************************
    elem_css = "div.scLayerNameLabel"
    driver.find_element_by_css_selector(elem_css).click()
    # ********************************
    ##8-3. Double Click new layer for renaming
    #print ("#8-3. Double Click new layer for renaming")
    # ********************************
    #actionchains = ActionChains(driver)
    #element = driver.find_element_by_css_selector(elem_css)
    #actionchains.double_click(element).perform()
    # ********************************
    ##8-4. Name Layer
    #print ("#8-4. Name Layer")
    # ********************************
    # ********************************
    #8-5. Select Condition for the Layer
    print ("#8-5. Select Condition for the Layer")
    # ********************************
    elem_css = "span.scSwitchName"
    driver.find_element_by_css_selector(elem_css).click()
    # ********************************
    #8-6. Set possible Xpath
    print ("#8-6. Set possible Xpath")
    # ********************************
    elem_alwaysOn = "//div[42]/div/div/div/div/div"
    elem_alwaysHide = "//div[42]/div/div/div[2]/div/div"
    elem_selection = "//div[42]/div/div/div[3]/div/small"
    elem_GT = "//div[42]/div/div/div[4]/div/div"
    # ********************************
    #8-9. Assign Switch to layer
    print ("#8-9. Assign Switch to layer")
    # ********************************
    driver.find_element_by_xpath(elem_GT).click()
    #elem = ""
    #driver.find_element_by_css_selector(elem).click()
    #driver.find_element_by_xpath(elem).click()
    time.sleep(1)
    # *********************************************************************************

    # *********************************************************************************
    #9. Add items
    print ("#9. Add items")
    # ********************************
    #9-1. Add Text frame, and Edit
    print ("#9-1. Add Text frame, and Edit")
    # ********************************
    driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
    driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
    if TestOnSauceLabs == True:
        print ("#9-2-1. Wait dynamically about appearance of next item due to execution on SauceLabs")
        elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
        wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
        driver.find_element_by_css_selector(elem_css).click()
    else:
        print ("#9-2-1. Skip dynamically waiting about appearance of next item due to local execution")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys("DevOps DSF-32455!")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    # ********************************
    #9-2-2. Change font size
    print ("#9-2-2. Change font size")
    # ********************************
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
    time.sleep(1)
    # ********************************
    #9-3. Ensure font has been changed
    # ********************************
    print ("#9-3. Ensure font has been changed")
    driver.find_element_by_xpath("//div").click()
    time.sleep(2)
    # *********************************************************************************

    # *********************************************************************************
    #10. Work on Buyer Mode - View
    print ("#10. Work on Buyer Mode - View")
    # ********************************
    #10-1. Open View menu
    print ("#10-1. Open View menu")
    # ********************************
    driver.find_element_by_xpath("//menu_panel/div/ul/li[4]/span").click()
    time.sleep(1)
    # ********************************
    #10-2. Click 'Buyer mode view'
    print ("#10-2. Click 'Buyer mode view'")
    # ********************************
    driver.find_element_by_xpath("//li[4]/ul/li[6]").click()
    time.sleep(1)
    # ****************************************************************
    #10-3. Type value - Test 01
    print ("#10-3. Type value - Test 01")
    # ****************************************************************
    # Use below for Ver7.2.9.x
    #elem = "(//input[@type='text'])[178]"
    # Use below for > Ver7.3.0.74
    #elem = "(//input[@type='text'])[181]"
    elem = "//div[41]/div/div[2]/div/div[2]/div/text_ctrl/div/div[2]/div/div[2]/input"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys("dwsdssdsdsdsssdsd")
    driver.find_element_by_xpath(elem).send_keys(Keys.ENTER)
    # ********************************
    #10-4. Switch Condition Evaluation - 01
    print ("#10-4. Switch Condition Evaluation - 01")
    # ********************************
    # ********************************
    #10-5. Assert text frame appearance on SmartCanvas UI - 01
    print ("#10-5. Assert text frame appearance on SmartCanvas UI - 01")
    # ********************************
    element = driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/div[1]/div/div/div/div/div/div")
    element.get_attribute("style")
    assert 'display: none;' in element.get_attribute("style")
    time.sleep(2)
    # ****************************************************************
    #10-6. Type value - Test 02
    print ("#10-6. Type value - Test 02")
    # ****************************************************************
    # Use below for Ver7.2.9.x
    #elem = "(//input[@type='text'])[178]"
    # Use below for > Ver7.3.0.74
    #elem = "(//input[@type='text'])[181]"
    elem = "//div[41]/div/div[2]/div/div[2]/div/text_ctrl/div/div[2]/div/div[2]/input"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys("10")
    driver.find_element_by_xpath(elem).send_keys(Keys.ENTER)
    # ********************************
    #10-7. Switch Condition Evaluation - 02
    print ("#10-7. Switch Condition Evaluation - 02")
    # ********************************
    # ********************************
    #10-8. Assert text frame appearance on SmartCanvas UI - 02
    print ("#10-8. Assert text frame appearance on SmartCanvas UI - 02")
    # ********************************
    element = driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/div[1]/div/div/div/div/div/div")
    element.get_attribute("style")
    assert 'display: none;' in element.get_attribute("style")
    time.sleep(2)
    # ****************************************************************
    #10-9. Type value - Test 03
    print ("#10-9. Type value - Test 03")
    # ****************************************************************
    # Use below for Ver7.2.9.x
    #elem = "(//input[@type='text'])[178]"
    # Use below for > Ver7.3.0.74
    #elem = "(//input[@type='text'])[181]"
    elem = "//div[41]/div/div[2]/div/div[2]/div/text_ctrl/div/div[2]/div/div[2]/input"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys("11")
    driver.find_element_by_xpath(elem).send_keys(Keys.ENTER)
    time.sleep(1)
    # ********************************
    #10-10. Switch Condition Evaluation - 03
    print ("#10-10. Switch Condition Evaluation - 03")
    # ********************************
    # ********************************
    #10-11. Assert text frame appearance on SmartCanvas UI - 03
    print ("#10-11. Assert text frame appearance on SmartCanvas UI - 03")
    # ********************************
    element = driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/div[1]/div/div/div/div/div/div")
    element.get_attribute("style")
    assert 'display: block;' in element.get_attribute("style")
    time.sleep(2)
    # ****************************************************************
    # ********************************
    #10-12. Buyer mode - Save Document
    print ("#10-12. Buyer mode - Save Document")
    # ********************************
    elem = "i.icon-DS_Save"
    driver.find_element_by_css_selector(elem).click()
    time.sleep(3)
    # ********************************
    #10-13. Buyer mode - Get Preview PDF
    print ("#10-13. Buyer mode - Get Preview PDF")
    # ********************************
    elem = "i.icon-DS_PDF"
    driver.find_element_by_css_selector(elem).click()
    # ********************************
    #10-14. Wait PDF rendering until 'Download PDF' link appear
    print ("#10-14. Wait PDF rendering until 'Download PDF' link appear")
    # ********************************
    wait = WebDriverWait(driver, 60)
    wait.until(EC.visibility_of_element_located((By.LINK_TEXT, "Download PDF")))
    # ********************************
    #10-15. Get part of link to be used in post action from "Download PDF" button
    print ("#10-15. Get part of link to be used in post action from 'Download PDF' button")
    # ********************************
    DL_LINK_LOW = driver.find_element_by_link_text("Download PDF").get_attribute("href")
    p_LOW = re.compile("(?<=pdf=)(.+)")
    m_LOW = p_LOW.search(DL_LINK_LOW)
    myPDFinfo_LOW = m_LOW.group(0)
    # ********************************
    #10-16. Get part of DSMO url
    print ("#10-16. Get part of DSMO url")
    # ********************************
    p2_LOW = re.compile("^http(.+)/")
    m2_LOW = p2_LOW.search(DL_LINK_LOW)
    myDSMOURL_LOW = m2_LOW.group(0)
    # ********************************
    #10-17. Click OK button to close dialog
    print ("#10-17. Click OK button to close dialog")
    # ********************************
    driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
    time.sleep(1)
    # ****************************************************************
    #10-18. Close Buyer mode
    print ("#10-18. Close Buyer mode")
    driver.find_element_by_css_selector("div.button > i.icon-DS_Close").click()
    time.sleep(2)
    # *********************************************************************************

    # *********************************************************************************
    #11. Clean-Up Operation
    # *********************************************************************************
    #11-1. Clean Up - All Content on Canvas
    print ("#7-1. Clean Up - All Content on Canvas")
    driver.find_element_by_css_selector(".scComposition").click()
    driver.find_element_by_css_selector(".scComposition").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_css_selector(".scComposition").send_keys(Keys.DELETE)
    time.sleep(2)
    # ****************************************************************
    #11-2. Clean Up - FormField
    print ("#11-2. Clean Up - FormField")
    # ****************************************************************
    for i in range(2):
        driver.find_element_by_css_selector("div.scCol-1 > div.scIconBtn > i.icon-DS_TrashCan").click()
        time.sleep(1)
    # ****************************************************************
    #11-3. Clean Up - Switch
    print ("#11-3. Clean Up - Switch")
    # ****************************************************************
    elem = "//div[@id='scContainerCtrl']/main_view/div/context_panel/div/div[4]/div/div[3]/div[2]/div/div/div[3]/div/i"
    driver.find_element_by_xpath(elem).click()
    # ****************************************************************
    #11-4. Clean Up - Layer
    print ("#11-4. Clean Up - Layer")
    # ****************************************************************
    elem_css = "div.scLayerNameLabel"
    driver.find_element_by_css_selector(elem_css).click()
    time.sleep(1)
    elem_css = "div.scLayersPanelToolbar > div.scIconBtn > i.icon-DS_TrashCan"
    driver.find_element_by_css_selector(elem_css).click()
    time.sleep(1)
    # ****************************************************************
    # Click OK button to proceed deletion (due to remains content inside of the target layer)
    # Comment out here as we delete all item in above
    #elem_css = "div.scModalFooter > div.scBtn"
    #driver.find_element_by_css_selector(elem_css).click()
    #time.sleep(1)
    # *********************************************************************************

    # *********************************************************************************
    #12. Preview PDF: Open DSMO GetImage interface converted PDF page as png and jpg 
    print ("#12. Preview PDF: Open DSMO GetImage interface converted PDF page to PNG and JPG")
    # ************************************
    # Loop as page number amount - optional for multi page PDF support
    for i in range(1):
    # ************************************
        # Loop for Low & High Resolution PDF Output verification
        for j in range(1):
            # Link Generation - Low Resolution
            if str(j) == "0":
                #Loop boolean as PNG and JPG format link generation
                for h in range(2):
                    if str(h) == "0":
                        #0. Set general variable with leading 0
                        url = "url_" + "{0:03d}".format(i+j+h+1)
                        #1. LowRes PDF as - PNG
                        print ("#Output - LowRes PDF as - PNG - 600px")
                        url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
                        print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
                    else:
                        #0. Set general variable with leading 0
                        url = "url_" + "{0:03d}".format(i+j+h+1)
                        #2. LowRes PDF as - JPG
                        print ("#Output - LowRes PDF as - JPG - 600px")
                        url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
                        print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
            # Link Generation - High Resolution
            else:
                #Loop boolean as PNG and JPG format link generation
                for h in range(2):
                    if str(h) == "0":
                        #0. Set general variable with leading 0
                        url = "url_" + "{0:03d}".format(i+j+h+1)
                        #3. HighRes PDF as PNG
                        print ("#Output - HighRes PDF as PNG - 1500px")
                        url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
                        print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
                    else:
                        #0. Set general variable with leading 0
                        url = "url_" + "{0:03d}".format(i+j+h+1)
                        #4. HighRes PDF as JPG
                        print ("#Output - HighRes PDF as JPG - 1500px")
                        url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
                        print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)

    #5 Preview Download URL
    print ("#Output - Preview Download URL")
    print ("#Preview Download Link: " + DL_LINK_LOW)
    # *********************************************************************************

def DSF_32455_LT():
    # *********************************************************************************
    # Testcase - Create new FormField
    # *********************************************************************************
    print ("#3-1. Set name for FormField")
    # Test 1: Set name for FormField
    str001 = 'myForm_01'
    str002 = 'myForm_02'
    # *********************************************************************************
    print ("#3-2. Set expected registered name for FormField")
    # Test 1: Set expected registered name for FormField
    NoSpecialStr001 = "[[myForm_01]]"
    NoSpecialStr002 = "[[myForm_02]]"
    # *********************************************************************************
    # *********************************************************************************
    print ("#3-3. Set default value for FormField")
    # Test 1: Set default value for FormField
    defVal001 = "0"
    defVal002 = "DSF-32455"
    # *********************************************************************************
    # *********************************************************************************
    #4. Current Window Size
    winSize = driver.get_window_size()
    print ("#4. Current Window Size: " + str(winSize))
    #4-1. Expand Windows Size to be able to recieve all command to add FormFields
    print("#4-1. Expand Windows Size to be able to recieve all command to add FormFields")
    driver.set_window_size(1280,2000)
    # *********************************************************************************
    # *********************************************************************************
    #5. Add FormField
    print("#5. Add New FormField")
    for i in range(2):
        driver.find_element_by_css_selector("div.scContextBlockContent > div.scBtn").click()
        time.sleep(1)
        assTxt = "[[FormField0]]"
        #print (assTxt)
        InputNr = "(//input[@type='text'])[" + str(i*2+38) + "]"
        InputNr2 = "(//input[@type='text'])[" + str(i*2+39) + "]"
        strNr = eval("str"+"{0:03d}".format(i+1))
        strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
        strNr3 = eval("defVal"+"{0:03d}".format(i+1))
        #print (InputNr)
        #print (strNr)
        if str(i) == "0":
            #Xpath = "//label_ctrl/div/div"
            css_path="div.scTitle"
            assert assTxt in driver.find_element_by_css_selector(css_path).text
            #print (Xpath)
            #assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_css_selector(css_path).click()
            time.sleep(1)
            #driver.find_element_by_xpath(Xpath).click()
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            #assert strNr2 in driver.find_element_by_xpath(Xpath).text
            assert strNr2 in driver.find_element_by_css_selector(css_path).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr3)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-" + "{0:03d}".format(i+1) + " - Add New FormField")
        else:
            Xpath = "//div[" + str(i*2+1) + "]/div/label_ctrl/div/div"
            #print (Xpath)
            assert assTxt in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(Xpath).click()
            time.sleep(1)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr).send_keys(strNr)
            driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
            assert strNr2 in driver.find_element_by_xpath(Xpath).text
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
            driver.find_element_by_xpath(InputNr2).send_keys(strNr3)
            driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
            print("#5-001-" + "{0:03d}".format(i+1) + " - Add New FormField")
    time.sleep(1)
    # *********************************************************************************
    # *********************************************************************************
    #6. Testcase - Create new Switch
    print ("#6. Testcase - Create new Switch")
    # *********************************************************************************
    #6-1. Click "Add Switch" button
    print ("#6-1. Click 'Add Switch' button")
    # ********************************
    driver.find_element_by_xpath("//div[3]/div[2]/div[2]").click()
    # ********************************
    #6-2. Click "Edit Switch" button of the first one
    # ********************************
    print ("#6-2. Click 'Edit Switch' button of the first one")
    driver.find_element_by_xpath("//div[3]/div[2]/div/div/div[2]/div/i").click()
    # ********************************
    #6-3. Create Switch Condition
    print ("#6-3. Create Switch Condition")
    # ********************************
    elem = "//div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).click()
    time.sleep(1)
    # ********************************
    #6-4. Set Target field
    print ("#6-4. Set Target field")
    # ********************************
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath(elem).send_keys("[[myForm_01]]")
    driver.find_element_by_xpath(elem).send_keys(Keys.RETURN)
    time.sleep(1)
    # ********************************
    #6-5. Change opertor of the condition
    print ("#6-5. Change opertor of the condition")
    # ********************************
    elem = "//div[1]/main_view/div/div[32]/div[2]/div/div/div/div/div/div/div/div/div[1]/div/div/text_ctrl/div/div[2]/div/div[3]/i"
    driver.find_element_by_xpath(elem).click()
    elem_eq = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li"
    elem_neq = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[2]"
    elem_contain = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[3]"
    elem_gt = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[4]"
    elem_lt = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[5]"
    elem_strw = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[6]"
    elem_endw = "//div/div/div/div/div/div/div/div/div/div/div/text_ctrl/div/div[2]/div/div[4]/div/ul/li[7]"
    driver.find_element_by_xpath(elem_lt).click()
    # ********************************
    #6-7. Set Value field
    print ("#6-7. Set Value field")
    # ********************************
    elem = "//div/div/div/div/div/div/div/div/div[2]/div/div/div"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).click()
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath(elem).send_keys("10")
    driver.find_element_by_xpath(elem).send_keys(Keys.RETURN)
    time.sleep(1)
    # ********************************
    #6-8. Close Switch Designer dialog
    print ("#6-8. Close Switch Designer dialog")
    # ********************************
    elem = "//div[32]/div[3]/div"
    driver.find_element_by_xpath(elem).click()
    # ********************************
    #6-9. Set name for Switch
    print ("#6-9. Set name for Switch")
    # ********************************
    elem = "//div[3]/div[2]/div/div/div/label_ctrl/div/div"
    driver.find_element_by_xpath(elem).click()
    elem = "(//input[@type='text'])[42]"
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath(elem).send_keys("LT")
    driver.find_element_by_xpath(elem).send_keys(Keys.RETURN)
    time.sleep(1)
    # *********************************************************************************
    # *********************************************************************************
    #7. Reset Windows Size to default
    print ("#7. Reset Windows Size to default")
    driver.set_window_size(1040,784)
    # *********************************************************************************
    # *********************************************************************************
    # Testcase - Create new Variable
    # *********************************************************************************
    # *********************************************************************************
    #8. Testcase - Create new Layer
    print ("#8. Testcase - Create new Layer")
    # *********************************************************************************
    #8-1. Create New Layer - LT
    print ("#8-1. Create New Layer - LT")
    # ********************************
    elem_css = "div.scLayersPanelToolbar > div.scIconBtn > i.icon-DS_Add"
    driver.find_element_by_css_selector(elem_css).click()
    # ********************************
    #8-2. Select created Layer
    print ("#8-2. Select created Layer")
    # ********************************
    elem_css = "div.scLayerNameLabel"
    driver.find_element_by_css_selector(elem_css).click()
    # ********************************
    ##8-3. Double Click new layer for renaming
    #print ("#8-3. Double Click new layer for renaming")
    # ********************************
    #actionchains = ActionChains(driver)
    #element = driver.find_element_by_css_selector(elem_css)
    #actionchains.double_click(element).perform()
    # ********************************
    ##8-4. Name Layer
    #print ("#8-4. Name Layer")
    # ********************************
    # ********************************
    #8-5. Select Condition for the Layer
    print ("#8-5. Select Condition for the Layer")
    # ********************************
    elem_css = "span.scSwitchName"
    driver.find_element_by_css_selector(elem_css).click()
    # ********************************
    #8-6. Set possible Xpath
    print ("#8-6. Set possible Xpath")
    # ********************************
    elem_alwaysOn = "//div[42]/div/div/div/div/div"
    elem_alwaysHide = "//div[42]/div/div/div[2]/div/div"
    elem_selection = "//div[42]/div/div/div[3]/div/small"
    elem_LT = "//div[42]/div/div/div[4]/div/div"
    # ********************************
    #8-9. Assign Switch to layer
    print ("#8-9. Assign Switch to layer")
    # ********************************
    driver.find_element_by_xpath(elem_LT).click()
    #elem = ""
    #driver.find_element_by_css_selector(elem).click()
    #driver.find_element_by_xpath(elem).click()
    time.sleep(1)
    # *********************************************************************************
    # *********************************************************************************
    #9. Add items
    print ("#9. Add items")
    # ********************************
    #9-1. Add Text frame, and Edit
    print ("#9-1. Add Text frame, and Edit")
    # ********************************
    driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
    driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
    if TestOnSauceLabs == True:
        print ("#9-2-1. Wait dynamically about appearance of next item due to execution on SauceLabs")
        elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
        wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
        driver.find_element_by_css_selector(elem_css).click()
    else:
        print ("#9-2-1. Skip dynamically waiting about appearance of next item due to local execution")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys("DevOps DSF-32455!")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    # ********************************
    #9-2-2. Change font size
    print ("#9-2-2. Change font size")
    # ********************************
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
    time.sleep(1)
    # ********************************
    #9-3. Ensure font has been changed
    # ********************************
    print ("#9-3. Ensure font has been changed")
    driver.find_element_by_xpath("//div").click()
    time.sleep(2)
    # *********************************************************************************
    # *********************************************************************************
    #10. Work on Buyer Mode - View
    print ("#10. Work on Buyer Mode - View")
    # ********************************
    #10-1. Open View menu
    print ("#10-1. Open View menu")
    # ********************************
    driver.find_element_by_xpath("//menu_panel/div/ul/li[4]/span").click()
    time.sleep(1)
    # ********************************
    #10-2. Click 'Buyer mode view'
    print ("#10-2. Click 'Buyer mode view'")
    # ********************************
    driver.find_element_by_xpath("//li[4]/ul/li[6]").click()
    time.sleep(1)
    # ****************************************************************
    #10-3. Type value - Test 01
    print ("#10-3. Type value - Test 01")
    # ****************************************************************
    # Use below for Ver7.2.9.x
    #elem = "(//input[@type='text'])[178]"
    # Use below for > Ver7.3.0.74
    #elem = "(//input[@type='text'])[181]"
    elem = "//div[41]/div/div[2]/div/div[2]/div/text_ctrl/div/div[2]/div/div[2]/input"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys("dwsdssdsdsdsssdsd")
    driver.find_element_by_xpath(elem).send_keys(Keys.ENTER)
    # ********************************
    #10-4. Switch Condition Evaluation - 01
    print ("#10-4. Switch Condition Evaluation - 01")
    # ********************************
    # ********************************
    #10-5. Assert text frame appearance on SmartCanvas UI - 01
    print ("#10-5. Assert text frame appearance on SmartCanvas UI - 01")
    # ********************************
    element = driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/div[1]/div/div/div/div/div/div")
    element.get_attribute("style")
    assert 'display: block;' in element.get_attribute("style")
    time.sleep(2)
    # ****************************************************************
    #10-6. Type value - Test 02
    print ("#10-6. Type value - Test 02")
    # ****************************************************************
    # Use below for Ver7.2.9.x
    #elem = "(//input[@type='text'])[178]"
    # Use below for > Ver7.3.0.74
    #elem = "(//input[@type='text'])[181]"
    elem = "//div[41]/div/div[2]/div/div[2]/div/text_ctrl/div/div[2]/div/div[2]/input"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys("10")
    driver.find_element_by_xpath(elem).send_keys(Keys.ENTER)
    # ********************************
    #10-7. Switch Condition Evaluation - 02
    print ("#10-7. Switch Condition Evaluation - 02")
    # ********************************
    # ********************************
    #10-8. Assert text frame appearance on SmartCanvas UI - 02
    print ("#10-8. Assert text frame appearance on SmartCanvas UI - 02")
    # ********************************
    element = driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/div[1]/div/div/div/div/div/div")
    element.get_attribute("style")
    assert 'display: none;' in element.get_attribute("style")
    time.sleep(2)
    # ****************************************************************
    #10-9. Type value - Test 03
    print ("#10-9. Type value - Test 03")
    # ****************************************************************
    # Use below for Ver7.2.9.x
    #elem = "(//input[@type='text'])[178]"
    # Use below for > Ver7.3.0.74
    #elem = "(//input[@type='text'])[181]"
    elem = "//div[41]/div/div[2]/div/div[2]/div/text_ctrl/div/div[2]/div/div[2]/input"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys("11")
    driver.find_element_by_xpath(elem).send_keys(Keys.ENTER)
    time.sleep(1)
    # ********************************
    #10-10. Switch Condition Evaluation - 03
    print ("#10-10. Switch Condition Evaluation - 03")
    # ********************************
    # ********************************
    #10-11. Assert text frame appearance on SmartCanvas UI - 03
    print ("#10-11. Assert text frame appearance on SmartCanvas UI - 03")
    # ********************************
    element = driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/div[1]/div/div/div/div/div/div")
    element.get_attribute("style")
    assert 'display: none;' in element.get_attribute("style")
    time.sleep(2)
    # ****************************************************************
    # ****************************************************************
    #10-12. Type value - Test 04
    print ("#10-12. Type value - Test 04")
    # ****************************************************************
    # Use below for Ver7.2.9.x
    #elem = "(//input[@type='text'])[178]"
    # Use below for > Ver7.3.0.74
    #elem = "(//input[@type='text'])[181]"
    elem = "//div[41]/div/div[2]/div/div[2]/div/text_ctrl/div/div[2]/div/div[2]/input"
    driver.find_element_by_xpath(elem).click()
    driver.find_element_by_xpath(elem).send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath(elem).send_keys("9")
    driver.find_element_by_xpath(elem).send_keys(Keys.ENTER)
    # ********************************
    #10-13. Switch Condition Evaluation - 04
    print ("#10-13. Switch Condition Evaluation - 04")
    # ********************************
    # ********************************
    #10-14. Assert text frame appearance on SmartCanvas UI - 04
    print ("#10-14. Assert text frame appearance on SmartCanvas UI - 04")
    # ********************************
    element = driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/div[1]/div/div/div/div/div/div")
    element.get_attribute("style")
    assert 'display: block;' in element.get_attribute("style")
    time.sleep(2)
    # ********************************
    #10-12. Buyer mode - Save Document
    print ("#10-12. Buyer mode - Save Document")
    # ********************************
    elem = "i.icon-DS_Save"
    driver.find_element_by_css_selector(elem).click()
    time.sleep(3)
    # ********************************
    #10-13. Buyer mode - Get Preview PDF
    print ("#10-13. Buyer mode - Get Preview PDF")
    # ********************************
    elem = "i.icon-DS_PDF"
    driver.find_element_by_css_selector(elem).click()
    # ********************************
    #10-14. Wait PDF rendering until 'Download PDF' link appear
    print ("#10-14. Wait PDF rendering until 'Download PDF' link appear")
    # ********************************
    wait = WebDriverWait(driver, 60)
    wait.until(EC.visibility_of_element_located((By.LINK_TEXT, "Download PDF")))
    # ********************************
    #10-15. Get part of link to be used in post action from "Download PDF" button
    print ("#10-15. Get part of link to be used in post action from 'Download PDF' button")
    # ********************************
    DL_LINK_LOW = driver.find_element_by_link_text("Download PDF").get_attribute("href")
    p_LOW = re.compile("(?<=pdf=)(.+)")
    m_LOW = p_LOW.search(DL_LINK_LOW)
    myPDFinfo_LOW = m_LOW.group(0)
    # ********************************
    #10-16. Get part of DSMO url
    print ("#10-16. Get part of DSMO url")
    # ********************************
    p2_LOW = re.compile("^http(.+)/")
    m2_LOW = p2_LOW.search(DL_LINK_LOW)
    myDSMOURL_LOW = m2_LOW.group(0)
    # ********************************
    #10-17. Click OK button to close dialog
    print ("#10-17. Click OK button to close dialog")
    # ********************************
    driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
    time.sleep(1)
    # ****************************************************************
    #10-18. Close Buyer mode
    print ("#10-18. Close Buyer mode")
    driver.find_element_by_css_selector("div.button > i.icon-DS_Close").click()
    time.sleep(2)
    # *********************************************************************************
    # *********************************************************************************
    #11. Clean-Up Operation
    # *********************************************************************************
    #11-1. Clean Up - All Content on Canvas
    print ("#7-1. Clean Up - All Content on Canvas")
    driver.find_element_by_css_selector(".scComposition").click()
    driver.find_element_by_css_selector(".scComposition").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_css_selector(".scComposition").send_keys(Keys.DELETE)
    time.sleep(2)
    # ****************************************************************
    #11-2. Clean Up - FormField
    print ("#11-2. Clean Up - FormField")
    # ****************************************************************
    for i in range(2):
        driver.find_element_by_css_selector("div.scCol-1 > div.scIconBtn > i.icon-DS_TrashCan").click()
        time.sleep(1)
    # ****************************************************************
    #11-3. Clean Up - Switch
    print ("#11-3. Clean Up - Switch")
    # ****************************************************************
    elem = "//div[@id='scContainerCtrl']/main_view/div/context_panel/div/div[4]/div/div[3]/div[2]/div/div/div[3]/div/i"
    driver.find_element_by_xpath(elem).click()
    # ****************************************************************
    #11-4. Clean Up - Layer
    print ("#11-4. Clean Up - Layer")
    # ****************************************************************
    elem_css = "div.scLayerNameLabel"
    driver.find_element_by_css_selector(elem_css).click()
    time.sleep(1)
    elem_css = "div.scLayersPanelToolbar > div.scIconBtn > i.icon-DS_TrashCan"
    driver.find_element_by_css_selector(elem_css).click()
    time.sleep(1)
    # ****************************************************************
    # Click OK button to proceed deletion (due to remains content inside of the target layer)
    # Comment out here as we delete all item in above
    #elem_css = "div.scModalFooter > div.scBtn"
    #driver.find_element_by_css_selector(elem_css).click()
    #time.sleep(1)
    # *********************************************************************************
    # *********************************************************************************
    #12. Preview PDF: Open DSMO GetImage interface converted PDF page as png and jpg 
    print ("#12. Preview PDF: Open DSMO GetImage interface converted PDF page to PNG and JPG")
    # ************************************
    # Loop as page number amount - optional for multi page PDF support
    for i in range(1):
    # ************************************
        # Loop for Low & High Resolution PDF Output verification
        for j in range(1):
            # Link Generation - Low Resolution
            if str(j) == "0":
                #Loop boolean as PNG and JPG format link generation
                for h in range(2):
                    if str(h) == "0":
                        #0. Set general variable with leading 0
                        url = "url_" + "{0:03d}".format(i+j+h+1)
                        #1. LowRes PDF as - PNG
                        print ("#Output - LowRes PDF as - PNG - 600px")
                        url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
                        print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
                    else:
                        #0. Set general variable with leading 0
                        url = "url_" + "{0:03d}".format(i+j+h+1)
                        #2. LowRes PDF as - JPG
                        print ("#Output - LowRes PDF as - JPG - 600px")
                        url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
                        print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
            # Link Generation - High Resolution
            else:
                #Loop boolean as PNG and JPG format link generation
                for h in range(2):
                    if str(h) == "0":
                        #0. Set general variable with leading 0
                        url = "url_" + "{0:03d}".format(i+j+h+1)
                        #3. HighRes PDF as PNG
                        print ("#Output - HighRes PDF as PNG - 1500px")
                        url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
                        print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
                    else:
                        #0. Set general variable with leading 0
                        url = "url_" + "{0:03d}".format(i+j+h+1)
                        #4. HighRes PDF as JPG
                        print ("#Output - HighRes PDF as JPG - 1500px")
                        url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
                        print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)

    #5 Preview Download URL
    print ("#Output - Preview Download URL")
    print ("#Preview Download Link: " + DL_LINK_LOW)
    # *********************************************************************************


def RichTextFormat_OptionAppearanceCheck():
    # **********************************************************************************************
    #3. Add items
    print ("#3. Add items")
    #3-1. Add Text frame, and Edit
    print ("#3-1. Add Text frame, and Edit")
    driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
    driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
    if TestOnSauceLabs == True:
        print ("#3-2-1. Wait dynamically about appearance of next item due to execution on SauceLabs")
        elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
        WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
        driver.find_element_by_css_selector(elem_css).click()
    else:
        print ("#3-2-1. Skip dynamically waiting about appearance of next item due to local execution")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys("Hello [[FirstName]]!")
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    #3-2. Change font size
    print ("#3-2. Change font size")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
    driver.find_element_by_xpath("//div").click()
    time.sleep(1)
    # **********************************************************************************************
    # Select top item on the layer
    driver.find_element_by_css_selector("span.scLayerItemName").click()
    time.sleep(1)
    # Disable Rich-Text format option
    driver.find_element_by_xpath("//checkbox_ctrl/div/div/div").click()
    time.sleep(1)
    # Open View menu
    driver.find_element_by_xpath("//menu_panel/div/ul/li[4]/span").click()
    time.sleep(1)
    # Click 'Buyer mode view'
    driver.find_element_by_xpath("//li[4]/ul/li[6]").click()
    time.sleep(1)
    elem_path = "//div/div/div/div/div/div/div/div[2]/div/div/p"
    WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.XPATH, elem_path)))
    # Select text frame
    driver.find_element_by_xpath(elem_path).click()
    time.sleep(1)
    # Select Edit mode option
    driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]").click()
    # **********************************************************************************************
    print ("Assert display value of scTextToolbar")
    element = driver.find_element_by_id("scTextToolbar")
    element.get_attribute("style")
    assert 'display: none;' in element.get_attribute("style")
    # **********************************************************************************************
    print ("close Buyer mode")
    driver.find_element_by_css_selector("div.button > i.icon-DS_Close").click()
    time.sleep(2)
    # **********************************************************************************************
    # Select top item on the layer
    driver.find_element_by_css_selector("span.scLayerItemName").click()
    time.sleep(1)
    # Enable Rich-Text format option
    driver.find_element_by_xpath("//checkbox_ctrl/div/div/div").click()
    time.sleep(1)
    # Open View menu
    driver.find_element_by_xpath("//menu_panel/div/ul/li[4]/span").click()
    time.sleep(1)
    # Click 'Buyer mode view'
    driver.find_element_by_xpath("//li[4]/ul/li[6]").click()
    time.sleep(1)
    elem_path = "//div/div/div/div/div/div/div/div[2]/div/div/p"
    WebDriverWait(driver, 60).until(EC.visibility_of_element_located((By.XPATH, elem_path)))
    # Select text frame
    driver.find_element_by_xpath(elem_path).click()
    time.sleep(1)
    # Select Edit mode option
    driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]").click()
    # **********************************************************************************************
    print ("Assert display value of scTextToolbar")
    element = driver.find_element_by_id("scTextToolbar")
    element.get_attribute("style")
    assert 'display: block;' in element.get_attribute("style")
    time.sleep(2)
    # **********************************************************************************************
    print ("close Buyer mode")
    driver.find_element_by_css_selector("div.button > i.icon-DS_Close").click()
    time.sleep(2)
    # **********************************************************************************************

def ResizeDocument(DocumentFormantListNumber):
    # **********************************************************************************************
    #1. Document settings
    print ("#4. Document settings")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li[1]/ul/li[4]/span").click()
    time.sleep(1)
    #2. Select Document Format
    #2-1. Open dropdown menu
    driver.find_element_by_xpath("/html/body/div[1]/main_view/div/div[34]/div[2]/div/div[1]/div[1]/text_ctrl/div/div[2]/div/div[3]/i").click()
    #2-2. Select Format
    # List ---- 1 = Letter
    # List ---- 2 = Tabloid
    # List ---- 3 = Legal
    # List ---- 4 = Statement
    # List ---- 5 = Executive
    # List ---- 6 = A0
    # List ---- 7 = A1
    # List ---- 8 = A2
    # List ---- 9 = A3
    # List ---- 10 = A4
    # List ---- 11 = A5
    # List ---- 12 = A6 (Default)
    # List ---- 13 = B4
    # List ---- 14 = B5
    driver.find_element_by_xpath("/html/body/div[1]/main_view/div/div[34]/div[2]/div/div[1]/div[1]/text_ctrl/div/div[2]/div/div[4]/div/ul/li["+str(DocumentFormantListNumber)+"]").click()
    # Close dialog by clicking OK button
    driver.find_element_by_xpath("//div[34]/div[3]/div").click()
    # *********************************************************************************

def changeDocumentDirection(direction):
    # **********************************************************************************************
    #1. Document settings
    print ("#1. Document settings")
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li/span").click()
    driver.find_element_by_xpath("//div/main_view/div/menu_panel/div/ul/li[1]/ul/li[4]/span").click()
    time.sleep(1)
    #2. Set xpath based on provided arg value
    if direction == "Portrait":
        # Portrait
        print ("#2. Set Portrait as page direction")
        driver.find_element_by_xpath("/html/body/div/main_view/div/div[34]/div[2]/div/div[3]/div[2]/checkbox_ctrl/div/div").click()
    elif direction == "Landscape":
        # Landscape
        print ("#2. Set Landscape as page direction")
        driver.find_element_by_xpath("/html/body/div/main_view/div/div[34]/div[2]/div/div[3]/div[2]/checkbox_ctrl/div/div/div").click()
    else:
        # Fall back selection
        print ("#2. Provided argument value is neither 'Portrait' nor 'Landscape', so we set Landscape as fallback default")
        driver.find_element_by_xpath("/html/body/div/main_view/div/div[34]/div[2]/div/div[3]/div[2]/checkbox_ctrl/div/div/div").click()
    #Close dialog by clicking OK button
    driver.find_element_by_xpath("//div[34]/div[3]/div").click()
    # **********************************************************************************************
    
def RichTextFormatOptionCheckboxIssue_Evaluation():
    # **********************************************************************************************
    for i in range(3):
        #3-2-1. Select top item on the layer
        driver.find_element_by_css_selector("span.scLayerItemName").click()
        time.sleep(1)
        #3-2-2. Disable Rich-Text format option
        driver.find_element_by_xpath("//checkbox_ctrl/div/div/div").click()
        time.sleep(1)
        #3-2-3. Open View menu
        driver.find_element_by_xpath("//menu_panel/div/ul/li[4]/span").click()
        time.sleep(1)
        #3-2-4. Click 'Buyer mode view'
        driver.find_element_by_xpath("//li[4]/ul/li[6]").click()
        time.sleep(1)
        #3-2-5. Select text frame
        driver.find_element_by_xpath("//div/div/div/div/div/div/div/div[2]/div/div/p").click()
        time.sleep(1)
        #3-2-6. Select Edit mode option
        driver.find_element_by_xpath("//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]").click()
        time.sleep(2)
        if i == 0 or i == 2:
            # **********************************************************************************************
            print ("#3-2-7. Assert display value of scTextToolbar - Assert display: none;")
            element = driver.find_element_by_id("scTextToolbar")
            element.get_attribute("style")
            assert 'display: none;' in element.get_attribute("style")
            # **********************************************************************************************
        else:
            # **********************************************************************************************
            print ("#3-2-7. Assert display value of scTextToolbar - Assert display: block;")
            element = driver.find_element_by_id("scTextToolbar")
            element.get_attribute("style")
            assert 'display: block;' in element.get_attribute("style")
            time.sleep(2)
            # **********************************************************************************************
        print ("#3-2-8. close Buyer mode")
        driver.find_element_by_css_selector("div.button > i.icon-DS_Close").click()
        time.sleep(2)
    # **********************************************************************************************

def clickBleedArea():
    # **********************************************************************************************
    css_element = "div.scPageBleedArea"
    driver.find_element_by_css_selector(css_element).click()
    time.sleep(1)
    # **********************************************************************************************

def DIRSMILE_567_AddImg_UploadFile_PlaceImg_JPG():
    # **********************************************************************************************
    #3. Add items
    print ("#3. Add items")
    testFile001="C:\\temp\\SmartCanvas_ImageUpload\\Mr # DevOps.jpg"
    #3-1. Add Image
    print ("#3-1. Add Image")
    driver.find_element_by_css_selector("i.icon-DS_Image").click()
    #3-2. Upload Image file
    print ("#3-2. Upload Image file")
    # ---- ---- Upload Image file
    elem_path = "//input[@name='myImageUploadButton']"
    print ("# ---- ---- Upload Image file")
    driver.file_detector.is_local_file()
    target = driver.find_element_by_xpath(elem_path)
    target.send_keys(testFile001)
    time.sleep(1)
    # ---- ---- Wait Uploading - up to 60 sec
    print("# ---- ---- Wait Uploading - up to 60 sec")
    elem_path = "//div[2]/div/div/img"
    WebDriverWait(driver,60).until(EC.visibility_of_element_located((By.XPATH, elem_path)))
    #3-3. Select uploaded Image file
    print ("#3-3. Select uploaded Image file")
    # ---- ---- ---- Select image file
    elem_path = "//div[2]/div/img"
    #elem_path = "//div[1]/main_view/div/div[26]/div[2]/div[1]/div[2]/div[2]/div[2]"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # **********************************************************************************************

def DIRSMILE_567_AddImg_UploadFile_PlaceImg_PDF():
    # **********************************************************************************************
    #3. Add items
    print ("#3. Add items")
    testFile001="C:\\temp\\SmartCanvas_ImageUpload\\Barn_#10env_Common.pdf" 
    #3-1. Add Image
    print ("#3-1. Add Image")
    driver.find_element_by_css_selector("i.icon-DS_Image").click()
    #3-2. Upload Image file
    print ("#3-2. Upload Image file")
    # ---- ---- Upload Image file
    elem_path = "//input[@name='myImageUploadButton']"
    print ("# ---- ---- Upload Image file")
    driver.file_detector.is_local_file()
    target = driver.find_element_by_xpath(elem_path)
    target.send_keys(testFile001)
    time.sleep(1)
    # ---- ---- Wait Uploading - up to 60 sec
    print("# ---- ---- Wait Uploading - up to 60 sec")
    elem_path = "//div[2]/div/div/img"
    WebDriverWait(driver,60).until(EC.visibility_of_element_located((By.XPATH, elem_path)))
    #3-3. Select uploaded Image file
    print ("#3-3. Select uploaded Image file")
    # ---- ---- ---- Select image file
    elem_path = "//div[2]/div/img"
    #elem_path = "//div[1]/main_view/div/div[26]/div[2]/div[1]/div[2]/div[2]/div[2]"
    target = driver.find_element_by_xpath(elem_path)
    target.click()
    time.sleep(1)
    # **********************************************************************************************