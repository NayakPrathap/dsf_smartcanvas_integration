# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys
import SCKit, SCKitRegression

allArgs = sys.argv
#allArgs = ["myScript", "firefox dev", "https://devops-az01.directsmile.com/webapi/smartcanvas/opensmartcanvas.html#{%22dsmx%22:{%22targetProtocol%22:%22https%22,%22targetSystem%22:%22devops-az01.directsmile.com%22,%22publicKey%22:%22E62E4FDD%22,%22privateKey%22:%22B87AC6C0B62B5322%22},%22documentId%22:2109,%22useCampaignStorage%22:true,%22showRulers%22:true,%22currentUserName%22:%22Admin%22}", "7.3.1.117"]

# **********************************************************************************************
# THIS IS REGRESSION TEST ABOUT SmartCanvas 7.2.4.194 - Fixed issue confirmation
# Please refer to below link for further information 
print ("THIS IS REGRESSION TEST ABOUT SmartCanvas 7.2.4.194 - toolbar Shown Only Styles - on Ver7.3")
# **********************************************************************************************
driver = SCKitRegression.magicArgs(allArgs)
SCKitRegression.ResizeDocument(10)
SCKitRegression.changeDocumentDirection("Landscape")
#SCKitRegression.getWindowSize()
#SCKitRegression.changeWindowSize(1280,2000)
SCKitRegression.toolbarShownOnlyStyles_AddFont()
SCKitRegression.addItems_TEXT()
SCKitRegression.editItems_TEXT()
SCKitRegression.clickBleedArea()
SCKitRegression.toolbarShownOnlyStyles_AddItems()
SCKitRegression.clickBleedArea()
SCKitRegression.toolbarShownOnlyStyles_AssertTest()
SCKitRegression.saveCanvas()
SCKitRegression.outputPDF_LOW()
SCKitRegression.outputPDF_HIGH()
SCKitRegression.toolbarShownOnlyStyles_CleanupConfiguration()
SCKitRegression.toolbarShownOnlyStyles_CleanupStyle()
SCKitRegression.toolbarShownOnlyStyles_CleanupFont()
SCKitRegression.cleanupCanvas()
SCKitRegression.printPDF(1)
SCKitRegression.quitDriver()
