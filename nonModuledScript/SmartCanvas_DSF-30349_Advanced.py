# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys
# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
# Filename: sysArgvTest.py
# Function: Test to retrieve system argument value
# Behavior: Based on provided argument, it run test locally or over SauceLabs
#           If some arguments are missing, then we stop to run test
# Usage: Jenkins execution with parameter value
# Author: Nobuaki Ogawa - nobuaki.ogawa@efi.com
# Arguments:
#   [1] Browser Name (SauceLabs - browserName) --- Input Example <firefox|chorme|firefox dev|firefox dev 64?>
#   [2] Target Test URL
#   [3] SmartCanvas - Version (7.2.x.x|7.3.x.x|Trunk(7.9.x.x))
#   [4] SauceLabs URL
#   [5] SauceLabs - platform
#   [6] SauceLabs - version
#   [7] SauceLabs - name
# Tips: If you run test over SauceLabs, please ensure all above arguments were provided while test. 
# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
# **********************************************************
# Create several variable based on system arguments as well as fallback value
# **********************************************************
allArgs = sys.argv
countArgs = len(allArgs)
print("001. Provided Arguments Counts: " + str(countArgs))
# Set FireFox exe path
FIREFOX_STD_EXE_PATH = "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"
FIREFOX_DEV_EXE_PATH  = "C:\\Program Files (x86)\\Mozilla Developer Preview\\firefox.exe"
FIREFOX_STD_EXE_PATH_X64 = "C:\\Program Files\\Mozilla Firefox\\firefox.exe"
FIREFOX_DEV_EXE_PATH_X64 = "C:\\Program Files\\Mozilla Developer Preview\\firefox.exe"
default_URL = "https://devops-az01.directsmile.com/smartcanvas/index.html"
default_SCVersion = "7.3.0.85"
TestOnSauceLabs = False
# **********************************************************
# Check whether any parameter was provided or not
# **********************************************************
if countArgs > 7:
    print("002. TestURL & SauceLabsURL were provide within script call as system arguments")
    print("003. Run script via SauceLabs")
    TestOnSauceLabs = True
    Browser = sys.argv[1:]
    TestURL = sys.argv[2:]
    SmartCanvas_Version = sys.argv[3:]
    SauceLabsURL = sys.argv[4:]
    SauceLabs_platform = sys.argv[5:]
    SauceLabs_version = sys.argv[6:]
    SauceLabs_name = sys.argv[7:]
    myUrl = TestURL[0]
    myBrowser = Browser[0]
    myVersion = SmartCanvas_Version[0]
    if myBrowser == "firefox dev":
        SauceLabs_browserName = "firefox"
        print ("Change browser to FireFox -- 01")
    elif myBrowser == "firefox dev 64":
        SauceLabs_browserName = "firefox"
        print ("Change browser to FireFox -- 02")
    elif myBrowser == "firefox 64":
        SauceLabs_browserName = "firefox"
        print ("Change browser to FireFox -- 03")
    else:
        SauceLabs_browserName = myBrowser
        print ("Change browser to specified one -- 04")
    # **********************************************************************************************
    # This is the only code you need to edit in your existing scripts
    # The command_executor tells the test to run on Sauce, while the desired_capabilities
    # parameter tells us which browsers and OS to spin up.
    # **********************************************************************************************
    desired_cap = {
        'platform': SauceLabs_platform[0],
        'browserName': SauceLabs_browserName,
        'version': SauceLabs_version[0],
        'name': SauceLabs_name[0]+ " - " + myVersion,
    }
    driver = webdriver.Remote(
        command_executor = SauceLabsURL[0],
        desired_capabilities = desired_cap
    )
elif countArgs > 4 and countArgs < 8:
    print("002. Browser & TestURL & SauceLabsURL were provide within script call as system arguments")
    print("003. Some SauceLabs relates arguments are missing, so we stop test now")
    quit()
elif countArgs > 3:
    print("002. Browser & TestURL, and SmartCanvas Version were provided within script call as system arguments")
    print("003. Run script locally")
    Browser = sys.argv[1:]
    myBrowser = Browser[0]
    TestURL = sys.argv[2:]
    myUrl = TestURL[0]
    SmartCanvas_Version = sys.argv[3:]
    myVersion = SmartCanvas_Version[0]
    if myBrowser == "chrome":        
        # **********************************************************
        #004. Set Chrome as default browser
        # **********************************************************
        print ("004. Set Chrome as default browser")
        driver = webdriver.Chrome()
        #006. Start WebDriver
        print ("005. Start WebDriver")
        driver.implicitly_wait(30)
    elif myBrowser == "firefox dev":
        # **********************************************************
        #004. Set Desired Capabilities for local execution (Mozilla Developer Preview)
        # **********************************************************
        print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
        binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
        myBrowser = "firefox"
        caps = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
        #0. Start WebDriver
        print ("005. Start WebDriver")
        driver.implicitly_wait(30)
    elif myBrowser == "firefox dev 64":
        # **********************************************************
        #004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
        # **********************************************************
        print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
        binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
        myBrowser = "firefox"
        caps = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
        #0. Start WebDriver
        print ("005. Start WebDriver")
        driver.implicitly_wait(30)
    elif myBrowser == "firefox 64":
        # **********************************************************
        #004. Set Desired Capabilities for local execution (FireFox - 64bit)
        # **********************************************************
        print ("004. Set Desired Capabilities for local execution (FireFox - 64bit)")
        binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
        myBrowser = "firefox"
        caps = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
        #0. Start WebDriver
        print ("005. Start WebDriver")
        driver.implicitly_wait(30)
    else:
        # **********************************************************
        #004. Set Desired Capabilities for local execution (FireFox)
        # **********************************************************
        print ("004. Set Desired Capabilities for local execution (FireFox)")
        myBrowser = "firefox"
        binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
        caps = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
        #005. Start WebDriver
        print ("005. Start WebDriver")
        driver.implicitly_wait(30)
elif countArgs > 2:
    print("002. Browser was provided, but neither TestURL nor SmartCanvas Version couldn't be found within script call as system arguments")
    print("003. Run script locally")
    Browser = sys.argv[1:]
    myBrowser = Browser[0]
    print("004. Set default target URL")
    # 004 Set default target URL as fallback
    myUrl = default_URL
    # 005. Set target SC Version: 
    myVersion = default_SCVersion
    print ("005. Set target SC Version: " + str(myVersion))
    if myBrowser == "chrome":
        # **********************************************************
        # 006. Set Chrome as default browser
        # **********************************************************
        print ("006. Set Chrome as default browser")
        driver = webdriver.Chrome()
        # 007. Start WebDriver
        print ("007. Start WebDriver")
        driver.implicitly_wait(30)
    elif myBrowser == "firefox dev":
        # **********************************************************
        # 006. Set Desired Capabilities for local execution (Mozilla Developer Preview)
        # **********************************************************
        print ("006. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
        binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
        myBrowser = "firefox"
        caps = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
        # 007. Start WebDriver
        print ("007. Start WebDriver")
        driver.implicitly_wait(30)
    elif myBrowser == "firefox dev 64":
        # **********************************************************
        # 006. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
        # **********************************************************
        print ("006. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
        binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
        myBrowser = "firefox"
        caps = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
        # 007. Start WebDriver
        print ("007. Start WebDriver")
        driver.implicitly_wait(30)
    elif myBrowser == "firefox 64":
        # **********************************************************
        # 006. Set Desired Capabilities for local execution (FireFox - 64bit)
        # **********************************************************
        print ("006. Set Desired Capabilities for local execution (FireFox - 64bit)")
        binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
        myBrowser = "firefox"
        caps = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
        # 007. Start WebDriver
        print ("007. Start WebDriver")
        driver.implicitly_wait(30)
    else:
        # **********************************************************
        # 006. Set Desired Capabilities for local execution (FireFox)
        # **********************************************************
        print ("006. Set Desired Capabilities for local execution (FireFox)")
        myBrowser = "firefox"
        binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
        caps = DesiredCapabilities.FIREFOX
        driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
        # 007. Start WebDriver
        print ("007. Start WebDriver")
        driver.implicitly_wait(30)
else:
    print("002. None of Browser, TestURL, SmartCanvas Version were provided within script call as system arguments")
    print("003. Run script locally")
    # 004 Set default as fallback
    print("004. Set default target URL")
    myUrl = default_URL
    # 005. Set target SC Version: 
    myVersion = default_SCVersion
    print ("005. Set target SC Version: " + str(myVersion))
    # **********************************************************
    # 006. Set Desired Capabilities for local execution
    # **********************************************************
    print ("006. Set Desired Capabilities for local execution")
    myBrowser = "firefox"
    binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
    caps = DesiredCapabilities.FIREFOX
    driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)

# **********************************************************
# Check actual Test URL
# **********************************************************
print("---- Test URL: " + myUrl) 

# **********************************************************
# Check Target SmartCanvas Version
# **********************************************************
print("---- SmartCanvas Version: " + myVersion) 
myVersion_Major = myVersion.split(".")[0]
myVersion_Mainor = myVersion.split(".")[1]
myVersion_Update = myVersion.split(".")[2]
myVersion_Build = myVersion.split(".")[3]
# Set Ver7 >= condition
if int(myVersion_Major) >= 7 and int(myVersion_Mainor) >= 3 and int(myVersion_Update) >= 0 and int(myVersion_Build) >= 85:
    print ("---- Alert may appear")
else:
    print ("---- Alert may not appear while uploading")
# **********************************************************

# *********************************************************************************
# THIS IS REGRESSION TEST ABOUT DSF-30349
# Please refer to below link for further information 
# https://jira.efi.com/browser/DSF-30349
print ("THIS IS REGRESSION TEST ABOUT DSF-30349")
print ("Please refer to below link for further information")
print ("https://jira.efi.com/browser/DSF-30349")
# *********************************************************************************

# **********************************************************
#1. Start WebDriver
print ("#1. Start WebDriver")
driver.implicitly_wait(30)

#2. Browse specify url
print ("#2. Browse specify url")
driver.get(myUrl)
wait = WebDriverWait(driver, 10)
logo = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, "div.scLogo")))
print ("#2-1, I waited dynamically")
time.sleep(3)
# **********************************************************

# *********************************************************************************
print ("#3-1. Store Special Characters Strings")
# Test 1: ASCII Special Characters in UI
str001 = u'"This sentence is wrapped in double quotes."'
str002 = u"'single quotes'"
str003 = u"[square brackets]"
str004 = u"{curly braces}"
str005 = u"<angle brackets>"
str006 = u"<! -- XML comment -->"
str007 = u'"quoted" segment & ampersand'
str008 = u'"A "quoted" segment; & (entity); wrapped in quotes"'
str009 = u"backslashes and colon: \c:\mydocs"
str010 = u"back-quote: Hawai`i"
str011 = u"hash: #20"
str012 = u"escaped slash: \/"
# Test 2: Strings of over 500 characters
str013 = u"START 1) I am typing in more than 500 characters of text to test whether the more than 500 characters are cut off or if they display correctly. 500 characters is a lot of text. 2) I am typing in more than 500 characters of text to test whether the more than 500 characters are cut off or if they display correctly. 500 characters is a lot of text. 3) I am typing in more than 500 characters of text to test whether the more than 500 characters are cut off or if they display correctly. 500 characters is a lot of text. END"
# Test 3: Non-ASCII Characters in UI
str014 = u"ñóǹ äŝçíì 汉语/漢語  华语/華語 Huáyǔ; 中文 Zhōngwén 漢字仮名交じり文 Lech Wałęsa æøå"
# Test 4: ASCII Special Characters and Non-ASCII Characters in fields with autocomplete behavior
str015 = u"Sanford & Sons"
str016 = u"O'Reilly"
str017 = u"Hawai`i"
str018 = u'"Bull" Connor'
str019 = u"Lech Wałęsa"
str020 = u"Herr Müller"
# *********************************************************************************
print ("#3-2. Store Special Characters Strings - Expected post-auto corrected FieldForm value")
# Test 1: ASCII Special Characters in UI
NoSpecialStr001 = "[[Thissentenceiswrappedindoublequotes.]]"
NoSpecialStr002 = "[[singlequotes]]"
NoSpecialStr003 = "[[squarebrackets]]"
NoSpecialStr004 = "[[curlybraces]]"
NoSpecialStr005 = "[[anglebrackets]]"
NoSpecialStr006 = "[[--XMLcomment--]]"
NoSpecialStr007 = "[[quotedsegmentampersand]]"
NoSpecialStr008 = "[[Aquotedsegmententitywrappedinquotes]]"
NoSpecialStr009 = "[[backslashesandcoloncmydocs]]"
NoSpecialStr010 = "[[back-quoteHawaii]]"
NoSpecialStr011 = "[[hash20]]"
NoSpecialStr012 = "[[escapedslash]]"
# Test 2: Strings of over 500 characters
NoSpecialStr013 = "[[START1Iamtypinginmorethan500charactersoftexttotestwhetherthemorethan500charactersarecutofforiftheydisplaycorrectly.500charactersisalotoftext.2Iamtypinginmorethan500charactersoftexttotestwhetherthemorethan500charactersarecutofforiftheydisplaycorrectly.500charactersisalotoftext.3Iamtypinginmorethan500charactersoftexttotestwhetherthemorethan500charactersarecutofforiftheydisplaycorrectly.500charactersisalotoftext.END"
# Test 3: Non-ASCII Characters in UI
NoSpecialStr014 = "[[noaeciiHuayZhngwenLechWaesaaeoa"
# Test 4: ASCII Special Characters and Non-ASCII Characters in fields with autocomplete behavior
NoSpecialStr015 = "[[SanfordSons]]"
NoSpecialStr016 = "[[OReilly]]"
NoSpecialStr017 = "[[Hawaii]]"
NoSpecialStr018 = "[[BullConnor]]"
NoSpecialStr019 = "[[LechWaesa]]"
NoSpecialStr020 = "[[HerrMueller]]"
# *********************************************************************************

# *********************************************************************************
# THIS IS REGRESSION TEST ABOUT DSF-30343
# Force to name FormFields name with special character, then check how it was handled 
# *********************************************************************************
#5. Add Items for test
print("#5-001 - Add New FormField")
for i in range(20):
    driver.find_element_by_css_selector("div.scContextBlockContent > div.scBtn").click()
    assTxt = "[[FormField0]]"
    #print (assTxt)
    InputNr = "(//input[@type='text'])[" + str(i*2+38) + "]"
    InputNr2 = "(//input[@type='text'])[" + str(i*2+39) + "]"
    strNr = eval("str"+"{0:03d}".format(i+1))
    strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
    #print (InputNr)
    #print (strNr)
    if str(i) == "0":
        Xpath = "//label_ctrl/div/div"
        #print (Xpath)
        assert assTxt in driver.find_element_by_xpath(Xpath).text
        driver.find_element_by_xpath(Xpath).click()
        print ("---- Scroll down to show target content in window (Necessary to handle invisible element in window)")
        driver.find_element_by_xpath(InputNr).location_once_scrolled_into_view
        driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath(InputNr).send_keys(strNr)
        driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
        assert strNr2 in driver.find_element_by_xpath(Xpath).text
        driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath(InputNr2).send_keys(strNr)
        driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
        print("#5-001-" + "{0:03d}".format(i+1) + " - Add New FormField")
    else:
        Xpath = "//div[" + str(i*2+1) + "]/div/label_ctrl/div/div"
        #print (Xpath)
        assert assTxt in driver.find_element_by_xpath(Xpath).text
        driver.find_element_by_xpath(Xpath).click()
        print ("---- Scroll down to show target content in window (Necessary to handle invisible element in window)")
        driver.find_element_by_xpath(InputNr).location_once_scrolled_into_view
        driver.find_element_by_xpath(InputNr).send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath(InputNr).send_keys(strNr)
        driver.find_element_by_xpath(InputNr).send_keys(Keys.RETURN)
        assert strNr2 in driver.find_element_by_xpath(Xpath).text
        driver.find_element_by_xpath(InputNr2).send_keys(Keys.CONTROL, "a")
        driver.find_element_by_xpath(InputNr2).send_keys(strNr)
        driver.find_element_by_xpath(InputNr2).send_keys(Keys.RETURN)
        print("#5-001-" + "{0:03d}".format(i+1) + " - Add New FormField")
time.sleep(1)
# *********************************************************************************

# *********************************************************************************
#3. Add items
print ("#3. Add items")
#3-1. Add Text frame, and Edit
print ("#3-1. Add Text frame, and Edit - 001/004")
for i in range(7):
    strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
    driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
    driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
    # *****************************************************************************
    # Condition for SauceLabs execution optional selection operation
    # *****************************************************************************
    if TestOnSauceLabs == True:
        print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
        elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
        wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
        driver.find_element_by_css_selector(elem_css).click()
    else:
        print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
    # *****************************************************************************
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").send_keys(strNr2)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    #3-3. Change font size
    print ("#3-3. Change font size")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
    time.sleep(1)
    #3-4. Change position
    print ("#3-4. Change position - Reset current target")
    driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li").click()
    #3-5. Select Top
    print("#3-5. Select Top")
    driver.find_element_by_css_selector("span.scLayerItemName").click()
    #3-6. Open 'Layout' context tab menu
    print ("#3-6. Open 'Layout' context tab menu")
    driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
    #3-7. Set X position of the element - 'X =< 7 inch'
    #print("#3-7. Set X position of the element - 'X =< 7 inch'")
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
    #3-8. Set Y position of the element - 'Y =< 7 inch'
    print("#3-8. Set Y position of the element - 'Y =< 7 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1))
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
    print ("#3-9. Add Text frame, and Edit - 001/004 - " + "{0:03d}".format(i+1)+ "/020")
    time.sleep(1)
# **********************************************************************************************

# *********************************************************************************
#4. Add Page
print ("#4. Add Page - 02")
driver.find_element_by_css_selector("i.icon-DS_Add").click()
time.sleep(1)
print ("#4-1. Change target page to new one - 02")
targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[2]").click()
targetCanvas = driver.find_element_by_xpath("//div").click()
# *********************************************************************************

# *********************************************************************************
#3. Add items
print ("#3. Add items")
#3-1. Add Text frame, and Edit
print ("#3-1. Add Text frame, and Edit - 002/004")
for i in range(7,12):
    strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
    driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
    driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
    # *****************************************************************************
    # Condition for SauceLabs execution optional selection operation
    # *****************************************************************************
    if TestOnSauceLabs == True:
        print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
        elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
        wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
        driver.find_element_by_css_selector(elem_css).click()
    else:
        print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
    # *****************************************************************************
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").send_keys(strNr2)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    #3-3. Change font size
    print ("#3-3. Change font size")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
    time.sleep(1)
    #3-4. Change position
    print ("#3-4. Change position - Reset current target")
    targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[2]").click()
    targetCanvas = driver.find_element_by_xpath("//div").click()
    time.sleep(1)
    #3-5. Select Top
    print("#3-5. Select Top")
    driver.find_element_by_css_selector("span.scLayerItemName").click()
    #3-6. Open 'Layout' context tab menu
    print ("#3-6. Open 'Layout' context tab menu")
    driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
    #3-7. Set X position of the element - 'X =< 7 inch'
    #print("#3-7. Set X position of the element - 'X =< 7 inch'")
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
    #3-8. Set Y position of the element - 'Y =< 7 inch'
    print("#3-8. Set Y position of the element - 'Y =< 7 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1-7))
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
    print ("#3-9. Add Text frame, and Edit - 002/004 - " + "{0:03d}".format(i+1)+ "/020")
    time.sleep(1)
# **********************************************************************************************

# *********************************************************************************
#4. Add Page
print ("#4. Add Page - 03")
driver.find_element_by_css_selector("i.icon-DS_Add").click()
time.sleep(1)
print ("#4-1. Change target page to new one - 03")
targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[3]").click()
targetCanvas = driver.find_element_by_xpath("//div").click()
# *********************************************************************************

# *********************************************************************************
#3. Add items
print ("#3. Add items")
#3-1. Add Text frame, and Edit
print ("#3-1. Add Text frame, and Edit - 003/004")
for i in range(13,14):
    strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
    driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
    driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
    # *****************************************************************************
    # Condition for SauceLabs execution optional selection operation
    # *****************************************************************************
    if TestOnSauceLabs == True:
        print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
        elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
        wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
        driver.find_element_by_css_selector(elem_css).click()
    else:
        print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
    # *****************************************************************************
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").send_keys(strNr2)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    #3-3. Change font size
    print ("#3-3. Change font size")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
    time.sleep(1)
    #3-4. Change position
    print ("#3-4. Change position - Reset current target")
    targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[3]").click()
    targetCanvas = driver.find_element_by_xpath("//div").click()
    time.sleep(1)
    #3-5. Select Top
    print("#3-5. Select Top")
    driver.find_element_by_css_selector("span.scLayerItemName").click()
    #3-6. Open 'Layout' context tab menu
    print ("#3-6. Open 'Layout' context tab menu")
    driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
    #3-7. Set X position of the element - 'X =< 7 inch'
    #print("#3-7. Set X position of the element - 'X =< 7 inch'")
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
    #3-8. Set Y position of the element - 'Y =< 7 inch'
    print("#3-8. Set Y position of the element - 'Y =< 7 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1-13))
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
    print ("#3-9. Add Text frame, and Edit - 003/004 - " + "{0:03d}".format(i+1)+ "/020")
    time.sleep(1)
# **********************************************************************************************

# *********************************************************************************
#4. Add Page
print ("#4. Add Page - 04")
driver.find_element_by_css_selector("i.icon-DS_Add").click()
time.sleep(1)
print ("#4-1. Change target page to new one - 04")
targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[4]").click()
targetCanvas = driver.find_element_by_xpath("//div").click()
# *********************************************************************************

# *********************************************************************************
#3. Add items
print ("#3. Add items")
#3-1. Add Text frame, and Edit
print ("#3-1. Add Text frame, and Edit - 004/004")
for i in range(14,20):
    strNr2 = eval("NoSpecialStr"+"{0:03d}".format(i+1))
    driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
    driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
    # *****************************************************************************
    # Condition for SauceLabs execution optional selection operation
    # *****************************************************************************
    if TestOnSauceLabs == True:
        print ("#3-2. Wait dynamically about appearance of next item due to execution on SauceLabs")
        elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
        wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
        driver.find_element_by_css_selector(elem_css).click()
    else:
        print ("#3-2. Skip dynamically waiting about appearance of next item due to local execution")
    # *****************************************************************************
    time.sleep(1)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("//div").send_keys(strNr2)
    driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
    time.sleep(1)
    #3-3. Change font size
    print ("#3-3. Change font size")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
    driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
    time.sleep(1)
    #3-4. Change position
    print ("#3-4. Change position - Reset current target")
    targetPage = driver.find_element_by_xpath("//div[2]/div/ul/li/ul/li[4]").click()
    targetCanvas = driver.find_element_by_xpath("//div").click()
    time.sleep(1)
    #3-5. Select Top
    print("#3-5. Select Top")
    driver.find_element_by_css_selector("span.scLayerItemName").click()
    #3-6. Open 'Layout' context tab menu
    print ("#3-6. Open 'Layout' context tab menu")
    driver.find_element_by_css_selector("ul.scContextTabs > li + li + li").click()
    #3-7. Set X position of the element - 'X =< 7 inch'
    #print("#3-7. Set X position of the element - 'X =< 7 inch'")
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(str(i))
    #driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
    #3-8. Set Y position of the element - 'Y =< 7 inch'
    print("#3-8. Set Y position of the element - 'Y =< 7 inch'")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(str(i+1-14))
    driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
    print ("#3-9. Add Text frame, and Edit - 004/004 - " + "{0:03d}".format(i+1)+ "/020")
    time.sleep(1)
# **********************************************************************************************

# **********************************************************************************************
#7. Open File menu and Save it
print ("#7. Open File menu and Save it")
#7-1. Open File menu
print ("#7-1. Open File menu")
driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
#7-2. Save
print ("#7-2. Save")
driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[2]").click()
time.sleep(3)
# **********************************************************************************************

# **********************************************************************************************
#15. Document settings
print ("#15. Document settings")
time.sleep(1)
driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
time.sleep(1)
driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
#15-1. Enable Facing Pages option
print ("#15-1. Enable Facing Pages option")
driver.find_element_by_xpath("//div[6]/div[2]/checkbox_ctrl/div/div/div").click()
#15-2. Close dialog
print ("#15-2. Close dialog")
driver.find_element_by_xpath("//div[@id='eins']/main_view/div/div[34]/div[3]/div").click()
time.sleep(1)
# **********************************************************************************************

# **********************************************************************************************
#5. Output - Default
print ("#5. Output")
#5-1. Open File menu and click "Create preview PDF"
print ("#5-1. Open File menu and click 'Create preview PDF'")
driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[6]").click()
#5-2. Wait PDF rendering until 'Download PDF' link appear
print ("#5-2. Wait PDF rendering until 'Download PDF' link appear")
wait = WebDriverWait(driver, 60)
wait.until(EC.visibility_of_element_located((By.LINK_TEXT, "Download PDF")))
#5-3. Get part of link to be used in post action from "Download PDF" button
print ("#5-3. Get part of link to be used in post action from 'Download PDF' button")
DL_LINK_LOW = driver.find_element_by_link_text("Download PDF").get_attribute("href")
p_LOW = re.compile("(?<=pdf=)(.+)")
m_LOW = p_LOW.search(DL_LINK_LOW)
myPDFinfo_LOW = m_LOW.group(0)
#5-4. Get part of DSMO url
print ("#5-4. Get part of DSMO url")
p2_LOW = re.compile("^http(.+)/")
m2_LOW = p2_LOW.search(DL_LINK_LOW)
myDSMOURL_LOW = m2_LOW.group(0)
#5-5. Click OK button to close dialog
print ("#5-5. Click OK button to close dialog")
driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
#5-6. Open File menu and click "Create #print PDF"
print ("#5-6. Open File menu and click 'Create #print PDF'")
driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[7]").click()
#5-7. Wait PDF rendering until 'Download PDF' link appear
print ("#5-7. Wait PDF rendering until 'Download PDF' link appear")
wait = WebDriverWait(driver, 60)
wait.until(EC.visibility_of_element_located((By.LINK_TEXT, "Download PDF")))
#5-8. Get part of link to be used in post action from "Download PDF" button
print ("#5-8. Get part of link to be used in post action from 'Download PDF' button")
DL_LINK_HIGH = driver.find_element_by_link_text("Download PDF").get_attribute("href")
p_HIGH = re.compile("(?<=pdf=)(.+)")
m_HIGH = p_HIGH.search(DL_LINK_HIGH)
myPDFinfo_HIGH = m_HIGH.group(0)
#5-9. Get part of DSMO url
print ("#5-9. Get part of DSMO url")
p2_HIGH = re.compile("^http(.+)/")
m2_HIGH = p2_HIGH.search(DL_LINK_HIGH)
myDSMOURL_HIGH = m2_HIGH.group(0)
#5-10. Click OK button to close dialog
print ("#5-10. Click OK button to close dialog")
driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
# **********************************************************************************************

# **********************************************************************************************
#17. Clean Up
print ("#17. Clean Up")
#17-1. Clean Up - Page
print ("#17-1. Clean Up - Pages")
for i in range(4):
    elem_path = "//div[2]/div/ul/li/ul/li"
    driver.find_element_by_xpath(elem_path).click()
    driver.find_element_by_css_selector("i.icon-DS_TrashCan").click()
    print ("#17-2. Clean Up - Pages: " + "{0:03d}".format(i+1))
    time.sleep(1)
#17-3. Clean Up - All Content on Canvas
print ("#17-3. Clean Up - All Content on Canvas")
driver.find_element_by_css_selector(".scComposition").click()
driver.find_element_by_css_selector(".scComposition").send_keys(Keys.CONTROL, "a")
driver.find_element_by_css_selector(".scComposition").send_keys(Keys.DELETE)
#17-4. Clean Up - FormField
print ("#17-4. Clean Up - FormField")
for i in range(20):
    driver.find_element_by_css_selector("div.scCol-1 > div.scIconBtn > i.icon-DS_TrashCan").click()
    time.sleep(1)
#17-5. Clean Up - Document settings
print ("#17-5. Clean Up - Document settings")
time.sleep(1)
driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
time.sleep(1)
driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
#17-6. Clean Up - Enable Facing Pages option
print ("#17-6. Clean Up - Enable Facing Pages option")
driver.find_element_by_xpath("//div[6]/div[2]/checkbox_ctrl/div/div/div").click()
time.sleep(1)
#17-7. Clean Up - Close dialog
print ("#17-7. Clean Up - Close dialog")
driver.find_element_by_xpath("//div[@id='eins']/main_view/div/div[34]/div[3]/div").click()
time.sleep(1)
# **********************************************************************************************

# *********************************************************************************
#18. Preview PDF: Open DSMO GetImage interface converted PDF page as png and jpg 
print ("#18. Preview PDF: Open DSMO GetImage interface converted PDF page to PNG and JPG")
# ************************************
# Loop as page number amount - optional for multi page PDF support
# ************************************************************************
for i in range(4):
# ************************************
    # Loop for Low & High Resolution PDF Output verification
    for j in range(2):
        # Link Generation - Low Resolution
        if str(j) == "0":
            #Loop boolean as PNG and JPG format link generation
            for h in range(2):
                if str(h) == "0":
                    #0. Set general variable with leading 0
                    url = "url_" + "{0:03d}".format(i+j+h+1)
                    #1. LowRes PDF as - PNG
                    print ("#18-1. LowRes PDF as - PNG - 600px")
                    url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
                    print ("#18-2. Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
                else:
                    #0. Set general variable with leading 0
                    url = "url_" + "{0:03d}".format(i+j+h+1)
                    #2. LowRes PDF as - JPG
                    print ("#18-3. LowRes PDF as - JPG - 600px")
                    url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
                    print ("#18-4. Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
        # Link Generation - High Resolution
        else:
            #Loop boolean as PNG and JPG format link generation
            for h in range(2):
                if str(h) == "0":
                    #0. Set general variable with leading 0
                    url = "url_" + "{0:03d}".format(i+j+h+1)
                    #3. HighRes PDF as PNG
                    print ("#18-5. HighRes PDF as PNG - 1500px")
                    url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
                    print ("#18-6. Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
                else:
                    #0. Set general variable with leading 0
                    url = "url_" + "{0:03d}".format(i+j+h+1)
                    #4. HighRes PDF as JPG
                    print ("#18-7. HighRes PDF as JPG - 1500px")
                    url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
                    print ("#18-8. Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
        # ************************************************************************
#5 Preview Download URL
print ("#18-9. Preview Download URL")
print ("#18-9. Preview Download Link: " + DL_LINK_LOW)
#6 Print Download URL
print ("#18-10. Print Download URL")
print ("#18-10. Print Download Link: " + DL_LINK_HIGH)
# *********************************************************************************

# *********************************************************************************
#19. Close
print ("#19. Close")
driver.quit()
# *********************************************************************************