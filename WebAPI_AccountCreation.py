# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys, array
import SCKitWebAPI

allArgs = sys.argv
#allArgs = ["PythonFileName", "firefox","https://devops-az01.directsmile.com/webapi/CreateAccount.html", "7.3.0.85", "FQDN", "publickey", "privatekey","email"
#allArgs = ["PythonFileName", "firefox", "https://devops-az01.directsmile.com/webapi/CreateAccount.html", "7.3.1.115", "devops-az01.directsmile.com", "2386F6D9DFC0F95B827F521810010D93", "DC83E546EC0B63CF92CF54CE5B34700150EA167582A10977EF68C9209B10F4E9", "devopsci@efi.com"]
# "PythonFileName" "firefox" "https://devops-az01.directsmile.com/webapi/CreateAccount.html" "7.3.1.115" "devops-az01.directsmile.com" "2386F6D9DFC0F95B827F521810010D93" "DC83E546EC0B63CF92CF54CE5B34700150EA167582A10977EF68C9209B10F4E9" "devopsci@efi.com"

# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
# **********************************************************************************************
# THIS IS SMOKE TEST FOR SMARTCANVAS
print ("THIS IS SMOKE TEST FOR SMARTCANVAS - General Functionality Test")
# **********************************************************************************************

driver = SCKitWebAPI.magicArgs(allArgs)
#CREDENTIAL = SCKitWebAPI.WebAPI_CreateAccount("devops-az01.directsmile.com", "2386F6D9DFC0F95B827F521810010D93", "DC83E546EC0B63CF92CF54CE5B34700150EA167582A10977EF68C9209B10F4E9","devopsci@efi.com")
CREDENTIAL = SCKitWebAPI.WebAPI_CreateAccount(driver[6],driver[7],driver[8],driver[9])
# Print Stored Information
print ("NewAccountID=" + str(CREDENTIAL[0]))
print ("NewAccountPublickey=" + str(CREDENTIAL[1]))
print ("NewAccountPrivatekey=" + str(CREDENTIAL[2]))
SCKitWebAPI.quitDriver()
#
