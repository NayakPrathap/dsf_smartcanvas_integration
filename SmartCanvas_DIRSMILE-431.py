# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys
import SCKit, SCKitRegression

allArgs = sys.argv
#allArgs = ["myScript", "firefox", "https://devops-az01.directsmile.com/smartcanvas/index007.html", "7.3.9.181"]

# *********************************************************************************
# THIS IS REGRESSION TEST ABOUT DSF-31075 & DIRSMILE-431
# Please refer to below link for further information 
# https://jira.efi.com/browse/DSF-31873
# https://jira.efi.com/browse/DIRSMILE-431
print ("THIS IS REGRESSION TEST ABOUT DSF-31874 & DIRSMILE-431")
print ("Please refer to below link for further information")
print ("https://jira.efi.com/browse/DSF-31075")
print ("https://jira.efi.com/browse/DIRSMILE-431")
# *********************************************************************************

driver = SCKitRegression.magicArgs(allArgs)
SCKitRegression.ResizeDocument(10)
SCKitRegression.changeDocumentDirection("Landscape")
SCKitRegression.DIRSMILE_431_AddFont("C:\\temp\\SmartCanvas_FontUpload\\Arial","1")
SCKitRegression.DIRSMILE_431_AddFont("C:\\temp\\SmartCanvas_FontUpload\\Arial","2")
SCKitRegression.DIRSMILE_431_AddFont("C:\\temp\\SmartCanvas_FontUpload\\Arial","3")
SCKitRegression.DIRSMILE_431_AddFont("C:\\temp\\SmartCanvas_FontUpload\\Arial","4")
SCKitRegression.DIRSMILE_431_AddItem()
SCKitRegression.saveCanvas()
SCKitRegression.outputPDF_LOW()
SCKitRegression.outputPDF_HIGH()
SCKitRegression.cleanupCanvas()
#SCKitRegression.DIRSMILE_431_CleanupFont()
SCKitRegression.printPDF(1)
SCKitRegression.quitDriver()