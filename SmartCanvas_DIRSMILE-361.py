# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys
import SCKit, SCKitRegression

allArgs = sys.argv

# *********************************************************************************
# THIS IS REGRESSION TEST ABOUT DIRSMILE-361
# Please refer to below link for further information 
# https://jira.efi.com/browse/DIRSMILE-361
print ("---- THIS IS REGRESSION TEST ABOUT DIRSMILE-361")
print ("---- Please refer to below link for further information")
print ("---- https://jira.efi.com/browse/DIRSMILE-361")
# *********************************************************************************
driver = SCKitRegression.magicArgs(allArgs)
SCKitRegression.ResizeDocument(10)
SCKitRegression.changeDocumentDirection("Landscape")
SCKitRegression.DIRSMILE_361_AddEdit()
SCKitRegression.DIRSMILE_361_OpenBuyerMode()
SCKitRegression.DIRSMILE_361_EditTextOnBuyerView()
SCKitRegression.DIRSMILE_361_EditTextOnBuyerViewSecondTime()
SCKitRegression.DIRSMILE_361_CloseBuyerMode()
SCKitRegression.saveCanvas()
SCKitRegression.outputPDF_LOW()
SCKitRegression.outputPDF_HIGH()
SCKitRegression.cleanupCanvas()
SCKitRegression.printPDF(1)
SCKitRegression.quitDriver()