# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys, array
import SCKitWebAPI

allArgs = sys.argv
#allArgs = ["PythonFileName", "firefox","https://devops-az01.directsmile.com/webapi/index.html", "7.3.0.85", "FQDN", "NewAccountPublickey", "NewAccountPrivatekey","NewSCDocumentName", "NewSCDocumentDescription", "NewAccountID"]
#allArgs = ["PythonFileName", "firefox", "https://devops-az01.directsmile.com/webapi/index.html", "7.3.1.115", "devops-az01.directsmile.com", "CE1F718B", "656D5312EDA1AE5B", "SCTemplate001","My SC Template Description","10"]
# "PythonFileName" "firefox" "https://devops-az01.directsmile.com/webapi/index.html" "7.3.1.115" "devops-az01.directsmile.com" "CE1F718B" "656D5312EDA1AE5B" "SCTemplate001" "My SC Template Description" "10"

# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
# **********************************************************************************************
# THIS IS SMOKE TEST FOR SMARTCANVAS
print ("THIS IS SMOKE TEST FOR SMARTCANVAS - General Functionality Test")
# **********************************************************************************************
driver = SCKitWebAPI.magicArgs(allArgs)
#SCKitWebAPI.SmartCanvas_CreateEmptyDocument("devops-az01.directsmile.com", "036C2D63", "30136099B863E956","mySCTemplated","my first SmartCanvas template","9")
sctempUrl = SCKitWebAPI.SmartCanvas_CreateEmptyDocument(driver[6], driver[7], driver[8], driver[10], driver[11], driver[12])
print ("SmartCanvas_URL="+str(sctempUrl))
SCKitWebAPI.quitDriver()
# 