# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys

driver = None

def getOrCreateWebDriver():
	global driver
	driver = driver or webdriver.Firefox()
	return driver

def magicArgs(allArgsParent):
	global driver
	global TestOnSauceLabs
	global myVersion_Major
	global myVersion_Mainor
	global myVersion_Update
	global myVersion_Build
	global FQDN
	global publicKey
	global privateKey
	global EmailAddress
	global SCTemplateName
	global SCTemplateDesc
	global AccountID
    # *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# Filename: sysArgvTest.py
	# Function: Test to retrieve system argument value
	# Behavior: Based on provided argument, it run test locally or over SauceLabs
	#           If some arguments are missing, then we stop to run test
	# Usage: Jenkins execution with parameter value
	# Author: Nobuaki Ogawa - nobuaki.ogawa@efi.com
    # *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# Arguments Case 1 = SauceLabs - WebAPI_CreateAccount():
	#   [1] Python file name ---- Needed to be listed to simulate command execution case system arguments
	#   [2] Browser Name (SauceLabs - browserName) --- Input Example <firefox|chorme|firefox dev|firefox dev 64?>
	#   [3] Target Test URL
	#   [4] SmartCanvas - Version (7.2.x.x|7.3.x.x|Trunk(7.9.x.x))
	#   [5] FQDN
	#   [6] publicKey - platform
	#   [7] privateKey - version
	#   [8] EmailAddress - name	
	#   [9] SauceLabs URL
	#   [10] SauceLabs - platform
	#   [11] SauceLabs - version
	#   [12] SauceLabs - name
    # *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# Arguments Case 2 = SauceLabs - SmartCanvas_CreateEmptyDocument():
	#   [1] Python file name ---- Needed to be listed to simulate command execution case system arguments
	#   [2] Browser Name (SauceLabs - browserName) --- Input Example <firefox|chorme|firefox dev|firefox dev 64?>
	#   [3] Target Test URL
	#   [4] SmartCanvas - Version (7.2.x.x|7.3.x.x|Trunk(7.9.x.x))
	#   [5] FQDN
	#   [6] publicKey - platform
	#   [7] privateKey - version
    #   [8] SCTemplateName - SmartCanvas Document Template's name
    #   [9] SCTemplateDesc - SmartCanvas Document Template's description
    #   [10] AccountID - AccountID which was generated over WebAPI
	#   [11] SauceLabs URL
	#   [12] SauceLabs - platform
	#   [13] SauceLabs - version
	#   [14] SauceLabs - name
    # *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# Arguments Case 3 = Local - WebAPI_CreateAccount():
	#   [1] Python file name ---- Needed to be listed to simulate command execution case system arguments
	#   [2] Browser Name (SauceLabs - browserName) --- Input Example <firefox|chorme|firefox dev|firefox dev 64?>
	#   [3] Target Test URL
	#   [4] SmartCanvas - Version (7.2.x.x|7.3.x.x|Trunk(7.9.x.x))
	#   [5] FQDN
	#   [6] publicKey - WebAPI generated public key
	#   [7] privateKey - WebAPI generated private key
	#   [8] EmailAddress - name	
    # *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# Arguments Case 4 = Local - SmartCanvas_CreateEmptyDocument():
	#   [1] Python file name ---- Needed to be listed to simulate command execution case system arguments
	#   [2] Browser Name (SauceLabs - browserName) --- Input Example <firefox|chorme|firefox dev|firefox dev 64?>
	#   [3] Target Test URL
	#   [4] SmartCanvas - Version (7.2.x.x|7.3.x.x|Trunk(7.9.x.x))
	#   [5] FQDN
	#   [6] publicKey - WebAPI generated public key
	#   [7] privateKey - WebAPI generated private key
    #   [8] SCTemplateName - SmartCanvas Document Template's name
    #   [9] SCTemplateDesc - SmartCanvas Document Template's description
    #   [10] AccountID - AccountID which was generated over WebAPI
    # *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
    #
    # Tips: If you run test over SauceLabs, please ensure all above arguments were provided while test. 
	#
    # *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# **********************************************************
	# Create several variable based on system arguments as well as fallback value
	# **********************************************************
	allArgs = allArgsParent
	countArgs = len(allArgs)
	print("001. Provided Arguments Counts: " + str(countArgs))
	# Set FireFox exe path
	FIREFOX_STD_EXE_PATH = "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"
	FIREFOX_DEV_EXE_PATH  = "C:\\Program Files (x86)\\Mozilla Developer Preview\\firefox.exe"
	FIREFOX_STD_EXE_PATH_X64 = "C:\\Program Files\\Mozilla Firefox\\firefox.exe"
	FIREFOX_DEV_EXE_PATH_X64 = "C:\\Program Files\\Mozilla Developer Preview\\firefox.exe\\"
	default_URL = "https://devops-az01.directsmile.com/smartcanvas/index.html"
	default_SCVersion = "7.3.0.85"
	TestOnSauceLabs = False
	default_SCDocumentName = "SmartCanvas_Template_Test_0000000000"
	default_EmailAddress = "devopsci@efi.com"
	default_SCDocumentDesc = "SmartCanvas Template for Nobukins CTP"
	default_AccountID = "0123456789"
	# **********************************************************
	# Check whether any parameter was provided or not
	# **********************************************************
	if countArgs > 13:
		print("002. TestURL & SauceLabsURL were provide within script call as system arguments")
		print("003. Run script via SauceLabs")
		TestOnSauceLabs = True
		print ("003-00. Echo provided arg 00: "+str(allArgsParent[0:]))
		Browser = allArgsParent[1:]
		myBrowser = Browser[0]
		print ("003-01. Echo provided arg 01: "+str(myBrowser))
		TestURL = allArgsParent[2:]
		myUrl = TestURL[0]
		print ("003-02. Echo provided arg 02: "+str(myUrl))
		SmartCanvasVersion = allArgsParent[3:]
		myVersion = SmartCanvasVersion[0]
		print ("003-03. Echo provided arg 03: "+str(myVersion))
		TargetFQDN = allArgsParent[4:]
		FQDN = TargetFQDN[0]
		print ("003-04. Echo provided arg 04: "+str(FQDN))
		AdminPublicKey = allArgsParent[5:]
		publicKey = AdminPublicKey[0]
		print ("003-05. Echo provided arg 05: "+str(publicKey))
		AdminPrivateKey = allArgsParent[6:]
		privateKey = AdminPrivateKey[0]
		print ("003-06. Echo provided arg 06: "+str(privateKey))
		SCDocumentName = allArgsParent[7:]
		SCTemplateName = SCDocumentName[0]
		print ("003-07. Echo provided arg 07: "+str(SCTemplateName))
		SCDocumentDesc = allArgsParent[8:]
		SCTemplateDesc = SCDocumentDesc[0]
		print ("003-08. Echo provided arg 08: "+str(SCTemplateDesc))
		SCAccountID = allArgsParent[9:]
		AccountID = SCAccountID[0]
		print ("003-09. Echo provided arg 09: "+str(AccountID))
		SauceLabsURL = allArgsParent[10:]
		print ("003-10. Echo provided arg 10: "+str(SauceLabsURL))
		SauceLabs_platform = allArgsParent[11:]
		print ("003-11. Echo provided arg 11: "+str(SauceLabs_platform))
		SauceLabs_version = allArgsParent[12:]
		print ("003-12. Echo provided arg 12: "+str(SauceLabs_version))
		SauceLabs_name = allArgsParent[13:]
		print ("003-13. Echo provided arg 13: "+str(SauceLabs_name))
		if myBrowser == "firefox dev":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 01")
		elif myBrowser == "firefox dev 64":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 02")
		elif myBrowser == "firefox 64":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 03")
		else:
			SauceLabs_browserName = myBrowser
			print ("Change browser to specified one -- 04")
		# **********************************************************************************************
		# This is the only code you need to edit in your existing scripts
		# The comm[and_executor tells the test to run on Sauce, while the desired_capabilities
		# parameter tells us which browsers and OS to spin up.
		# **********************************************************************************************
		desired_cap = {
			'platform': SauceLabs_platform[0],
			'browserName': SauceLabs_browserName,
			'version': SauceLabs_version[0],
			'name': SauceLabs_name[0]+ " - " + myVersion,
		}
		driver = webdriver.Remote(
			command_executor = SauceLabsURL[0],
			desired_capabilities = desired_cap
		)
        # ******** Set Default value to prevent error ********
		# 004-01. Set target Email address: 
		EmailAddress = default_EmailAddress
		print ("004-01. Set target Email address: "+str(EmailAddress))
        # ****************************************************
	if countArgs > 11:
		print("002. TestURL & SauceLabsURL were provide within script call as system arguments")
		print("003. Run script via SauceLabs")
		print ("003-00. Echo provided arg 00: "+str(allArgsParent[0:]))
		TestOnSauceLabs = True
		Browser = allArgsParent[1:]
		myBrowser = Browser[0]
		print ("003-01. Echo provided arg 01: "+str(myBrowser))
		TestURL = allArgsParent[2:]
		myUrl = TestURL[0]
		print ("003-02. Echo provided arg 02: "+str(myUrl))
		SmartCanvasVersion = allArgsParent[3:]
		myVersion = SmartCanvasVersion[0]
		print ("003-03. Echo provided arg 03: "+str(myVersion))
		TargetFQDN = allArgsParent[4:]
		FQDN = TargetFQDN[0]
		print ("003-04. Echo provided arg 04: "+str(FQDN))
		AdminPublicKey = allArgsParent[5:]
		publicKey = AdminPublicKey[0]
		print ("003-05. Echo provided arg 05: "+str(publicKey))
		AdminPrivateKey = allArgsParent[6:]
		privateKey = AdminPrivateKey[0]
		print ("003-06. Echo provided arg 06: "+str(privateKey))
		SCAccountEmail = allArgsParent[7:]
		EmailAddress = SCAccountEmail[0]
		print ("003-07. Echo provided arg 07: "+str(EmailAddress))
		SauceLabsURL = allArgsParent[8:]
		print ("003-08. Echo provided arg 08: "+str(SauceLabsURL))
		SauceLabs_platform = allArgsParent[9:]
		print ("003-09. Echo provided arg 09: "+str(SauceLabs_platform))
		SauceLabs_version = allArgsParent[10:]
		print ("003-10. Echo provided arg 10: "+str(SauceLabs_version))
		SauceLabs_name = allArgsParent[11:]
		print ("003-11. Echo provided arg 11: "+str(SauceLabs_name))
		if myBrowser == "firefox dev":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 01")
		elif myBrowser == "firefox dev 64":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 02")
		elif myBrowser == "firefox 64":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 03")
		else:
			SauceLabs_browserName = myBrowser
			print ("Change browser to specified one -- 04")
		# **********************************************************************************************
		# This is the only code you need to edit in your existing scripts
		# The comm[and_executor tells the test to run on Sauce, while the desired_capabilities
		# parameter tells us which browsers and OS to spin up.
		# **********************************************************************************************
		desired_cap = {
			'platform': SauceLabs_platform[0],
			'browserName': SauceLabs_browserName,
			'version': SauceLabs_version[0],
			'name': SauceLabs_name[0]+ " - " + myVersion,
		}
		driver = webdriver.Remote(
			command_executor = SauceLabsURL[0],
			desired_capabilities = desired_cap
		)
        # ******** Set Default value to prevent error ********
		# 004-01. Set target SC Template name: 
		SCTemplateName = default_SCDocumentName
		print ("004-01. Set target SC Template name: "+str(SCTemplateName))
		# 004-02. Set target SC Template description: 
		SCTemplateDesc = default_SCDocumentDesc
		print ("004-02. Set target SC Template description: "+str(SCTemplateDesc))
		# 004-03. Set target WebAPI Account ID: 
		AccountID = default_AccountID
		print ("004-03. Set target WebAPI Account ID: "+str(AccountID))
        # ****************************************************
	elif countArgs > 9:
		print("002. Browser & TestURL, and SmartCanvas Version were provided within script call as system arguments")
		print("003. Run script locally")
		print ("003-00. Echo provided arg 00: "+str(allArgsParent[0:]))
		Browser = allArgsParent[1:]
		myBrowser = Browser[0]
		print ("003-01. Echo provided arg 01: "+str(myBrowser))
		TestURL = allArgsParent[2:]
		myUrl = TestURL[0]
		print ("003-02. Echo provided arg 02: "+str(myUrl))
		SmartCanvasVersion = allArgsParent[3:]
		myVersion = SmartCanvasVersion[0]
		print ("003-03. Echo provided arg 03: "+str(myVersion))
		TargetFQDN = allArgsParent[4:]
		FQDN = TargetFQDN[0]
		print ("003-04. Echo provided arg 04: "+str(FQDN))
		AdminPublicKey = allArgsParent[5:]
		publicKey = AdminPublicKey[0]
		print ("003-05. Echo provided arg 05: "+str(publicKey))
		AdminPrivateKey = allArgsParent[6:]
		privateKey = AdminPrivateKey[0]
		print ("003-06. Echo provided arg 06: "+str(privateKey))
		SCDocumentName = allArgsParent[7:]
		SCTemplateName = SCDocumentName[0]
		print ("003-07. Echo provided arg 07: "+str(SCTemplateName))
		SCDocumentDesc = allArgsParent[8:]
		SCTemplateDesc = SCDocumentDesc[0]
		print ("003-08. Echo provided arg 08: "+str(SCTemplateDesc))
		SCAccountID = allArgsParent[9:]
		AccountID = SCAccountID[0]
		print ("003-09. Echo provided arg 09: "+str(AccountID))
		if myBrowser == "chrome":        
			# **********************************************************
			#004. Set Chrome as default browser
			# **********************************************************
			print ("004. Set Chrome as default browser")
			driver = webdriver.Chrome()
			#006. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (Mozilla Developer Preview)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev 64":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox 64":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (FireFox - 64bit)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (FireFox - 64bit)")
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		else:
			# **********************************************************
			#004. Set Desired Capabilities for local execution (FireFox)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (FireFox)")
			myBrowser = "firefox"
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#005. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
        # ******** Set Default value to prevent error ********
		# 006. Set target Email address: 
		EmailAddress = default_EmailAddress
		print ("006. Set target Email address: "+str(EmailAddress))
        # ****************************************************
	elif countArgs > 7:
		print("002. Browser & TestURL, and SmartCanvas Version were provided within script call as system arguments")
		print("003. Run script locally")
		print ("003-00. Echo provided arg 00: "+str(allArgsParent[0:]))
		Browser = allArgsParent[1:]
		myBrowser = Browser[0]
		print ("003-01. Echo provided arg 01: "+str(myBrowser))
		TestURL = allArgsParent[2:]
		myUrl = TestURL[0]
		print ("003-02. Echo provided arg 02: "+str(myUrl))
		SmartCanvasVersion = allArgsParent[3:]
		myVersion = SmartCanvasVersion[0]
		print ("003-03. Echo provided arg 03: "+str(myVersion))
		TargetFQDN = allArgsParent[4:]
		FQDN = TargetFQDN[0]
		print ("003-04. Echo provided arg 04: "+str(FQDN))
		AdminPublicKey = allArgsParent[5:]
		publicKey = AdminPublicKey[0]
		print ("003-05. Echo provided arg 05: "+str(publicKey))
		AdminPrivateKey = allArgsParent[6:]
		privateKey = AdminPrivateKey[0]
		print ("003-06. Echo provided arg 06: "+str(privateKey))
		SCAccountEmail = allArgsParent[7:]
		EmailAddress = SCAccountEmail[0]
		print ("003-07. Echo provided arg 07: "+str(EmailAddress))
		if myBrowser == "chrome":        
			# **********************************************************
			#004. Set Chrome as default browser
			# **********************************************************
			print ("004. Set Chrome as default browser")
			driver = webdriver.Chrome()
			#006. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (Mozilla Developer Preview)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev 64":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox 64":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (FireFox - 64bit)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (FireFox - 64bit)")
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		else:
			# **********************************************************
			#004. Set Desired Capabilities for local execution (FireFox)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (FireFox)")
			myBrowser = "firefox"
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#005. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
        # ******** Set Default value to prevent error ********
		# 005-01. Set target SC Template name: 
		SCTemplateName = default_SCDocumentName
		print ("005-01. Set target SC Template name: "+str(SCTemplateName))
		# 005-02. Set target SC Template description: 
		SCTemplateDesc = default_SCDocumentDesc
		print ("005-02. Set target SC Template description: "+str(SCTemplateDesc))
		# 005-03. Set target WebAPI Account ID: 
		AccountID = default_AccountID
		print ("005-03. Set target WebAPI Account ID: "+str(AccountID))
        # ****************************************************
	elif countArgs > 2:
		print("002. Browser was provided, but neither TestURL nor SmartCanvas Version couldn't be found within script call as system arguments")
		print("003. Run script locally")
		Browser = allArgsParent[0:]
		myBrowser = Browser[0]
		print("004. Set default target URL")
		# 004 Set default target URL as fallback
		myUrl = default_URL
        # ******** Set Default value to prevent error ********
		# 005-01. Set target SC Version: 
		myVersion = default_SCVersion
		print ("005-01. Set target SC Version: " + str(myVersion))
		# 005-02. Set target Email address: 
		EmailAddress = default_EmailAddress
		print ("005-02. Set target Email address: "+str(EmailAddress))
		# 005-03. Set target SC Template name: 
		SCTemplateName = default_SCDocumentName
		print ("005-03. Set target SC Template name: "+str(SCTemplateName))
		# 005-04. Set target SC Template description: 
		SCTemplateDesc = default_SCDocumentDesc
		print ("005-04. Set target SC Template description: "+str(SCTemplateDesc))
		# 005-05. Set target WebAPI Account ID: 
		AccountID = default_AccountID
		print ("005-05. Set target WebAPI Account ID: "+str(AccountID))
        # ****************************************************
		if myBrowser == "chrome":
			# **********************************************************
			# 006. Set Chrome as default browser
			# **********************************************************
			print ("006. Set Chrome as default browser")
			driver = webdriver.Chrome()
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (Mozilla Developer Preview)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev 64":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox 64":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (FireFox - 64bit)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (FireFox - 64bit)")
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		else:
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (FireFox)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (FireFox)")
			myBrowser = "firefox"
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
	else:
		print("002. None of Browser, TestURL, SmartCanvas Version were provided within script call as system arguments")
		print("003. Run script locally")
		# 004 Set default as fallback
		print("004. Set default target URL")
		myUrl = default_URL
        # ******** Set Default value to prevent error ********
		# 006-01. Set target SC Version: 
		myVersion = default_SCVersion
		print ("006-01. Set target SC Version: " + str(myVersion))
		# 006-02. Set target Email address: 
		EmailAddress = default_EmailAddress
		print ("006-02. Set target Email address: "+str(EmailAddress))
		# 006-03. Set target SC Template name: 
		SCTemplateName = default_SCDocumentName
		print ("006-03. Set target SC Template name: "+str(SCTemplateName))
		# 006-04. Set target SC Template description: 
		SCTemplateDesc = default_SCDocumentDesc
		print ("006-04. Set target SC Template description: "+str(SCTemplateDesc))
		# 006-05. Set target WebAPI Account ID: 
		AccountID = default_AccountID
		print ("006-05. Set target WebAPI Account ID: "+str(AccountID))
        # ****************************************************
		# **********************************************************
		# 007. Set Desired Capabilities for local execution
		# **********************************************************
		print ("007. Set Desired Capabilities for local execution")
		myBrowser = "firefox"
		binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
		caps = DesiredCapabilities.FIREFOX
		driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)

	# **********************************************************
	# Check actual Test URL
	# **********************************************************
	print("---- Test URL: " + myUrl) 

	# **********************************************************
	# Check Target SmartCanvas Version
	# **********************************************************
	print("---- SmartCanvas Version: " + myVersion) 
	myVersion_Major = myVersion.split(".")[0]
	myVersion_Mainor = myVersion.split(".")[1]
	myVersion_Update = myVersion.split(".")[2]
	myVersion_Build = myVersion.split(".")[3]
	# Set Ver7 >= condition
	if int(myVersion_Major) >= 7 and int(myVersion_Mainor) >= 3 and int(myVersion_Update) >= 0 and int(myVersion_Build) >= 85:
		print ("---- Alert may appear")
	else:
		print ("---- Alert may not appear while uploading")
	# **********************************************************
	
	# **********************************************************
	#1. Start WebDriver
	print ("#1. Start WebDriver")
	driver.implicitly_wait(30)

	#2. Browse specify url
	print ("#2. Browse specify url")
	driver.get(myUrl)
	#wait = WebDriverWait(driver, 10)
	#logo = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, "div.scLogo")))
	#print ("#2-1, I waited dynamically")
	#time.sleep(3)
	# **********************************************************
	return (driver,TestOnSauceLabs,	myVersion_Major, myVersion_Mainor, myVersion_Update, myVersion_Build, FQDN, publicKey, privateKey, EmailAddress, SCTemplateName, SCTemplateDesc, AccountID)

def WebAPI_CreateAccount(FQDN,publicKey,privateKey,EmailAddress):
    global AccountID
    global PublicKey
    global PrivateKey
    # **********************************************************************************************
    # Setting Section
    driver.find_element_by_id("targetSystem").clear()
    driver.find_element_by_id("targetSystem").send_keys(FQDN)
    driver.find_element_by_id("publicKey").clear()
    driver.find_element_by_id("publicKey").send_keys(publicKey)
    driver.find_element_by_id("privateKey").clear()
    driver.find_element_by_id("privateKey").send_keys(privateKey)
    # Data Section
    driver.find_element_by_xpath("//div[@id='modelBox']/div/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div/div/input").send_keys(EmailAddress)
    driver.find_element_by_xpath("//div[@id='modelBox']/div/div/input").send_keys(Keys.ENTER)
    driver.find_element_by_xpath("//div[@id='modelBox']/div[2]/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div[2]/div/input").send_keys("DevOps23")
    driver.find_element_by_xpath("//div[@id='modelBox']/div[2]/div/input").send_keys(Keys.ENTER)
    driver.find_element_by_xpath("//div[@id='modelBox']/div[3]/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div[3]/div/input").send_keys("Web API Account Creation")
    driver.find_element_by_xpath("//div[@id='modelBox']/div[3]/div/input").send_keys(Keys.ENTER)
    driver.find_element_by_xpath("//div[@id='modelBox']/div[4]/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div[4]/div/input").send_keys("10")
    driver.find_element_by_xpath("//div[@id='modelBox']/div[4]/div/input").send_keys(Keys.ENTER)
    driver.find_element_by_xpath("//div[@id='modelBox']/div[5]/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div[5]/div/input").send_keys("Mr.")
    driver.find_element_by_xpath("//div[@id='modelBox']/div[5]/div/input").send_keys(Keys.ENTER)
    driver.find_element_by_xpath("//div[@id='modelBox']/div[6]/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div[6]/div/input").send_keys("Developments")
    driver.find_element_by_xpath("//div[@id='modelBox']/div[6]/div/input").send_keys(Keys.ENTER)
    driver.find_element_by_xpath("//div[@id='modelBox']/div[7]/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div[7]/div/input").send_keys("Operations")
    driver.find_element_by_xpath("//div[@id='modelBox']/div[7]/div/input").send_keys(Keys.ENTER)
    driver.find_element_by_xpath("//div[@id='modelBox']/div[8]/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div[8]/div/input").send_keys(EmailAddress)
    driver.find_element_by_xpath("//div[@id='modelBox']/div[8]/div/input").send_keys(Keys.ENTER)
    Select(driver.find_element_by_css_selector("select.form-control")).select_by_visible_text("Male")
    driver.find_element_by_xpath("//div[@id='modelBox']/div[10]/div/input").clear()
    driver.find_element_by_xpath("//div[@id='modelBox']/div[10]/div/input").send_keys("efi")
    driver.find_element_by_xpath("//div[@id='modelBox']/div[10]/div/input").send_keys(Keys.ENTER)
    #driver.find_element_by_xpath("//div[@id='modelBox']/div[11]/div/input").clear()
    #driver.find_element_by_xpath("//div[@id='modelBox']/div[11]/div/input").send_keys("onHoldCallbackUrl")
    #driver.find_element_by_xpath("//div[@id='modelBox']/div[11]/div/input").send_keys(Keys.ENTER)
    #driver.find_element_by_xpath("//div[@id='modelBox']/div[12]/div/input").clear()
    #driver.find_element_by_xpath("//div[@id='modelBox']/div[12]/div/input").send_keys("AdditionalOptionJson")
    #driver.find_element_by_xpath("//div[@id='modelBox']/div[12]/div/input").send_keys(Keys.ENTER)
    #Select(driver.find_element_by_xpath("//div[@id='modelBox']/div[13]/div/select")).select_by_visible_text("Randomize")
    #Select(driver.find_element_by_xpath("//div[@id='modelBox']/div[13]/div/select")).select_by_visible_text("Secure Hash")
    #Select(driver.find_element_by_xpath("//div[@id='modelBox']/div[13]/div/select")).select_by_visible_text("Columns Plus Random Number")
    Select(driver.find_element_by_xpath("//div[@id='modelBox']/div[13]/div/select")).select_by_visible_text("Default")
    # Create New Account
    driver.find_element_by_id("createRequest").click()
    # Confirm Account Creation
    WebDriverWait(driver, 60).until(EC.text_to_be_present_in_element((By.ID,"statusBox"),"created account"))
    # Assert Print Bleed area text
    element = driver.find_element_by_id("statusBox")
    assert element.text == "created account"
    # Store Account Information
    AccountID = driver.find_element_by_id("accountId").get_attribute("value")
    PublicKey = driver.find_element_by_id("accountPublicKey").get_attribute("value")
    PrivateKey = driver.find_element_by_id("accountPrivateKey").get_attribute("value")
    return (AccountID, PublicKey, PrivateKey)
    # **********************************************************************************************

def SmartCanvas_CreateEmptyDocument(FQDN, PublicKey, PrivateKey, documentName,documentDisplayName,AccountID):
    global SmartCanvasTemplateURL
    # **********************************************************************************************
    driver.find_element_by_xpath("(//a[contains(text(),'Try it!')])[23]").click()
    driver.find_element_by_id("targetSystem").clear()
    driver.find_element_by_id("targetSystem").send_keys(FQDN)
    driver.find_element_by_id("targetSystem").send_keys(Keys.ENTER)
    driver.find_element_by_id("publicKey").clear()
    driver.find_element_by_id("publicKey").send_keys(PublicKey)
    driver.find_element_by_id("publicKey").send_keys(Keys.ENTER)
    driver.find_element_by_id("privateKey").clear()
    driver.find_element_by_id("privateKey").send_keys(PrivateKey)
    driver.find_element_by_id("privateKey").send_keys(Keys.ENTER)
    driver.find_element_by_id("documentName").clear()
    driver.find_element_by_id("documentName").send_keys(documentName)
    driver.find_element_by_id("documentName").send_keys(Keys.ENTER)
    driver.find_element_by_id("documentDisplayName").clear()
    driver.find_element_by_id("documentDisplayName").send_keys(documentDisplayName)
    driver.find_element_by_id("documentDisplayName").send_keys(Keys.ENTER)
    driver.find_element_by_id("storageId").clear()
    driver.find_element_by_id("storageId").send_keys(AccountID)
    driver.find_element_by_id("storageId").send_keys(Keys.ENTER)
    driver.find_element_by_id("storagePublicKey").clear()
    driver.find_element_by_id("storagePublicKey").send_keys(PublicKey)
    driver.find_element_by_id("storagePublicKey").send_keys(Keys.ENTER)
    driver.find_element_by_id("storagePrivateKey").clear()
    driver.find_element_by_id("storagePrivateKey").send_keys(PrivateKey)
    driver.find_element_by_id("storagePrivateKey").send_keys(Keys.ENTER)
    # Create Empty Document
    driver.find_element_by_id("createEmptyDocument").click()
    # Confirm status message
    WebDriverWait(driver, 120).until(EC.text_to_be_present_in_element((By.ID,"statusBox"),"New document created."))
    # Assert status message text
    element = driver.find_element_by_id("statusBox")
    assert element.text == "New document created."
    # Open document in SmartCanvas
    driver.find_element_by_id("openInSmartCanvas").click()
    driver.switch_to_window(driver.window_handles[1])
    driver.title
    SmartCanvasTemplateURL = driver.current_url
    return SmartCanvasTemplateURL
    # **********************************************************************************************

def quitDriver():
    # **********************************************************************************************
    #15. Close
    print ("#15. Close")
    driver.quit()
    # **********************************************************************************************
