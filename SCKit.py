# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys

driver = None

def getOrCreateWebDriver():
	global driver
	driver = driver or webdriver.Firefox()
	return driver

def magicArgs(allArgsParent):
	global driver
	# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# Filename: sysArgvTest.py
	# Function: Test to retrieve system argument value
	# Behavior: Based on provided argument, it run test locally or over SauceLabs
	#           If some arguments are missing, then we stop to run test
	# Usage: Jenkins execution with parameter value
	# Author: Nobuaki Ogawa - nobuaki.ogawa@efi.com
	# Arguments:
	#   [1] Browser Name (SauceLabs - browserName) --- Input Example <firefox|chorme|firefox dev|firefox dev 64?>
	#   [2] Target Test URL
	#   [3] SmartCanvas - Version (7.2.x.x|7.3.x.x|Trunk(7.9.x.x))
	#   [4] SauceLabs URL
	#   [5] SauceLabs - platform
	#   [6] SauceLabs - version
	#   [7] SauceLabs - name
	# Tips: If you run test over SauceLabs, please ensure all above arguments were provided while test. 
	# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
	# **********************************************************
	# Create several variable based on system arguments as well as fallback value
	# **********************************************************
	allArgs = allArgsParent
	countArgs = len(allArgs)
	print("001. Provided Arguments Counts: " + str(countArgs))
	# Set FireFox exe path
	FIREFOX_STD_EXE_PATH = "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"
	FIREFOX_DEV_EXE_PATH  = "C:\\Program Files (x86)\\Mozilla Developer Preview\\firefox.exe"
	FIREFOX_STD_EXE_PATH_X64 = "C:\\Program Files\\Mozilla Firefox\\firefox.exe"
	FIREFOX_DEV_EXE_PATH_X64 = "C:\\Program Files\\Mozilla Developer Preview\\firefox.exe\\"
	default_URL = "https://devops-az06.directsmile.com/smartcanvas/index.html"
	default_SCVersion = "7.3.0.85"
	TestOnSauceLabs = False
	# **********************************************************
	# Check whether any parameter was provided or not
	# **********************************************************
	if countArgs > 7:
		print("002. TestURL & SauceLabsURL were provide within script call as system arguments")
		print("003. Run script via SauceLabs")
		TestOnSauceLabs = True
		Browser = sys.argv[1:]
		TestURL = sys.argv[2:]
		SmartCanvas_Version = sys.argv[3:]
		SauceLabsURL = sys.argv[4:]
		SauceLabs_platform = sys.argv[5:]
		SauceLabs_version = sys.argv[6:]
		SauceLabs_name = sys.argv[7:]
		myUrl = TestURL[0]
		myBrowser = Browser[0]
		myVersion = SmartCanvas_Version[0]
		if myBrowser == "firefox dev":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 01")
		elif myBrowser == "firefox dev 64":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 02")
		elif myBrowser == "firefox 64":
			SauceLabs_browserName = "firefox"
			print ("Change browser to FireFox -- 03")
		else:
			SauceLabs_browserName = myBrowser
			print ("Change browser to specified one -- 04")
		# **********************************************************************************************
		# This is the only code you need to edit in your existing scripts
		# The command_executor tells the test to run on Sauce, while the desired_capabilities
		# parameter tells us which browsers and OS to spin up.
		# **********************************************************************************************
		desired_cap = {
			'platform': SauceLabs_platform[0],
			'browserName': SauceLabs_browserName,
			'version': SauceLabs_version[0],
			'name': SauceLabs_name[0]+ " - " + myVersion,
		}
		driver = webdriver.Remote(
			command_executor = SauceLabsURL[0],
			desired_capabilities = desired_cap
		)
	elif countArgs > 4 and countArgs < 8:
		print("002. Browser & TestURL & SauceLabsURL were provide within script call as system arguments")
		print("003. Some SauceLabs relates arguments are missing, so we stop test now")
		quit()
	elif countArgs > 3:
		print("002. Browser & TestURL, and SmartCanvas Version were provided within script call as system arguments")
		print("003. Run script locally")
		Browser = sys.argv[1:]
		myBrowser = Browser[0]
		TestURL = sys.argv[2:]
		myUrl = TestURL[0]
		SmartCanvas_Version = sys.argv[3:]
		myVersion = SmartCanvas_Version[0]
		if myBrowser == "chrome":        
			# **********************************************************
			#004. Set Chrome as default browser
			# **********************************************************
			print ("004. Set Chrome as default browser")
			driver = webdriver.Chrome()
			#006. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (Mozilla Developer Preview)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev 64":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox 64":
			# **********************************************************
			#004. Set Desired Capabilities for local execution (FireFox - 64bit)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (FireFox - 64bit)")
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#0. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
		else:
			# **********************************************************
			#004. Set Desired Capabilities for local execution (FireFox)
			# **********************************************************
			print ("004. Set Desired Capabilities for local execution (FireFox)")
			myBrowser = "firefox"
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			#005. Start WebDriver
			print ("005. Start WebDriver")
			driver.implicitly_wait(30)
	elif countArgs > 2:
		print("002. Browser was provided, but neither TestURL nor SmartCanvas Version couldn't be found within script call as system arguments")
		print("003. Run script locally")
		Browser = sys.argv[1:]
		myBrowser = Browser[0]
		print("004. Set default target URL")
		# 004 Set default target URL as fallback
		myUrl = default_URL
		# 005. Set target SC Version: 
		myVersion = default_SCVersion
		print ("005. Set target SC Version: " + str(myVersion))
		if myBrowser == "chrome":
			# **********************************************************
			# 006. Set Chrome as default browser
			# **********************************************************
			print ("006. Set Chrome as default browser")
			driver = webdriver.Chrome()
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (Mozilla Developer Preview)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (Mozilla Developer Preview)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox dev 64":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (Mozilla Developer Preview - 64bit)")
			binary = FirefoxBinary(FIREFOX_DEV_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		elif myBrowser == "firefox 64":
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (FireFox - 64bit)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (FireFox - 64bit)")
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH_X64)
			myBrowser = "firefox"
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
		else:
			# **********************************************************
			# 006. Set Desired Capabilities for local execution (FireFox)
			# **********************************************************
			print ("006. Set Desired Capabilities for local execution (FireFox)")
			myBrowser = "firefox"
			binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
			caps = DesiredCapabilities.FIREFOX
			driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)
			# 007. Start WebDriver
			print ("007. Start WebDriver")
			driver.implicitly_wait(30)
	else:
		print("002. None of Browser, TestURL, SmartCanvas Version were provided within script call as system arguments")
		print("003. Run script locally")
		# 004 Set default as fallback
		print("004. Set default target URL")
		myUrl = default_URL
		# 005. Set target SC Version: 
		myVersion = default_SCVersion
		print ("005. Set target SC Version: " + str(myVersion))
		# **********************************************************
		# 006. Set Desired Capabilities for local execution
		# **********************************************************
		print ("006. Set Desired Capabilities for local execution")
		myBrowser = "firefox"
		binary = FirefoxBinary(FIREFOX_STD_EXE_PATH)
		caps = DesiredCapabilities.FIREFOX
		driver = webdriver.Firefox(capabilities=caps,firefox_binary=binary)

	# **********************************************************
	# Check actual Test URL
	# **********************************************************
	print("---- Test URL: " + myUrl) 

	# **********************************************************
	# Check Target SmartCanvas Version
	# **********************************************************
	print("---- SmartCanvas Version: " + myVersion) 
	myVersion_Major = myVersion.split(".")[0]
	myVersion_Mainor = myVersion.split(".")[1]
	myVersion_Update = myVersion.split(".")[2]
	myVersion_Build = myVersion.split(".")[3]
	# Set Ver7 >= condition
	if int(myVersion_Major) >= 7 and int(myVersion_Mainor) >= 3 and int(myVersion_Update) >= 0 and int(myVersion_Build) >= 85:
		print ("---- Alert may appear")
	else:
		print ("---- Alert may not appear while uploading")
	# **********************************************************
	
	# **********************************************************
	#1. Start WebDriver
	print ("#1. Start WebDriver")
	driver.implicitly_wait(30)

	#2. Browse specify url
	print ("#2. Browse specify url")
	driver.get(myUrl)
	wait = WebDriverWait(driver, 10)
	logo = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, "div.scLogo")))
	print ("#2-1, I waited dynamically")
	time.sleep(3)
	# **********************************************************
	return driver

def saveCanvas():
	# **********************************************************************************************
	#4. Open File menu and Save it
	print ("#4. Open File menu and Save it")
	#4-1. Open File menu
	print ("#4-1. Open File menu")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	time.sleep(1)
	#4-2. Save
	print ("#4-2. Save")
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[2]").click()
	time.sleep(5)
	# **********************************************************************************************

def outputPDF():
	# **********************************************************************************************
	#5. Output - Default
	print ("#5. Output")
	#5-1. Open File menu and click "Create preview PDF"
	print ("#5-1. Open File menu and click 'Create preview PDF'")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[6]").click()
	#5-2. Wait PDF rendering until 'Download PDF' link appear
	print ("#5-2. Wait PDF rendering until 'Download PDF' link appear")
	WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.LINK_TEXT, "Download PDF")))
	#5-3. Get part of link to be used in post action from "Download PDF" button
	print ("#5-3. Get part of link to be used in post action from 'Download PDF' button")
	DL_LINK_LOW = driver.find_element_by_link_text("Download PDF").get_attribute("href")
	p_LOW = re.compile("(?<=pdf=)(.+)")
	m_LOW = p_LOW.search(DL_LINK_LOW)
	myPDFinfo_LOW = m_LOW.group(0)
	#5-4. Get part of DSMO url
	print ("#5-4. Get part of DSMO url")
	p2_LOW = re.compile("^http(.+)/")
	m2_LOW = p2_LOW.search(DL_LINK_LOW)
	myDSMOURL_LOW = m2_LOW.group(0)
	#5-5. Click OK button to close dialog
	print ("#5-5. Click OK button to close dialog")
	driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
	#5-6. Open File menu and click "Create #print PDF"
	print ("#5-6. Open File menu and click 'Create #print PDF'")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[7]").click()
	#5-7. Wait PDF rendering until 'Download PDF' link appear
	print ("#5-7. Wait PDF rendering until 'Download PDF' link appear")
	WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.LINK_TEXT, "Download PDF")))
	#5-8. Get part of link to be used in post action from "Download PDF" button
	print ("#5-8. Get part of link to be used in post action from 'Download PDF' button")
	DL_LINK_HIGH = driver.find_element_by_link_text("Download PDF").get_attribute("href")
	p_HIGH = re.compile("(?<=pdf=)(.+)")
	m_HIGH = p_HIGH.search(DL_LINK_HIGH)
	myPDFinfo_HIGH = m_HIGH.group(0)
	#5-9. Get part of DSMO url
	print ("#5-9. Get part of DSMO url")
	p2_HIGH = re.compile("^http(.+)/")
	m2_HIGH = p2_HIGH.search(DL_LINK_HIGH)
	myDSMOURL_HIGH = m2_HIGH.group(0)
	#5-10. Click OK button to close dialog
	print ("#5-10. Click OK button to close dialog")
	driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
	# **********************************************************************************************

def outputPDFCustom():
	# **********************************************************************************************
	#8. Output - Custom Document Bleed for #print
	print ("#8. Output")
	#8-1. Open File menu and click "Create preview PDF"
	print ("#8-1. Open File menu and click 'Create preview PDF'")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[6]").click()
	#8-2. Wait PDF rendering until 'Download PDF' link appear
	print ("#8-2. Wait PDF rendering until 'Download PDF' link appear")
	wait = WebDriverWait(driver, 60)
	wait.until(EC.visibility_of_element_located((By.LINK_TEXT, "Download PDF")))
	#8-3. Get part of link to be used in post action from "Download PDF" button
	print ("#8-3. Get part of link to be used in post action from 'Download PDF' button")
	DL_LINK_LOW_CUSTOM = driver.find_element_by_link_text("Download PDF").get_attribute("href")
	p_LOW_CUSTOM = re.compile("(?<=pdf=)(.+)")
	m_LOW_CUSTOM = p_LOW_CUSTOM.search(DL_LINK_LOW_CUSTOM)
	myPDFinfo_LOW_CUSTOM = m_LOW_CUSTOM.group(0)
	#8-4. Get part of DSMO url
	print ("#8-4. Get part of DSMO url")
	p2_LOW_CUSTOM = re.compile("^http(.+)/")
	m2_LOW_CUSTOM = p2_LOW_CUSTOM.search(DL_LINK_LOW_CUSTOM)
	myDSMOURL_LOW_CUSTOM = m2_LOW_CUSTOM.group(0)
	#8-5. Click OK button to close dialog
	print ("#8-5. Click OK button to close dialog")
	driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
	#8-6. Open File menu and click "Create #print PDF"
	print ("#8-6. Open File menu and click 'Create #print PDF'")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[7]").click()
	#5-7. Wait PDF rendering until 'Download PDF' link appear
	print ("#5-7. Wait PDF rendering until 'Download PDF' link appear")
	wait = WebDriverWait(driver, 60)
	wait.until(EC.visibility_of_element_located((By.LINK_TEXT, "Download PDF")))
	#8-8. Get part of link to be used in post action from "Download PDF" button
	print ("#8-8. Get part of link to be used in post action from 'Download PDF' button")
	DL_LINK_HIGH_CUSTOM = driver.find_element_by_link_text("Download PDF").get_attribute("href")
	p_HIGH_CUSTOM = re.compile("(?<=pdf=)(.+)")
	m_HIGH_CUSTOM = p_HIGH_CUSTOM.search(DL_LINK_HIGH_CUSTOM)
	myPDFinfo_HIGH_CUSTOM = m_HIGH_CUSTOM.group(0)
	#8-9. Get part of DSMO url
	print ("#8-9. Get part of DSMO url")
	p2_HIGH_CUSTOM = re.compile("^http(.+)/")
	m2_HIGH_CUSTOM = p2_HIGH_CUSTOM.search(DL_LINK_HIGH_CUSTOM)
	myDSMOURL_HIGH_CUSTOM = m2_HIGH_CUSTOM.group(0)
	#8-10. Click OK button to close dialog
	print ("#8-10. Click OK button to close dialog")
	driver.find_element_by_xpath("//div[40]/div[3]/div/div").click()
	time.sleep(3)
	# **********************************************************************************************
	
def cleanupCanvas():
	# **********************************************************************************************
	#6-1. Clean Up - All Content on Canvas
	print ("#6-1. Clean Up - All Content on Canvas")
	driver.find_element_by_css_selector(".scComposition").click()
	driver.find_element_by_css_selector(".scComposition").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_css_selector(".scComposition").send_keys(Keys.DELETE)
	time.sleep(2)

def cleanupFonts(self):
	#6-2. Clean Up - Manage fonts
	print ("#6-2. Manage fonts")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[9]").click()
	time.sleep(2)
	for i in range(self):
		print ("#6-3. Delete Font - " + "{0:03d}".format(i+1)+ "/011")
		driver.find_element_by_css_selector("span.title").click()
		driver.find_element_by_css_selector("div.scListBtn").click()
		print ("#6-4. Confirm deletion on alert window")
		alert = driver.switch_to_alert()
		alert.accept()
		time.sleep(2)
	print ("#6-5. Close dialog")
	driver.find_element_by_xpath("//div[10]/div[3]/div").click()
	time.sleep(2)
	# **********************************************************************************************

def cleanupDocumentSetting():
	# **********************************************************************************************
	#9. Clean Up - Document settings
	print ("#9. Clean Up - Document settings")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	time.sleep(1)
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
	print ("#9-1. Clean Up - Document settings")
	# ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
	elem_css = "div.scCol-8 > div.scFormCtrl > div.scCtrlDropdown > i.icon-DS_ArrowDown"
	driver.find_element_by_css_selector(elem_css).location_once_scrolled_into_view
	# Margin 
	print ("#9-2. Clean Up - Document settings -- Margin")
	for i in range(4):
		elem_xpath = "(//input[@type='text'])["+str(i+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).clear()
		driver.find_element_by_xpath(elem_xpath).send_keys("0")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	# Print bleed area
	print ("#9-3. Clean Up - Document settings -- Print bleed area")
	# ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
	elem_xpath = "(//input[@type='text'])["+str(4+int(CurrentInputNumber)) +"]"
	driver.find_element_by_xpath(elem_xpath).location_once_scrolled_into_view
	for i in range(4):
		# Print bleed area
		elem_xpath = "(//input[@type='text'])["+str(i+4+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).clear()
		driver.find_element_by_xpath(elem_xpath).send_keys("0")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	# Activate '#print bleed active' option
	print ("#9-3. Clean Up - Document settings -- Deactivate 'Print bleed active' option")
	driver.find_element_by_xpath("//div/main_view/div/div[34]/div[2]/div/div[13]/div/checkbox_ctrl/div/div/div").click()
	time.sleep(1)
	# Display bleed and offset
	for i in range(6):
		elem_xpath = "(//input[@type='text'])["+str(i+8+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).clear()
		driver.find_element_by_xpath(elem_xpath).send_keys("0")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	#8-5. DSMI document for preview rendering
	#driver.find_element_by_xpath("//div[23]/div/checkbox_ctrl/div/div/div").click()
	time.sleep(1)
	#8-6. Close dialog
	print ("#8-6. Close dialog")
	driver.find_element_by_xpath("//div[34]/div[3]/div").click()
	time.sleep(1)
	# **********************************************************************************************

def printPDF():	
	# *********************************************************************************
	#18. Preview PDF: Open DSMO GetImage interface converted PDF page as png and jpg 
	print ("#18. Preview PDF: Open DSMO GetImage interface converted PDF page to PNG and JPG")
	# ************************************
	# Loop as page number amount - optional for multi page PDF support
	for i in range(1):
	# ************************************
		# Loop for Low & High Resolution PDF Output verification
		for j in range(2):
			# Link Generation - Low Resolution
			if str(j) == "0":
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#1. LowRes PDF as - PNG
						print ("#1. LowRes PDF as - PNG - 600px")
						url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#2. LowRes PDF as - JPG
						print ("#2. LowRes PDF as - JPG - 600px")
						url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
			# Link Generation - High Resolution
			else:
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#3. HighRes PDF as PNG
						print ("#3. HighRes PDF as PNG - 1500px")
						url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#4. HighRes PDF as JPG
						print ("#4. HighRes PDF as JPG - 1500px")
						url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)

	#5 Preview Download URL
	print ("#5 Preview Download URL")
	print ("#Preview Download Link: " + DL_LINK_LOW)
	#6 Print Download URL
	print ("#6 Print Download URL")
	print ("#Print Download Link: " + DL_LINK_HIGH)
	# *********************************************************************************

def printPDFCustom():
	# *********************************************************************************
	#18. Preview PDF: Open DSMO GetImage interface converted PDF page as png and jpg 
	print ("#18. Preview PDF: Open DSMO GetImage interface converted PDF page to PNG and JPG")
	print ("     Repeat 2 times for  - Print Bleed Active Option test")
	# ************************************
	# Loop as page number amount - optional for multi page PDF support
	for i in range(1):
	# ************************************
		# Loop for Low & High Resolution PDF Output verification
		for j in range(2):
			# Link Generation - Low Resolution
			if str(j) == "0":
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#1. LowRes PDF as - PNG
						print ("#1. LowRes PDF as - PNG - 600px")
						url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#2. LowRes PDF as - JPG
						print ("#2. LowRes PDF as - JPG - 600px")
						url = myDSMOURL_LOW + "GetImage.ashx?img=" + myPDFinfo_LOW + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
			# Link Generation - High Resolution
			else:
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#3. HighRes PDF as PNG
						print ("#3. HighRes PDF as PNG - 1500px")
						url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#4. HighRes PDF as JPG
						print ("#4. HighRes PDF as JPG - 1500px")
						url = myDSMOURL_HIGH + "GetImage.ashx?img=" + myPDFinfo_HIGH + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)


	# ************************************
	# Loop as page number amount - optional for multi page PDF support
	for i in range(1):
	# ************************************
		# Loop for Low & High Resolution PDF Output verification
		for j in range(2):
			# Link Generation - Low Resolution
			if str(j) == "0":
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#1. LowRes PDF as - PNG - PrintBleedActive
						print ("#1. LowRes PDF as - PNG - 600px - PrintBleedActive")
						url = myDSMOURL_LOW_CUSTOM + "GetImage.ashx?img=" + myPDFinfo_LOW_CUSTOM + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#2. LowRes PDF as - JPG - PrintBleedActive
						print ("#2. LowRes PDF as - JPG - 600px - PrintBleedActive")
						url = myDSMOURL_LOW_CUSTOM + "GetImage.ashx?img=" + myPDFinfo_LOW_CUSTOM + "&pw=600&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
			# Link Generation - High Resolution
			else:
				#Loop boolean as PNG and JPG format link generation
				for h in range(2):
					if str(h) == "0":
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#3. HighRes PDF as PNG - PrintBleedActive
						print ("#3. HighRes PDF as PNG - 1500px - PrintBleedActive")
						url = myDSMOURL_HIGH_CUSTOM + "GetImage.ashx?img=" + myPDFinfo_HIGH_CUSTOM + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=png"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)
					else:
						#0. Set general variable with leading 0
						url = "url_" + "{0:03d}".format(i+j+h+1)
						#4. HighRes PDF as JPG - PrintBleedActive
						print ("#4. HighRes PDF as JPG - 1500px - PrintBleedActive")
						url = myDSMOURL_HIGH_CUSTOM + "GetImage.ashx?img=" + myPDFinfo_HIGH_CUSTOM + "&pw=1500&rot=0&page=" + str(i+1) + "&co=100&acid=1&ext=jpg"
						print("#Output - "+str(i+j+h+1)+" -- PAGE " + "{0:03d}".format(i+1) + ": " + url)

	#5 Preview Download URL
	print ("#5 Preview Download URL")
	print ("#Preview Download Link: " + DL_LINK_LOW)
	#6 Print Download URL
	print ("#6 Print Download URL")
	print ("#Print Download Link: " + DL_LINK_HIGH)
	#5 Preview Download URL - PrintBleedActive
	print ("#5 Preview Download URL - PrintBleedActive")
	print ("#Preview Download Link: " + DL_LINK_LOW_CUSTOM)
	#6 Print Download URL - PrintBleedActive
	print ("#6 Print Download URL - PrintBleedActive")
	print ("#Print Download Link: " + DL_LINK_HIGH_CUSTOM)
	# *********************************************************************************

def addItems():
	# **********************************************************************************************
	#3. Add items
	print ("#3. Add items")
	#3-1. Add Text frame, and Edit
	print ("#3-1. Add Text frame, and Edit")
	driver.find_element_by_css_selector("span.scToolbarPanelIcon > i.icon-DS_Text").click()
	driver.find_element_by_xpath("//button_menu/div/ul/li[5]").click()
	if TestOnSauceLabs == True:
		print ("#3-2-1. Wait dynamically about appearance of next item due to execution on SauceLabs")
		elem_css = "html body div#eins main_view div.SmartCanvas.scMenuPanelEnabled.scToolbarPanelEnabled.scDocumentsPanelEnabled.scContextPanelEnabled.scLayersPanelEnabled.scDataPanelCollapsed.scDebugHidden.scDesktopDevice div.scMainPanel div.scCanvasScroller div#scDocInlineTextEditor.scDocInlineTextEditor div.scBoundingBoxTextEdit"
		wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
		driver.find_element_by_css_selector(elem_css).click()
	else:
		print ("#3-2-1. Skip dynamically waiting about appearance of next item due to local execution")
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys("Hello [[FirstName]]!")
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(1)
	#3-2. Change font size
	print ("#3-2. Change font size")
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys("28")
	driver.find_element_by_xpath("(//input[@type='text'])[21]").send_keys(Keys.RETURN)
	time.sleep(1)
	#3-3. Add Headline frame, and Edit
	print ("#3-3. Add Headline frame, and Edit")
	driver.find_element_by_css_selector("i.icon-DS_Headline").click()
	time.sleep(1)
	elem_path="//div[1]/main_view/div/div[1]/div[3]/button_menu/div/ul/li[5]/i[1]"
	driver.find_element_by_xpath(elem_path).click()
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys(Keys.CONTROL, "a")
	time.sleep(1)
	driver.find_element_by_xpath("//div").send_keys("This is my Headline")
	time.sleep(1)
	#3-4. Add Image
	print ("#3-4. Add Image")
	driver.find_element_by_css_selector("i.icon-DS_Image").click()
	elem_css = "div.image > img"
	wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
	time.sleep(1)
	driver.find_element_by_css_selector(elem_css).click()
	#3-4-1. Move Imagge element
	print ("#3-4-1. Move Imagge element")
	#3-4-2-1. Open 'Layout' context tab menu
	print ("#3-4-2-1. Open 'Layout' context tab menu")
	elem_css = "ul.scContextTabs > li + li + li"
	wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, elem_css)))
	driver.find_element_by_css_selector(elem_css).click()
	#3-4-2-2. Set X position of the element - 'X = 8 inch'
	print("#3-4-2-2. Set X position of the element - 'X = 8 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("8")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-4-2-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-4-2-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	#3-5. QRCode and click OK on modal dialog
	print ("#3-5. QRCode and click OK on modal dialog")
	driver.find_element_by_css_selector("i.icon-DS_QRcode").click()
	driver.find_element_by_css_selector("div.scCol-8 > div.scFormCtrl.sc100PercentWidth > div.scCtrlInput > input[type='text']").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_css_selector("div.scCol-8 > div.scFormCtrl.sc100PercentWidth > div.scCtrlInput > input[type='text']").send_keys("www.directsmile.com")
	driver.find_element_by_xpath("//div[38]/div[3]/div").click()
	#3-5-2. Move QRCode element
	print("#3-5-2. Move QRCode element")
	#3-5-2-2. Set X position of the element - 'X = 4 inch'
	print("#3-5-2-2. Set X position of the element - 'X = 4 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("4")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-5-2-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-5-2-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	#3-6-1. Add Rectangle Box element
	print("#3-6-1. Add Rectangle Box element")
	driver.find_element_by_css_selector("i.icon-DS_Checkbox").click()
	#3-6-2. Set X position of the element - 'X = 0.5 inch'
	print("#3-6-2. Set X position of the element - 'X = 0.5 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("0.5")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-6-3. Set Y position of the element - 'Y = 2 inch'
	print("#3-6-3. Set Y position of the element - 'Y = 2 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("2")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	#3-7-1. Add Ellipse element
	print("#3-7-1. Add Ellipse element")
	driver.find_element_by_css_selector("i.icon-DS_Ellipse").click()
	#3-7-2. Set X position of the element - 'X = 1 inch'
	print("#3-7-2. Set X position of the element - 'X = 1 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("1")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-7-3. Set Y position of the element - 'Y = 5 inch'
	print("#3-7-3. Set Y position of the element - 'Y = 5 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("5")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	#3-7-1. Add Line element
	print("#3-7-1. Add Line element")
	driver.find_element_by_css_selector("i.icon-DS_Line").click()
	#3-7-2. Set X position of the element - 'X = 8 inch'
	print("#3-7-2. Set X position of the element - 'X = 8 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys("8")
	driver.find_element_by_xpath("(//input[@type='text'])[23]").send_keys(Keys.RETURN)
	#3-7-3. Set Y position of the element - 'Y = 4 inch'
	print("#3-7-3. Set Y position of the element - 'Y = 4 inch'")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").click()
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.CONTROL, "a")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys("4")
	driver.find_element_by_xpath("(//input[@type='text'])[24]").send_keys(Keys.RETURN)
	time.sleep(1)
	# **********************************************************************************************

def setDocument():
	# **********************************************************************************************
	#4. Document settings
	print ("#4. Document settings")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[4]").click()
	time.sleep(1)
	#4-1. Configure "Document Setting"
	#Ver7.3 - [150]
	CurrentInputNumber = "150"
	# Margin 
	for i in range(4):
		elem_xpath = "(//input[@type='text'])["+str(i+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.CONTROL, "a")
		driver.find_element_by_xpath(elem_xpath).send_keys("1")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	#4-2. Activate '#print bleed active' option
	print ("#4-2. Activate '#print bleed active' option")
	elem_path = "//div/main_view/div/div[34]/div[2]/div/div[13]/div/checkbox_ctrl/div/div/div"
	driver.find_element_by_xpath(elem_path).click()
	time.sleep(1)
	# Print bleed area
	# ---- Scroll down to show target content in window (Necessary to handle invisible element in window)
	elem_xpath = "(//input[@type='text'])["+str(i+4+int(CurrentInputNumber)) +"]"
	driver.find_element_by_xpath(elem_xpath).location_once_scrolled_into_view
	for i in range(4):
		# Print bleed area
		elem_xpath = "(//input[@type='text'])["+str(i+4+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.CONTROL, "a")
		driver.find_element_by_xpath(elem_xpath).send_keys("1")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	# Display bleed and offset
	for i in range(6):
		elem_xpath = "(//input[@type='text'])["+str(i+8+int(CurrentInputNumber)) +"]"
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.CONTROL, "a")
		driver.find_element_by_xpath(elem_xpath).send_keys("1")
		driver.find_element_by_xpath(elem_xpath).send_keys(Keys.RETURN)
	#4-5. DSMI document for preview rendering
	#driver.find_element_by_xpath("//div[23]/div/checkbox_ctrl/div/div/div").click()
	time.sleep(1)
	#4-6. Close dialog
	print ("#4-6. Close dialog")
	driver.find_element_by_xpath("//div[34]/div[3]/div").click()
	time.sleep(1)

def exitCanvas():
	# **********************************************************************************************
	#11. Exit
	print ("#11. Exit")
	driver.find_element_by_xpath("//div[@id='eins']/main_view/div/menu_panel/div/ul/li/span").click()
	driver.find_element_by_xpath("//menu_panel/div/ul/li/ul/li[12]/span").click()
	time.sleep(1)
	#11-1. Click OK to save unsaved change alert dialog
	print ("#11-1. Click OK to save unsaved change alert dialog")
	elem_path = "//div[8]/div[3]/div/div"
	driver.find_element_by_xpath(elem_path).click()
	# **********************************************************************************************

def quitDriver():
    # **********************************************************************************************
    #15. Close
    print ("#15. Close")
    driver.quit()
    # **********************************************************************************************
