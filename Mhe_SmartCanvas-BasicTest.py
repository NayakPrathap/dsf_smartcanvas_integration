#-----------------------------------------------------------------------------------------------
# TESTCASE
# Scenario:
# Very basic testcase about basic functionality. Currently tested is documentsize and direction,
# placement of image and text, saving and deleting the items.
# This testcase needs to be expanded.
#-----------------------------------------------------------------------------------------------

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys, os

# Custom modules:
currentDirectory = ((sys.argv[0]).rsplit('\\', 1))[0]   # This gets the filepath from the current file e.g.: "C:\Users\Devops\MyPath"
os.chdir(currentDirectory)                              # The path is necessary when running in Visual Studio to be able to find custommodules
import Mhe_ScModule

# If you would like to run the script from commandline with arguments: Use False
# If you would like to run the script from here and without specifying arguments through external commandline: Use True
# An external commandline testcall of this script could look something like this:
# python C:\Users\marcelhe\Desktop\SmartCanvas-MheBranch\NOG\SmartCanvas-BasicTest.py -brwsr "Firefox" -scurl "https://devops-az01.directsmile.com/webapi/smartcanvas/opensmartcanvas.html#{%22dsmx%22:{%22targetProtocol%22:%22https%22,%22targetSystem%22:%22devops-az01.directsmile.com%22,%22publicKey%22:%229904294D%22,%22privateKey%22:%2207F22073035C3209%22},%22documentId%22:2123,%22useCampaignStorage%22:true,%22showRulers%22:true,%22currentUserName%22:%22Admin%22}" -scvrs "7.8.2.18" -ufbck "False"
ExecuteWithLocalArguments = True

if ExecuteWithLocalArguments:
    #sys.argv = ["SmartCanvas-BasicTest.py", "-brwsr", "Firefox", "-scurl", "https://devops-az01.directsmile.com/webapi/smartcanvas/opensmartcanvas.html#{%22dsmx%22:{%22targetProtocol%22:%22https%22,%22targetSystem%22:%22devops-az01.directsmile.com%22,%22publicKey%22:%229904294D%22,%22privateKey%22:%2207F22073035C3209%22},%22documentId%22:2123,%22useCampaignStorage%22:true,%22showRulers%22:true,%22currentUserName%22:%22Admin%22}", "-scvrs", "7.8.2.18", "-ufbck", "False"]
    sys.argv = ["SmartCanvas-BasicTest.py", "-brwsr", "Firefox", "-scurl", "https://devops-az01.directsmile.com/smartcanvas", "-scvrs", "7.8.2.18", "-ufbck", "False"]

# --- INTRO ---
# Before starting the webdriver, evaluate and check all the provided arguments
BrowserToRun, ScURL, ScVersion, Fallback = Mhe_ScModule.evaluateArguments(sys.argv)

# Start the webdriver with the desired browser
# Accepted values: "Firefox", "Internetexplorer", "Edge", "Chrome" or "Safari"
# Accepted values: "en-us", "fr", etc.
driver = Mhe_ScModule.startToRunBrowser(BrowserToRun, "en-us")

# Take the started driver and let it browse to specified SmartCanvas-URL
# Accepted values: Any well-formated URL as string
Mhe_ScModule.openSmartCanvas(driver, ScURL)


# --- TESTCASE ---
# Start of actual testcase
Mhe_ScModule.printSeparator()
print("Starting Testcase")
Mhe_ScModule.printSeparator()

# Resize the document format. Please note: Resizing sets the direction to "Portrait" by default. This means if you resize, you have to set "Landscape" again afterwards.
# Accepted values: "Letter", "Tabloid", "Legal", "Statement", "Executive", "A0", "A1", "A2", "A3", "A4", "A5", "A6", "B4" or "B5"
#Mhe_ScModule.resizeDocument(driver, "A4")

# Change the document's direction
# Accepted values: "Portrait" or "Landscape"
#Mhe_ScModule.changeDocumentDirection(driver, "Landscape")

# Adds an item to the SmartCanvas document
# Accepted values: "Text", "Headline", "Image", "QR Code", "Rectangle", "Ellipse" or "Line"
Mhe_ScModule.addItem(driver, "Image")

Mhe_ScModule.addItem(driver, "Image")
# OpenImageBrowser
Mhe_ScModule.openImageBrowser(driver)
# Sets a selected image to another source
# Accepted values: "Text", "Headline", "Image", "QR Code", "Rectangle", "Ellipse" or "Line"
Mhe_ScModule.setImage(driver, "man whispering4.png")

# Selects an item of the SmartCanvas document
# Accepted values: "Text", "Headline", "Image", "QR Code", "Rectangle", "Ellipse" or "Line"
#Mhe_ScModule.selectItemByType(driver, "Text")

# Saves the SmartCanvas document
Mhe_ScModule.saveCanvas(driver)

# Deletes all the components on the Canvas
#Mhe_ScModule.cleanupCanvas(driver)

# Closes the webdriver
Mhe_ScModule.closeWebdriver(driver)










# TO BE IMPLEMENTED / REFACTORED
#Mhe_ScModule.addItems_Image()

#Mhe_ScModule.moveItems_Image()

#Mhe_ScModule.addItems_QRCode()

#Mhe_ScModule.moveItems_QRCode()

#Mhe_ScModule.addItems_Rectangle()

#Mhe_ScModule.moveItems_Rectangle()

#Mhe_ScModule.addItems_Ellipse()

#Mhe_ScModule.moveItems_Ellipse()

#Mhe_ScModule.addItems_Line()

#Mhe_ScModule.moveItems_Line()

#Mhe_ScModule.addItems_TEXT()

#Mhe_ScModule.editItems_TEXT()

#Mhe_ScModule.addItems_Headline()

#Mhe_ScModule.saveCanvas()

#Mhe_ScModule.outputPDF_LOW()

#Mhe_ScModule.outputPDF_HIGH()

#Mhe_ScModule.setDocument()

#Mhe_ScModule.outputPDFCustom_LOW_CUSTOM()

#Mhe_ScModule.outputPDFCustom_HIGH_CUSTOM()

#Mhe_ScModule.cleanupDocumentSetting()

#Mhe_ScModule.printPDFCustom(1)

#Mhe_ScModule.quitDriver()