# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys
import SCKit, SCKitRegression

#allArgs = ["myScript", "firefox", "https://devops-az01.directsmile.com/webapi/smartcanvas/opensmartcanvas.html#{%22dsmx%22:{%22targetProtocol%22:%22https%22,%22targetSystem%22:%22devops-az01.directsmile.com%22,%22publicKey%22:%222AD197A5%22,%22privateKey%22:%22866ACFF70A9CD7BD%22},%22documentId%22:2138,%22useCampaignStorage%22:true,%22showRulers%22:true,%22currentUserName%22:%22Admin%22,%22showPreviewDocumentSettings%22:true}", "7.3.9.200"]
allArgs = sys.argv
#allArgs = ["myScript", "firefox", "https://devops-az01.directsmile.com/smartcanvas/index.html", "7.3.3.128"]
#allArgs = ["myScript", "firefox", "https://devops-az01.directsmile.com/smartcanvas/index.html", "7.3.3.128", "http://Nobukins:1b000cda-5739-49ad-8f79-271f7794cf78@ondemand.saucelabs.com:80/wd/hub", "WIN10", "52", "DSMI-Document-for-Preview-Rendering"]


# **********************************************************************************************
# THIS IS SMOKE TEST FOR SMARTCANVAS
print ("THIS IS SMOKE TEST FOR SMARTCANVAS - DSMI document for preview rendering function test")
# **********************************************************************************************
driver = SCKitRegression.magicArgs(allArgs)
SCKitRegression.getWindowSize()
SCKitRegression.changeWindowSize(1980,1200)
SCKitRegression.ResizeDocument(10)
SCKitRegression.changeDocumentDirection("Landscape")
#SCKitRegression.DSMI_Document_for_Preview_Rendering__SettingDocument("GlobalAccount")
#SCKitRegression.DSMI_Document_for_Preview_Rendering__SettingDocument("OwnAccount")
SCKitRegression.openDocumentSetting()
SCKitRegression.doubleClick_scModalHead()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering()
#SCKitRegression.closeDocumentSetting()
#SCKitRegression.changeWindowSize(1280,898)
#SCKitRegression.openDocumentSetting()
#SCKitRegression.doubleClick_scModalHead()
#SCKitRegression.scroll_to_DSMI_Document_for_Preview_Rendering()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____SetMode("Print")
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____SetAliasName("TumblerDevOps")
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____SetSourceType("GlobalAccount")
SCKitRegression.doubleClick_scModalHead()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____CloseDialog()

SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields_Add()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields_Open_FormFieldEditor()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields_Edit_FormFieldEditor("TumblerDevOps")
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields_Close_FormFieldEditor()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____UploadImage()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____SelectImage()

#SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____ConfigureFormFields("TumblerDevOps")
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____ConfigureImage_Add()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____ConfigureImage_Link()
SCKitRegression.configure_DSMI_Document_for_Preview_Rendering____ConfigureImage_Move()
SCKitRegression.saveCanvas()
SCKitRegression.outputPDF_LOW()
SCKitRegression.outputPDF_HIGH()
SCKitRegression.cleanupCanvas()
SCKitRegression.cleanupFormFields(2)
SCKitRegression.deleteUploadedImageFile()
SCKitRegression.Cleanup__DSMI_Document_for_Preview_Rendering__SettingDocument()
SCKitRegression.printPDF(1)
SCKitRegression.saveCanvas()
SCKitRegression.quitDriver()