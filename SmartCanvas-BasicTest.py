# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys
import SCKit, SCKitRegression

allArgs = sys.argv
#allArgs = ["myScript", "firefox", "https://devops-az06.directsmile.com/webapi/smartcanvas/opensmartcanvas.html#{%22dsmx%22:{%22targetProtocol%22:%22https%22,%22targetSystem%22:%22devops-az06.directsmile.com%22,%22publicKey%22:%221EA4F119%22,%22privateKey%22:%221803F974E546BB7C%22},%22documentId%22:2103,%22useCampaignStorage%22:true,%22showRulers%22:true,%22currentUserName%22:%22Admin%22}", "7.3.1.117"]

# *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*- 
# **********************************************************************************************
# THIS IS SMOKE TEST FOR SMARTCANVAS
print ("THIS IS SMOKE TEST FOR SMARTCANVAS - General Functionality Test")
# **********************************************************************************************
driver = SCKitRegression.magicArgs(allArgs)
#SCKitRegression.addItems()
SCKitRegression.ResizeDocument(10)
SCKitRegression.changeDocumentDirection("Landscape")
SCKitRegression.addItems_Image()
SCKitRegression.moveItems_Image()
SCKitRegression.addItems_QRCode()
SCKitRegression.moveItems_QRCode()
SCKitRegression.addItems_Rectangle()
SCKitRegression.moveItems_Rectangle()
SCKitRegression.addItems_Ellipse()
SCKitRegression.moveItems_Ellipse()
SCKitRegression.addItems_Line()
SCKitRegression.moveItems_Line()
SCKitRegression.addItems_TEXT()
SCKitRegression.editItems_TEXT()
SCKitRegression.addItems_Headline()
SCKitRegression.saveCanvas()
SCKitRegression.outputPDF_LOW()
SCKitRegression.outputPDF_HIGH()
SCKitRegression.setDocument()
SCKitRegression.saveCanvas()
SCKitRegression.outputPDFCustom_LOW_CUSTOM()
SCKitRegression.outputPDFCustom_HIGH_CUSTOM()
SCKitRegression.cleanupDocumentSetting()
SCKitRegression.cleanupCanvas()
#SCKit.exitCanvas()
SCKitRegression.printPDFCustom(1)
SCKitRegression.quitDriver()