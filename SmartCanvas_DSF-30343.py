# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, re, urllib, sys
import SCKit, SCKitRegression

allArgs = sys.argv
#allArgs = ["myScript", "firefox", "https://devops-az06.directsmile.com/webapi/smartcanvas/opensmartcanvas.html#{%22dsmx%22:{%22targetProtocol%22:%22https%22,%22targetSystem%22:%22devops-az06.directsmile.com%22,%22publicKey%22:%221EA4F119%22,%22privateKey%22:%221803F974E546BB7C%22},%22documentId%22:2103,%22useCampaignStorage%22:true,%22showRulers%22:true,%22currentUserName%22:%22Admin%22}", "7.3.1.117"]

# *********************************************************************************
# THIS IS REGRESSION TEST ABOUT DSF-30343
# Please refer to below link for further information 
# https://jira.efi.com/browse/DSF-30343
print ("THIS IS REGRESSION TEST ABOUT DSF-30343")
print ("Please refer to below link for further information")
print ("https://jira.efi.com/browse/DSF-30343")
# *********************************************************************************
# *********************************************************************************
# THIS IS REGRESSION TEST ABOUT DSF-30349
# Please refer to below link for further information 
# https://jira.efi.com/browse/DSF-30349
print ("THIS IS REGRESSION TEST ABOUT DSF-30349")
print ("Please refer to below link for further information")
print ("https://jira.efi.com/browse/DSF-30349")
# *********************************************************************************
# **********************************************************************************************
# Jira Number: DSF-30343
# --- --- ---: Enable Facing Pages Optio when it has contents on the canvas of each page.
# --- --- ---: Expect contents remained, and just shows a Facing pages as you layouted.
# **********************************************************************************************
# Jira Number: DSF-30349_Advanced
# --- --- ---: Test possible special charcter as input. Check compatibility with those auto conversion.
# --- --- ---: Expect special character treated in proper auto converted letters in where you cannot use them.
# **********************************************************************************************
driver = SCKitRegression.magicArgs(allArgs)
SCKitRegression.ResizeDocument(10)
SCKitRegression.changeDocumentDirection("Landscape")
SCKitRegression.DSF_30343()
SCKitRegression.saveCanvas()
SCKitRegression.outputPDF_LOW()
SCKitRegression.outputPDF_HIGH()
SCKitRegression.TickFacingPage()
SCKitRegression.outputPDFCustom_LOW_CUSTOM()
SCKitRegression.outputPDFCustom_HIGH_CUSTOM()


# **********************************************************************************************
#17. Clean Up
print ("#17. Clean Up")
# **********************************************************************************************

SCKitRegression.TickFacingPage()
SCKitRegression.cleanupPages(4)
SCKitRegression.cleanupCanvas()
SCKitRegression.cleanupFormFields(20)
SCKitRegression.printPDF(4)
SCKitRegression.printPDFCustom(3)
SCKitRegression.quitDriver()